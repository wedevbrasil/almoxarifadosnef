<div class="preloader js-preloader flex-center">
    <div class="dots">
        <div class="dot"></div>
        <div class="dot"></div>
        <div class="dot"></div>
    </div>
</div>

    <!-- Preloader CSS -->
    <style>
        .flex-center {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(214, 214, 214, 0.3);
            z-index: 99999;
        }

        .dots .dot {
            display: inline-block;
            width: 15px;
            height: 15px;
            margin: 0 10px 0 10px;
            border-radius: 50%;
            background: #999;
            -webkit-animation: dot-dot-dot 1.4s linear infinite;
            animation: dot-dot-dot 1.4s linear infinite;
        }

        .dots .dot:nth-child(2) {
            animation-delay: .2s;
        }

        .dots .dot:nth-child(3) {
            animation-delay: .4s;
        }

        @keyframes dot-dot-dot {
            0%, 60%, 100% {
                -webkit-transform: initial;
                -ms-transform: initial;
                transform: initial;
            }
            30% {
                -webkit-transform: translateY(-25px);
                -ms-transform: translateY(-25px);
                transform: translateY(-25px);
            }
        }
    </style>
    <!-- End Preloader CSS -->

@section('post_scripts')
    <!-- Preloader JS-->
    <script src="/assets/js/jquery.preloadinator.min.js"></script>
    <script>
        $('.js-preloader').preloadinator();
    </script>
    <!-- End Preloader JS -->
@endsection
