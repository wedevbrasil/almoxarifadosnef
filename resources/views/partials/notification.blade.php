@php
    $unreadNotifications = Auth::user()->unreadNotifications;
@endphp

<li class="notify-toggle-wrapper dropdown-notifications" style="padding-right: 20px;">
    <a href="#" data-toggle="dropdown" class="toggle">
        <i class="fa fa-bell"></i>
        <span class="badge badge-danger nowrap notif-count" data-count="{{$unreadNotifications->count()}}">
            {{$unreadNotifications->count()}}
        </span>
    </a>
    <ul class="dropdown-menu notifications animated fadeIn" style="width: 450px !important;">
        <li class="total">
            <span class="small">
               Você tem <strong class="notif-count">{{$unreadNotifications->count()}}</strong> novas notificões.
                {{--<a href="javascript:;" class="pull-right">Marcar todas como lidas</a>--}}
            </span>
        </li>
        <li class="list">

            <ul class="dropdown-menu-list list-unstyled ps-scrollbar">

                @foreach($unreadNotifications as $notification)
                    <li class="unread
                            @if($notification->type == 'App\Notifications\NewTransferNotification')
                                available
                            @elseif($notification->type == 'App\Notifications\NewInvoiceNotification')
                                away
                            @endif
                        "> <!-- available: success, warning, info, error -->
                        <a href="{{ $notification->data['link'] }}">
                            <div class="notice-icon">
                                @if($notification->type == 'App\Notifications\NewTransferNotification')
                                    <i class="fa fa-exchange-alt"></i>
                                @elseif($notification->type == 'App\Notifications\NewInvoiceNotification')
                                    <i class="fa fa-file-invoice"></i>
                                @endif
                            </div>
                            <div>
                                <span class="name">
                                    <strong>
                                        {{ $notification->data['title'] }}
                                    </strong><span class="time small pull-right">
                                        {{\Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans()}}
                                    </span>
                                    <span class="clear-fix"></span>
                                    <p><small class="small">
                                        {{ $notification->data['message'] }}
                                    </small></p>
                                </span>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>

        </li>

        {{--<li class="external">
            <a href="javascript:;">
                <span>Ler todas as notificações</span>
            </a>
        </li>--}}
    </ul>
</li>
