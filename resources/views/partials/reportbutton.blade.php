<div class="feedback left">
    <div class="tooltips">
        <div class="btn-group dropup">
            <button type="button" class="btn btn-primary dropdown-toggle btn-circle btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-bug fa-2x" title="Feedback"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right dropdown-menu-form">
                <li>
                    <div class="report">
                        <h2 class="text-center">Feedback</h2>
                        <form class="doo" method="post" action="{{ route('reportbug') }}">
                            @method('POST')
                            @csrf
                            <div class="col-sm-12">
                                <select required name="type" class="form-control">
                                    <option value="">Assunto</option>
                                    <option value="report-bug">Reportar Erro</option>
                                    <option value="report-problem">Reportar Problema</option>
                                    <option value="suggestion">Sugestão</option>
                                </select>
                                <textarea required name="comment" class="form-control"
                                          placeholder="Por favor, seja o mais detalhado possível."
                                ></textarea>
                                <input name="screenshot" type="hidden" class="screen-uri">
                                <span class="screenshot pull-right"><i class="fa fa-camera cam" title="Tirar printscreen"></i></span>
                            </div>
                            <div class="col-sm-12 clearfix">
                                <button class="btn btn-primary btn-block" type="submit">Enviar relatório</button>
                            </div>
                        </form>
                    </div>
                    <div class="loading text-center hideme">
                        <h2>Aguarde...</h2>
                        <h2><i class="fa fa-refresh fa-spin"></i></h2>
                    </div>
                    <div class="reported text-center hideme">
                        <h2>Obrigado!</h2>
                        <p>Recebemos o seu relatório e em breve iremos resolver.</p>
                        <div class="col-sm-12 clearfix">
                            <button class="btn btn-success btn-block do-close">Fechar</button>
                        </div>
                    </div>
                    <div class="failed text-center hideme">
                        <h2>Erro</h2>
                        <p>Erro ao tentar enviar.<br><br><a href="mailto:loganguns@gmail.com">Tente entra em contato por email.</a></p>
                        <div class="col-sm-12 clearfix">
                            <button class="btn btn-danger btn-block do-close">Fechar</button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

