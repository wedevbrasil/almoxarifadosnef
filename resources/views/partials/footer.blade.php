<footer class="page-footer font-small blue">

    <div class="col-lg-12" style="margin-top: 50px !important; padding-top: 50px !important;">
        <section class="box ">
            <!-- Copyright -->
            <div class="text-center text">© 2019 Todos os direitos reservados.
                <div class="pull-right" style="margin-right: 10px">
                    <a href="#">
                        <small> @version('full')</small>
                    </a>
                </div>
            </div>
            <!-- Copyright -->
        </section>
    </div>

</footer>
