<li class='menusection'>Principal</li>
<li class="">
    <a href="{{ route('admin.dashboard.index') }}">
        <i class="fa fa-tachometer-alt"></i>
        <span class="title">Dashboard</span>
    </a>
</li>
<li class='menusection'>Administração</li>
        <li class="open-">
            <a href="javascript:;">
                <i class="fa fa-warehouse"></i>
                <span class="title">Almoxarifados</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a class="active-" href="{{ route('admin.almoxarifado.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos os Almoxarifados</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.almoxarifado.create') }}" >
                        <i class="fa fa-plus-circle"></i> Novo Almoxarifado</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.almoxarifado.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <i class="fa fa-truck-loading"></i>
                <span class="title">Fornecedores</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('admin.fornecedor.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos os Fornecedores</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.fornecedor.create') }}" >
                        <i class="fa fa-plus-circle"></i> Novo Fornecedor</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.fornecedor.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <i class="fa fa-id-badge"></i>
                <span class="title">Funcionários</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('admin.funcionario.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos os Funcionários</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.funcionario.create') }}" >
                        <i class="fa fa-plus-circle"></i> Novo Funcionário</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.funcionario.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-sitemap"></i>
                        <span class="title">Setores</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" >
                        <li>
                            <a class="" href="{{ route("admin.setor.index") }}" >
                                <i class="fa fa-list-ol"></i> Todos os Setores</a>
                        </li>
                        <li>
                            <a class="" href="{{ route("admin.setor.create") }}" >
                                <i class="fa fa-plus-circle"></i> Novo Setor</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('admin.setor.deleted') }}" >
                                <i class="fa fa-trash"></i> Inativos</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-marker"></i>
                        <span class="title">Funções</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" >
                        <li>
                            <a class="" href="{{ route("admin.funcao.index") }}" >
                                <i class="fa fa-list-ol"></i> Todos as Funções</a>
                        </li>
                        <li>
                            <a class="" href="{{ route("admin.funcao.create") }}" >
                                <i class="fa fa-plus-circle"></i> Nova Função</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('admin.funcao.deleted') }}" >
                                <i class="fa fa-trash"></i> Inativos</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <i class="fa fa-barcode"></i>
                <span class="title">Produtos</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route("admin.ferramenta.index") }}" >
                        <i class="fa fa-list-ol"></i> Todos os Produtos</a>
                </li>
                <li>
                    <a class="" href="{{ route("admin.ferramenta.create") }}" >
                        <i class="fa fa-plus-circle"></i> Novo Produto</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.ferramenta.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-object-group"></i>
                        <span class="title">Grupos</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" >
                        <li>
                            <a class="" href="{{ route('admin.tipoitem.index') }}" >
                                <i class="fa fa-list-ol"></i> Todos os Grupos</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('admin.tipoitem.create') }}" >
                                <i class="fa fa-plus-circle"></i> Novo Grupo</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('admin.tipoitem.deleted') }}" >
                                <i class="fa fa-trash"></i> Inativos</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-weight"></i>
                        <span class="title">Unidades</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" >
                        <li>
                            <a class="" href="{{ route('admin.unidade.index') }}" >
                                <i class="fa fa-list-ol"></i> Todos as Unidades</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('admin.unidade.create') }}" >
                                <i class="fa fa-plus-circle"></i> Nova Unidade</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('admin.unidade.deleted') }}" >
                                <i class="fa fa-trash"></i> Inativos</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <i class="fa fa-hard-hat"></i>
                <span class="title">Obras</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('admin.obra.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos as Obras</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.obra.create') }}" >
                        <i class="fa fa-plus-circle"></i> Nova Obra</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.obra.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <i class="fa fa-plus"></i>
                <span class="title">Entradas</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('admin.entrada.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos as Entradas</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.entrada.create') }}" >
                        <i class="fa fa-plus-circle"></i> Nova Entrada</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.entrada.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>

                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-object-group"></i>
                        <span class="title">Tipos de Entrada</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" >
                        <li>
                            <a class="" href="{{ route('admin.tipoentrada.index') }}" >
                                <i class="fa fa-list-ol"></i> Todos os Tipos</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('admin.tipoentrada.create') }}" >
                                <i class="fa fa-plus-circle"></i> Novo Tipo</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('admin.tipoentrada.deleted') }}" >
                                <i class="fa fa-trash"></i> Inativos</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <i class="fa fa-minus"></i>
                <span class="title">Retiradas</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('admin.retirada.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos as Retiradas</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.retirada.create') }}" >
                        <i class="fa fa-plus-circle"></i> Nova Retirada</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.retirada.index', ['status' => '1']) }}" >
                        <i class="fa fa-clock"></i> Aguardando Aprovação</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.retirada.index', ['status' => '5']) }}" >
                        <i class="fa fa-undo-alt"></i> Devoluções</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.retirada.index', ['status' => '5']) }}" >
                        <i class="fa fa-exchange-alt"></i> Transferências</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.retirada.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <i class="fa fa-exchange-alt"></i>
                <span class="title">Transferências</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('admin.transferencia.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos as Transferências</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.transferencia.create') }}" >
                        <i class="fa fa-plus-circle"></i> Nova Transferência</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.transferencia.index', ['status' => '1']) }}" >
                        <i class="fa fa-clock"></i> Aguardando Aprovação</a>
                </li>
                <li>
                    <a class="" href="{{ route('admin.transferencia.index', ['status' => '5']) }}" >
                        <i class="fa fa-undo-alt"></i> Devoluções</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="{{ route('admin.fatura.index') }}">
                <i class="fa fa-file-invoice"></i>
                <span class="title">Faturas</span>
            </a>
        </li>
        <li class="">
            <a href="javascript:;">
                <i class="fa fa-users"></i>
                <span class="title">Usuários</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('admin.user.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos os Usuários</a>
                </li>
                <li>
                    <a class="" href="#" >
                        <i class="fa fa-plus-circle"></i> Novo Usuário</a>
                </li>
                <li>
                    <a class="" href="#" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
            </ul>
        </li>
