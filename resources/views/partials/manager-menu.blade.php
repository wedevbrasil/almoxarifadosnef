<li class='menusection'>Principal</li>
<li class="">
    <a href="/">
        <i class="fa fa-tachometer-alt"></i>
        <span class="title">Dashboard</span>
    </a>
</li>
<li class='menusection'>{{ Auth::user()->employee['building']['name'] }}</li>
<li>
    <a class="" href="{{ route('user.obra.show', Auth::user()->employee['building']['id']) }}" >
        <i class="fa fa-clipboard-list"></i>
        <span class="title">Resumo da Obra</span>
    </a>
</li>
<li class="">
    <a href="javascript:;">
        <i class="fa fa-book"></i>
        <span class="title">Cadastros</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu" >
        <li class="">
            <a href="javascript:;">
                <span class="title">Almoxarifados</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('user.almoxarifado.index') }}" >Todos os Almoxarifados</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.almoxarifado.create') }}" >Novo Almoxarifado</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.almoxarifado.deleted') }}" >Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <span class="title">Funcionários</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('user.funcionario.index') }}" >Todos os Funcionários</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.funcionario.create') }}" >Novo Funcionário</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.funcionario.deleted') }}" >Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <span class="title">Fornecedores</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('user.fornecedor.index') }}" >Todos os Fornecedores</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.fornecedor.create') }}" >Novo Fornecedor</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.fornecedor.deleted') }}" >Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <span class="title">Itens</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route("user.ferramenta.index") }}" >Todos os Itens</a>
                </li>
                <li>
                    <a class="" href="{{ route("user.ferramenta.create") }}" >Novo Item</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.ferramenta.deleted') }}" >Inativos</a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li class="">
    <a href="javascript:;">
        <i class="fa fa-tools"></i>
        <span class="title">Ferramentas/Equipamentos</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu" >
        @foreach($buildings as $building)
            <?php //Teste Parar ver se Pertence a Obra ?>
            @if($building->id == Auth::user()->employee['building']['id'])
                <li>
                    <a class="" href="{{ route('user.ferramenta.index', $building->id) }}" >Resumo</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.entrada.index') }}" >Entradas</a>
                </li>
                    <li>
                        <a class="" href="{{ route('user.retirada.index') }}" >Retiradas</a>
                    </li>
                    <ul class="" >
                        <li>
                            <a class="" href="{{ route('user.retirada.index', ['status' => '1']) }}" >Aguardando Aprovação</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('user.retirada.index', ['status' => '5']) }}" >Devoluções</a>
                        </li>
                    </ul>
            @endif
        @endforeach
    </ul>
</li>

<li class='menusection'>Outras Obras</li>

<ul class="sub-menu" >
    @foreach($buildings as $building)
        <?php //Teste Parar ver se Pertence a Obra ?>
        @if($building->id != Auth::user()->employee['building']['id'])
            <li class="">
                <a href="javascript:;">
                    <span class="title">{{ str_limit($building->name, $limit = 35, $end = '...') }}</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li>
                        <a class="" href="{{ route('user.obra.show', $building->id) }}" >Resumo</a>
                    </li>
                    <li>
                        <a class="" href="{{ route('user.entrada.index') }}" >Entradas</a>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <span class="title">Retiradas</span>
                            <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu" >
                            <li>
                                <a class="" href="{{ route('user.retirada.index') }}" >Todas as Retiradas</a>
                            </li>
                            <li>
                                <a class="" href="{{ route('user.retirada.create') }}" >Nova Retirada</a>
                            </li>
                            <li>
                                <a class="" href="#" >Devolução</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        @endif
    @endforeach
</ul>
