<li class='menusection'>Principal</li>
<li class="">
    <a href="{{ route('user.dashboard.index') }}">
        <i class="fa fa-tachometer-alt"></i>
        <span class="title">Dashboard</span>
    </a>
</li><li class='menusection'>{{ Auth::user()->employee['building']['name'] }}</li>
<li>
    <a class="" href="{{ route('user.almoxarifado.show', Auth::user()->employee['building']['id']) }}" >
        <i class="fa fa-chart-line"></i>
        <span class="title">Resumo do Almoxarifado</span>
    </a>
</li>
<li class="">
    <a href="javascript:;">
        <i class="fa fa-address-card"></i>
        <span class="title">Cadastros</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu" >
        <li class="">
            <a href="javascript:;">
                <span class="title">
                    <i class="fa fa-warehouse"></i>
                    Almoxarifados</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('user.almoxarifado.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos os Almoxarifados</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.almoxarifado.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <span class="title">
                        <i class="fa fa-id-badge"></i> Funcionários</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('user.funcionario.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos os Funcionários</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.funcionario.create') }}" >
                        <i class="fa fa-plus-circle"></i> Novo Funcionário</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.funcionario.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <span class="title">
                <i class="fa fa-truck-loading"></i>Fornecedores</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route('user.fornecedor.index') }}" >
                        <i class="fa fa-list-ol"></i> Todos os Fornecedores</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.fornecedor.create') }}" >
                        <i class="fa fa-plus-circle"></i> Novo Fornecedor</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.fornecedor.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
                <span class="title">
                <i class="fa fa-barcode"></i>Itens</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu" >
                <li>
                    <a class="" href="{{ route("user.ferramenta.index") }}" >
                        <i class="fa fa-list-ol"></i> Todos os Itens</a>
                </li>
                <li>
                    <a class="" href="{{ route("user.ferramenta.create") }}" >
                        <i class="fa fa-plus-circle"></i> Novo Item</a>
                </li>
                <li>
                    <a class="" href="{{ route('user.ferramenta.deleted') }}" >
                        <i class="fa fa-trash"></i> Inativos</a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li class="">
    <a href="javascript:;">
        <i class="fa fa-tools"></i>
        <span class="title">Ferramentas/Equipamentos</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu" >
        @foreach($buildings as $building)
            <?php //Teste Parar ver se Pertence a Obra ?>
            @if($building->id == Auth::user()->employee['building']['id'])
                <li>
                    <a class="" href="{{ route('user.obra.show', $building->id) }}" >
                        <i class="fa fa-chart-line"></i>Resumo</a>
                </li>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <span class="title">
                                <i class="fa fa-plus"></i>Entradas</span>
                            <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu" >
                            <li>
                                <a class="" href="{{ route('user.entrada.index') }}" >
                                    <i class="fa fa-list-ol"></i> Todas as Entradas</a>
                            </li>
                            <li>
                                <a class="" href="{{ route('user.entrada.create') }}" >
                                    <i class="fa fa-plus-circle"></i> Nova Entrada</a>
                            </li>
                        </ul>
                    </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <span class="title">
                            <i class="fa fa-minus"></i>Retiradas</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" >
                        <li>
                            <a class="" href="{{ route('user.retirada.index') }}" >
                                <i class="fa fa-list-ol"></i> Todas as Retiradas</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('user.retirada.create') }}" >
                                <i class="fa fa-plus-circle"></i> Nova Retirada</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('user.retirada.index', ['status' => '1']) }}" >
                                <i class="fa fa-clock"></i> Aguardando Aprovação</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('user.retirada.index', ['status' => '5']) }}" >
                                <i class="fa fa-undo-alt"></i> Devoluções</a>
                        </li>
                    </ul>
                </li>
            @endif
        @endforeach
    </ul>
</li>
@php
/*
<li class='menusection'>Outras Obras</li>
<ul class="sub-menu" >
    @foreach($buildings as $building)
        <?php //Teste Parar ver se Pertence a Obra ?>
        @if($building->id != Auth::user()->employee['building']['id'])
                <li class="">
                    <a href="javascript:;">
                        <span class="title">{{ str_limit($building->name, $limit = 35, $end = '...') }}</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" >
                        <li>
                            <a class="" href="{{ route('user.obra.show', $building->id) }}" >Resumo</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('user.entrada.index') }}" >Entradas</a>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <span class="title">Retiradas</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="" href="{{ route('user.retirada.index') }}" >Todas as Retiradas</a>
                                </li>
                                <li>
                                    <a class="" href="{{ route('user.retirada.create') }}" >Nova Retirada</a>
                                </li>
                                <li>
                                    <a class="" href="#" >Devolução</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
        @endif
    @endforeach
</ul> */
@endphp
