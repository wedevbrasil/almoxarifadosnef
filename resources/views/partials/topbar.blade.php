<div class='page-topbar '>
    <div class='logo-area'>

    </div>
    <div class='quick-area'>
        <div class='pull-left'>
            <ul class="info-menu left-links list-inline list-unstyled">
                <li class="sidebar-toggle-wrap">
                    <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class='pull-right'>
            <ul class="info-menu right-links list-inline list-unstyled">
                {{--<li class="hidden-sm hidden-xs searchform" style="padding-right: 10px !important;">
                    <form action="#" method="post">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-search" style="width: 15px !important;"></i>
                            </span>
                            <input type="text" class="form-control animated fadeIn" placeholder="Procurar">
                        </div>
                        <input type='submit' value="" style="padding-right: 0 !important;">
                    </form>
                </li>--}}
                @include('partials.notification')

                <li class="profile">
                    <a href="#" data-toggle="dropdown" class="toggle">
                        <img src="/assets/images/default-user.png" alt="user-image" class="img-circle img-inline">
                        <span>{{ Auth::user()->employee['name'] }} <i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="dropdown-menu profile animated fadeIn">
                        <li>
                            <a href="{{ route('user.profile') }}">
                                <i class="fa fa-user"></i>
                                Perfil
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-info"></i>
                                Ajuda
                            </a>
                        </li>
                        <li class="last">
                            <a chref="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-lock"></i>
                                Sair
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                <meta name="_token" content="{{ csrf_token() }}">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

</div>
