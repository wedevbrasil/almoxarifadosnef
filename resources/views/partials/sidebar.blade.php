<div class="page-sidebar fixedscroll" style="background-color: #f4f4f4">
    <!-- MAIN MENU - START -->
    <div class="page-sidebar-wrapper fixedscroll" id="main-menu-wrapper">

        <ul class='wraplist fixedscroll'>
            @if(Auth()->user()->role['role'] == 2)
                @include('partials.manager-menu')

            @elseif(Auth()->user()->role['role'] == 3)
                @include('partials.stockman-menu')

            @endif

            @if(Auth()->user()->role['role'] == 1)
                @include('partials.admin-menu')
            @endif
        </ul>
    </div>
    <!-- MAIN MENU - END -->
</div>
