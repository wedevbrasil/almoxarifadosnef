@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Entrada</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Entrada</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Entrada</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <form action="{{ route("admin.entrada.store") }}" method="post">
                    @method('post')
                    @csrf
                    <meta name="_token" content="{{ csrf_token() }}">
                    <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Tipo de Entrada</label>
                                        <div class="controls">
                                            <select class="select2" id="type" name="type" >
                                                <option value="">Selecione um tipo</option>
                                                @foreach($entranceTypes as $entranceType)
                                                    <option value="{{ $entranceType->id }}" @if( $entranceType->id == old("type")) selected @endif>{{ $entranceType->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Nº Nota Fiscal</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="doc" value="{{ old("doc") }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Fornecedor</label>
                                        <div class="controls">
                                            <select class="select2" id="provider" name="provider" >
                                                <option value="">Selecione um Fornecedor</option>
                                                @foreach($providers as $provider)
                                                    <option value="{{ $provider->id }}" @if( $provider->id == old("provider")) selected @endif>{{ $provider->doc }} - {{ $provider->name }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Almoxarifado</label>
                                        <div class="controls">
                                            <select class="select2" id="warehouse" name="warehouse" >
                                                <option value="">Selecione um Almoxarifado</option>
                                                @foreach($warehouses as $warehouse)
                                                    <option value="{{ $warehouse->id }}" @if( $warehouse->id == old("warehouse")) selected @endif>
                                                        {{ $warehouse->name }} - {{ $warehouse->building['number'] }} - {{ $warehouse->building['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <table id="itemTable" class="table order-list">
                                        <thead>
                                        <tr>
                                            <td>Código Produto - Descrição - Tipo</td>
                                            <td>Quantidade</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5" style="text-align: left;">
                                                <input type="button" class="btn btn-lg btn-block " id="addrow" value="Adicionar Item" />
                                            </td>
                                        </tr>
                                        <tr>
                                        </tr>
                                        </tfoot>
                                    </table>

                                    <div class="form-group">
                                        <div class="controls">
                                            <button class="btn btn-primary">Salvar</button>
                                            <button class="btn btn-danger" type="button" onclick="history.back(-1)">Cancelar</button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </section>
    </div>

@endsection

@section('post_scripts')
    <script>
        $(document).ready(function () {
            var counter = 0;

            $("#addrow").on("click", function () {
                var newRow = $("<tr>");
                var cols = "";

                cols += '<td>' +
                    '<select class="select2" id="tool" name="tool[' + counter + ']" >\n' +
                    '   <option value="">Selecione uma Ferramenta</option>\n' +
                    '       @foreach($tools as $tool)\n' +
                    '           <option value="{{ $tool->id }}">{{ $tool->cod }} - {{ $tool->name }} - {{ $tool->itemType['name'] }}</option>\n' +
                    '       @endforeach\n' +
                    '</select>'+
                    '</td>';

                cols += '<td>' +
                    '<input type="text" class="form-control" name="quantity[' + counter + ']">'+
                    '</td>';

                cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Remover"></td>';
                newRow.append(cols);
                $("table.order-list").append(newRow);
                counter++;
                    $('select.select2').select2();
            });



            $("table.order-list").on("click", ".ibtnDel", function (event) {
                $(this).closest("tr").remove();
                counter -= 1
            });


        });
    </script>
@endsection
