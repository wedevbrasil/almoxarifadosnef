@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Entradas</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Visualizar Entrada</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Entrada</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Tipo</label>
                                    <div class="controls">
                                        <select class="select2" id="type" name="type" readonly disabled>
                                            <option value="">Selecione um tipo</option>
                                            @foreach($entranceTypes as $entranceType)
                                                <option value="{{ $entranceType->id }}" @if($entranceType->id == $entrance->entranceType['id']) selected @endif>{{ $entranceType->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Nº Nota Fiscal</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $entrance->doc }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Fornecedor</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $entrance->provider['name'] }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Almoxarifado - Obra</label>
                                    <div class="controls">
                                        <input type="text" class="form-control"
                                               value="{{ $entrance->warehouse['name'] }} - {{ $entrance->warehouse['building']['name'] }}"
                                               readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Itens</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ count($entrance->tool_ids) }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12">
        <ul class="nav nav-tabs content-tabs" id="maincontent" role="tablist">
            <li class="active">
                <a href="#itens" data-toggle="tab">
                    <i class="fa fa-plus"></i> Itens
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade in active" id="itens">
                <table id="itemTable" class="table order-list">
                    <thead>
                    <tr>
                        <td>Código Produto</td>
                        <td>Descrição</td>
                        <td>Tipo</td>
                        <td>Quantidade</td>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    @foreach($itens as $i => $item)
                        <tr>
                            <td>{{ $item->cod }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->itemType->name }}</td>
                            <td>{{ $entrance->quantity[$i] }} {{ $item->unity['name'] }}(s)</td>
                        </tr>
                    @endforeach
                    <tr>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
@endsection
