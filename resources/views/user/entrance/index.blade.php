@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Entradas</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Entradas</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Todos as Entradas</h2>
                <div class="actions panel_actions pull-right">
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">
                        @if(session()->get('success'))
                            <div class="alert alert-success alert-dismissible fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Sucesso!</strong> {{ session()->get('success') }}
                            </div>
                        @endif

                        <table class="table datatable display responsive no-wrap" width="100%">
                            <thead>
                            <tr>
                                <th>Data de Entrada</th>
                                <th>Nº Nota Fiscal</th>
                                <th>Tipo de Entrada</th>
                                <th>CNPJ - Fornecedor</th>
                                <th>Almoxarifado</th>
                                <th>Obra</th>
                                <th>Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($entrances as $entrance)
                                <tr>
                                    <td>{{ \Carbon\Carbon::parse($entrance->created_at)->format('d/m/Y')}}</td>
                                    <td>{{ $entrance->doc }}</td>
                                    <td>{{ $entrance->entranceType['name'] }}</td>
                                    <td>
                                        @if($entrance->provider["id"] != null)
                                        <a href="{{ route("user.fornecedor.show", $entrance->provider["id"]) }}">
                                        {{ $entrance->provider['doc'] }} - {{ $entrance->provider['name'] }}
                                        </a>
                                        @endif
                                    </td>
                                    <!--td>{{ /*$entrance->tool['cod']*/null }} - {{ /*$entrance->tool['name']*/ null }}</td-->
                                    <td>
                                        @if($entrance->warehouse["id"] != null)
                                        <a href="{{ route("user.almoxarifado.show", $entrance->warehouse["id"]) }}">
                                        {{ $entrance->warehouse['name'] }}
                                        </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($entrance->warehouse['building']["id"] != null)
                                        <a href="{{ route("user.obra.show", $entrance->warehouse['building']["id"]) }}">
                                            {{ $entrance->warehouse['building']['name'] }}
                                        </a>
                                        @endif
                                    </td>
                                    <!--td>
                                        @if($entrance->quantity > 0)
                                            {{ /*$entrance->quantity*/ null }} {{ /*$entrance->tool['unity']['name']*/ null }}
                                        @endif
                                    </td-->
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="{{ route("user.entrada.show", $entrance->id) }}">
                                                    <button class="btn btn-sm btn-info" title="Visualizar Entrada">
                                                        <span class="fa fa-eye"></span>
                                                    </button>
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('pre_scripts')
    <!--
    <link href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" rel="stylesheet" type="text/css"/>
    <link href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css" rel="stylesheet" rel="stylesheet" type="text/css"/>
    <link href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css" rel="stylesheet" rel="stylesheet" rel="stylesheet" type="text/css"/>
    -->
@endsection

@section('post_scripts')
    <link href="/assets/plugins/datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/datatables/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>

    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>

    <script src="/assets/plugins/datatables/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/jszip.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/pdfmake.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/vfs_fonts.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.print.min.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            var table = $('.datatable').dataTable({
                dom: 'lBfrtip',
                responsive: true,
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10 linhas', '25 linhas', '50 linhas', '100 linhas', 'Mostrar Todos' ]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        text: 'Exportar',
                        buttons: [
                            {
                                extend: 'copy',
                                text: 'Copiar',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5 ]
                                }
                            },
                            {
                                extend: 'print',
                                text: 'Imprimir',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5 ]
                                }
                            },
                            {
                                extend: 'excel',
                                text: 'Excel',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5 ]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: 'PDF',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5 ]
                                }
                            },
                            {
                                extend: 'csv',
                                text: 'CSV',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5 ]
                                }
                            },
                        ]
                    },
                ],
                "language": {
                    buttons: {
                        copyTitle: 'Capiado para área de transferência',
                        copySuccess: {
                            _: '%d linhas copiadas',
                            1: '1 linha copiada'
                        }
                    },
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection

