@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Tipos Arquivadas</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Tipos</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Todos os Tipos</h2>
                <div class="actions panel_actions pull-right">
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    @if(session()->get('success'))
                        <div class="alert alert-success alert-dismissible fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <strong>Sucesso!</strong> {{ session()->get('success') }}
                        </div>
                    @endif
                    <div class="col-xs-12">

                        <table class="table datatable display responsive no-wrap" width="100%">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($itemTypes as $itemType)
                                <tr>
                                    <td>{{ $itemType->name }}</td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="{{ route("admin.tipoitem.show", $itemType->id) }}">
                                                    <button class="btn btn-sm btn-info" title="Visualizar Tipo">
                                                        <span class="fa fa-eye"></span>
                                                    </button>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route("admin.tipoitem.edit", $itemType->id) }}">
                                                    <button class="btn btn-sm btn-accent" title="Editar Tipo">
                                                        <span class="fa fa-edit"></span>
                                                    </button>
                                                </a>
                                            </li>
                                            <li>
                                                <form action="{{ route('admin.tipoitem.restore', $itemType->id)}}" method="post">
                                                    @csrf
                                                    <meta name="_token" content="{{ csrf_token() }}">
                                                    @method('POST')
                                                    <button class="btn btn-sm btn-secondary" title="Restaurar Tipo"
                                                            data-toggle="confirm"
                                                            data-title=""
                                                            data-message="Tem certeza que deseja restaurar esse tipo para itens?"
                                                            data-type="success">
                                                        <span class="fa fa-recycle"></span>
                                                    </button>
                                                </form>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            $('.datatable').dataTable({
                responsive: true,
                "language": {
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection
