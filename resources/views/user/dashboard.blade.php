@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Dashboard</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Dashboard</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box nobox">
            <div class="content-body">
                <div class="row">
                    <style>
                        .panel-body .btn:not(.btn-block) { width:120px;margin-bottom:10px; }
                        .panel-heading { padding: 10px; }
                    </style>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <span class="fa fa-bookmark"></span> Atalhos</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                                <a href="#" class="btn btn-corner btn-primary btn-sm" role="button">
                                                    <span class="fa fa-plus"></span>
                                                    <small><br/>Nova<br/>Entrada</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-primary btn-sm" role="button">
                                                    <span class="fa fa-minus"></span>
                                                    <small><br/>Nova<br/>Retirada</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-primary btn-sm" role="button">
                                                    <span class="fa fa-id-badge"></span>
                                                    <small><br/>Novo<br/>Funcionário</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-primary btn-sm" role="button">
                                                    <span class="fa fa-truck-loading"></span>
                                                    <small><br/>Novo<br/>Fornecedor</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-primary btn-sm" role="button">
                                                    <span class="fa fa-barcode"></span>
                                                    <small><br/>Novo<br/>Item</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-primary btn-sm" role="button">
                                                    <span class="fa fa-warehouse"></span>
                                                    <small><br/>Todos os<br/>Almoxarifados</small>
                                                </a>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                                <a href="#" class="btn btn-corner btn-danger btn-sm" role="button">
                                                    <span class="fa fa-file-invoice"></span>
                                                    <small><br/>Todas as<br/>Faturas</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-info btn-sm" role="button">
                                                    <span class="fa fa-exchange-alt"></span>
                                                    <small><br/>Nova<br/>Transferência</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-success btn-sm" role="button">
                                                    <span class="fa fa-undo-alt"></span>
                                                    <small><br/>Todas as<br/>Devoluções</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-warning btn-sm" role="button">
                                                    <span class="fa fa-clock"></span>
                                                    <small><br/>Aguardando<br/>Aprovação</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-primary btn-sm" role="button">
                                                    <span class="fa fa-undo-alt"></span>
                                                    <small><br/>Todas as<br/>Devoluções</small>
                                                </a>
                                                <a href="#" class="btn btn-corner btn-warning btn-sm" role="button">
                                                    <span class="fa fa-clock"></span>
                                                    <small><br/>Aguardando<br/>Aprovação</small>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
