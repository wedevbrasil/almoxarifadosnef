@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Fornecedor - {{ $provider->name }}</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li>
                    <a href="#"></i>Fornecedores</a>
                </li>
                <li class="active">
                    <strong>{{ $provider->name }}</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Fornecedor</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">CNPJ</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="doc" value="{{ $provider->doc }}" readonly disabled >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Nome</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="name" value="{{ $provider->name }}" readonly disabled >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Endereço</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $provider->address }}" name="address" readonly disabled >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Contato</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $provider->contact }}" name="contact" readonly disabled >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Total de Entradas</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $provider->entrances->count() }}" name="address" readonly disabled >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#entrances" data-toggle="tab">
                    <i class="fa fa-plus"></i> Entradas
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade in active" id="entrances">
                <table class="table datatable display responsive no-wrap" width="100%">
                    <thead>
                    <tr>
                        <th>Data de Entrada</th>
                        <th>Nº Nota Fiscal</th>
                        <th>CNPJ - Fornecedor</th>
                        <th>Almoxarifado</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($provider->entrances as $entrance)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($entrance->created_at)->format('d/m/Y')}}</td>
                            <td>{{ $entrance->doc }}</td>
                            <td>{{ $entrance->provider['doc'] }} - {{ $entrance->provider['name'] }}</td>
                            <td>{{ $entrance->warehouse['name'] }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li>
                                        <a href="{{ route("admin.entrada.show", $entrance->id) }}">
                                            <button class="btn btn-sm btn-info" title="Visualizar Entrada">
                                                <span class="fa fa-eye"></span>
                                            </button>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('post_scripts')
    <link href="/assets/plugins/datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/datatables/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>

    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>

    <script src="/assets/plugins/datatables/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/jszip.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/pdfmake.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/vfs_fonts.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.print.min.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            var table = $('.datatable').dataTable({
                dom: 'lBfrtip',
                responsive: true,
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10 linhas', '25 linhas', '50 linhas', '100 linhas', 'Mostrar Todos' ]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        text: 'Exportar',
                        buttons: [
                            {
                                extend: 'copy',
                                text: 'Copiar',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 5 ]
                                }
                            },
                            {
                                extend: 'print',
                                text: 'Imprimir',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 5 ]
                                }
                            },
                            {
                                extend: 'excel',
                                text: 'Excel',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 5 ]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: 'PDF',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 5 ]
                                }
                            },
                            {
                                extend: 'csv',
                                text: 'CSV',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 5 ]
                                }
                            },
                        ]
                    },
                ],
                "language": {
                    buttons: {
                        copyTitle: 'Capiado para área de transferência',
                        copySuccess: {
                            _: '%d linhas copiadas',
                            1: '1 linha copiada'
                        }
                    },
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection

