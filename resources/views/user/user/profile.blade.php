@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Perfil do Usuário</h1><!-- PAGE HEADING TAG - END -->
        </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Perfil do Usuário</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Dados Pessoais</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="container">
                            <h1>Editar Perfil</h1>
                            <hr>
                            <div class="row">
                                <!-- left column -->
                                <div class="col-md-3">
                                    <div class="text-center">
                                        <img src="/assets/images/default-user.png" class="avatar img-circle" alt="avatar" width="100" height="100">
                                        <h6>Atualizar foto de perfil</h6>

                                        <input type="file" class="form-control">
                                    </div>
                                </div>

                                <!-- edit form column -->
                                <div class="col-md-9 personal-info">
                                    <h3>Informações Pessoais</h3>

                                    <form class="form-horizontal" role="form" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Nome:</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" value="{{ $user->employee['name'] }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Sobrenome:</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" value="{{ $user->employee['lastname'] }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Obra:</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" value="{{ $user->employee['building']['name'] }}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Contato:</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" value="{{ $user->employee['contact'] }}" data-mask="(99) 9 9999-999[9]">
                                            </div>
                                        </div>
                                        <h3>Dados de Acesso</h3>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Email/Usuário de acesso:</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" type="text" value="{{ $user->username }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Senha:</label>
                                            <div class="col-md-8">
                                                <p class="bg-info">Deixe em branco para não alterar a senha</p>
                                                <input class="form-control" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Confirmar senha:</label>
                                            <div class="col-md-8">
                                                <input class="form-control" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"></label>
                                            <div class="col-md-8">
                                                <input type="button" class="btn btn-primary" value="Salvar Alterações">
                                                <span></span>
                                                <input type="reset" class="btn btn-default" value="Cancelar">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <script src="/assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
@endsection
