@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Unidades</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Visualizar Unidade</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Unidade</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Nome</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="name" value="{{ $unity->name }}" readonly disabled >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="controls">
                                        <button class="btn btn-primary" onclick="history.back(-1)">Voltar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Funcionários</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">

                            <table class="table datatable display responsive no-wrap" width="100%">
                                <thead>
                                <tr>
                                    <th>CPF</th>
                                    <th>Nome</th>
                                    <th>Setor</th>
                                    <th>Unidade</th>
                                    <th>Obra</th>
                                    <th>Usuário</th>
                                    <th>Contato</th>
                                    <th>Opções</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($unity->employees != null)
                                @foreach($unity->employees as $employee)
                                    <tr>
                                        <td>{{ $employee->doc }}</td>
                                        <td>{{ $employee->name }}</td>
                                        <td>{{ $employee->sector['name'] }}</td>
                                        <td>{{ $employee->unity['name'] }}</td>
                                        <td>
                                            <a href="{{ route("admin.obra.show", $employee->building["id"]) }}">
                                                {{ $employee->building['name'] }}
                                            </a>
                                        </td>
                                        <td>{{ $employee->user['username'] }}</td>
                                        <td>{{ $employee->contact }}</td>
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{ route("admin.funcionario.show", $employee->id) }}">
                                                        <button class="btn btn-sm btn-info" title="Visualizar Funcionário">
                                                            <span class="fa fa-eye"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route("admin.funcionario.edit", $employee->id) }}">
                                                        <button class="btn btn-sm btn-accent" title="Editar Funcionário">
                                                            <span class="fa fa-edit"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route("admin.funcionario.user.edit", $employee->id) }}">
                                                        <button class="btn btn-sm btn-success" title="Adcionar/Editar Usuário">
                                                            <span class="fa fa-user"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                                <li>
                                                    <form action="{{ route('admin.funcionario.destroy', $employee->id)}}" method="post">
                                                        @csrf
                                                        <meta name="_token" content="{{ csrf_token() }}">
                                                        @method('DELETE')
                                                        <button class="btn btn-sm btn-danger" title="Desativar Funcionário"
                                                                data-toggle="confirm"
                                                                data-title=""
                                                                data-message="Tem certeza que deseja desativar esse funcionário?"
                                                                data-type="danger">
                                                            <span class="fa fa-trash"></span>
                                                        </button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            $('.datatable').dataTable({
                responsive: true,
                "language": {
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection
