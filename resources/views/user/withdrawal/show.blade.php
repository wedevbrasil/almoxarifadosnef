@extends('layouts.app')

@section('pre_scripts')
    <link href="{{ asset('/assets/plugins/icheck/skins/square/_all.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Visualizar Retirada</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Visualizar Retirada</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Dados da Retirada</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="row">
                            @if(session()->get('error'))
                                <div class="alert alert-danger alert-dismissible fade in">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <strong>Erro!</strong> {{ session()->get('error') }}
                                </div>
                            @endif
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Tipo de Retirada</label>
                                    <div class="controls">
                                        <input type="text" class="form-control"
                                               value="{{ $withdrawal->withdrawalType->name }}"
                                               readonly disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Código Interno</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $withdrawal->doc }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Almoxarifado - Obra</label>
                                    <div class="controls">
                                        <input type="text" class="form-control"
                                               value="{{ $withdrawal->warehouse['name'] }} - {{ $withdrawal->warehouse['building']['name'] }}"
                                               readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Data de Devolução</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $withdrawal->giveback_date }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Funcionário</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $withdrawal->employee['name'] }}" readonly disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Status</label>
                                    <div class="controls">
                                        <p class="uilabels text-lg">
                                            @if($withdrawal->status == 1)
                                            <span class="label label-warning"><i class='fa fa-clock-o'></i> Aguardando aprovação</span>
                                            @elseif($withdrawal->status == 2)
                                            <span class="label label-danger"><i class='fa fa-times'></i> Cancelado</span>
                                            @elseif($withdrawal->status == 3)
                                            <span class="label label-success"><i class='fa fa-thumbs-up'></i> Aprovado</span>
                                            @elseif($withdrawal->status == 5)
                                            <span class="label label-primary"><i class='fa fa-undo-alt'></i> Devolvido</span>
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12">
        <ul class="nav nav-tabs content-tabs" id="maincontent" role="tablist">
            @if($withdrawal->status != 5)
            <li class="active">
                <a href="#itens" data-toggle="tab">
                    <i class="fa fa-plus"></i> Itens Retirados
                </a>
            </li>
            @endif

            @if($withdrawal->status == 5)
            <li class="active">
                <a href="#itens-givedback" data-toggle="tab">
                    <i class="fa fa-plus"></i> Itens Devolvidos
                </a>
            </li>
            @endif
        </ul>

        <div class="tab-content">
            @if($withdrawal->status != 5)
            <div class="tab-pane fade in active" id="itens">
                <table id="itemTable" class="table order-list">
                    <thead>
                    <tr><td>Código Produto</td>
                        <td>Descrição</td>
                        <td>Tipo</td>
                        <td>Quantidade</td>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    @foreach($itens as $i => $item)
                        <tr>
                            <td>{{ $item->cod }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->itemType->name }}</td>
                            <td>{{ $withdrawal->quantity[$i] }} {{ $item->unity['name'] }}(s)</td>
                        </tr>
                    @endforeach
                    <tr>
                    </tr>
                    </tfoot>
                </table>
            </div>
            @endif

            @if($withdrawal->status == 5)
            <div class="tab-pane fade in active" id="itens-givedback">
                <table id="itemTable" class="table order-list">
                    <thead>
                    <tr>
                        <td>Código Produto</td>
                        <td>Descrição</td>
                        <td>Tipo</td>
                        <td>Quantidade Retirada</td>
                        <td>Quantidade Devolvida</td>
                        <td>Defeito</td>
                        <td>Observação</td>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    @foreach($withdrawal->tool_giveback as $i => $item_givedback)
                        <tr><td>{{ $item_givedback['cod'] }}</td>
                            <td>{{ $item_givedback['name'] }}</td>
                            <td>{{ $item_givedback['typename'] }}</td>
                            <td>{{ $withdrawal->quantity[$i] }} {{ $itens[$i]->unity['name'] }}(s)</td>
                            <td>{{ $item_givedback['quantity'] }} {{ $itens[$i]->unity['name'] }}(s)</td>
                            <td>
                                @if($item_givedback['status'] == 'no_broken')
                                    Não
                                @elseif($item_givedback['status'] == 'broken')
                                    Sim <small>{{ $item_givedback['which_defect'] }}</small>
                                @endif
                            </td>
                            <td>{{ $item_givedback['obs_iten'] }}</td>
                        </tr>
                    @endforeach
                    <tr>
                    </tr>
                    </tfoot>
                </table>
            </div>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="controls pull-right">
            <br />
            <form action="{{ route('user.retirada.cancel', $withdrawal->id)}}" method="post" class="form-horizontal">
                @csrf
                <meta name="_token" content="{{ csrf_token() }}">
                @method('POST')
                <div class="col-md-12">
                    @if($withdrawal->status == 1)
                        <a data-toggle="modal" href="#employee_confirmation" data-backdrop="static" data-keyboard="false">
                            <button type="button" class="btn btn-success" title="Aprovar Retirada">
                                <span class="fa fa-check-circle"></span>
                                Aprovar Retirada</button></a>
                        <button class="btn btn-warning" title="Cancelar Retirada"
                                data-toggle="confirm"
                                data-title=""
                                data-message="Tem certeza que deseja cancelar essa retirada?"
                                data-type="warning">
                            <span class="fa fa-ban"></span>
                            Cancelar Retirada
                        </button>
                    @elseif($withdrawal->status == 2)
                        <p class="uilabels text-lg">
                            Sem opções para essa retirada
                        </p>
                    @elseif($withdrawal->status == 3)
                        <p class="uilabels text-lg">
                            <a data-toggle="modal" href="#withdrawal_giveback" data-backdrop="static" data-keyboard="false">
                                <button type="button" class="btn btn-primary" title="Devolver Retirada">
                                    <span class="fa fa-undo-alt"></span>
                                    Devolução de Retirada</button></a>
                        </p>
                    @elseif($withdrawal->status == 5)
                        <p class="uilabels text-lg">
                            Sem opções para essa retirada
                        </p>
                    @endif
                </div>
            </form>
        </div>
    </div>

    @if($withdrawal->status == 1)
    <!-- modal start -->
    <form action="{{ route('user.retirada.approve', $withdrawal->id)}}" method="post">
        <div class="modal fade col-xs-12" id="employee_confirmation" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Aprovação do Funcionario</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-10 col-sm-10 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Funcionário</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $withdrawal->employee['name'] }}" readonly disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Itens</label>
                                    <div class="controls">
                                        <ul class="list">
                                        @foreach($itens as $i => $item)
                                            <li>{{ $item->name }} - {{ $withdrawal->quantity[$i] }} {{ $item->unity['name'] }}(s)</li>
                                        @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Pin do Funcionário</label>
                                    <div class="controls">
                                    @if($withdrawal->employee['user']['pin'] == null)
                                        <p class="uilabels text-lg">
                                            <span class="label label-warning"></i>Funcionário sem PIN registrado.</span>
                                        </p>
                                        <a href="{{ route("user.funcionario.user.edit", $withdrawal->employee['id']) }}">
                                        Clique aqui para registrar um pin para esse funcionário.
                                        </a>
                                    @else
                                        <input type="number" class="form-control" id="pin" name="pin" data-mask="9999" placeholder="Pin Numérico" required>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        @if($withdrawal->employee['user']['pin'] != null)
                        <button type="submit" class="btn btn-success" type="button"> Aprovar</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- modal end -->

    @elseif($withdrawal->status == 3)
    <!-- modal start -->
    <form action="{{ route('user.retirada.giveback', $withdrawal->id)}}" method="post">
        <input type="hidden" name="doc" value="{{ $withdrawal->doc }}">
        <input type="hidden" name="employee" value="{{ $withdrawal->employee['id'] }}">
        <input type="hidden" name="warehouse" value="{{ $withdrawal->warehouse['id'] }}">
        <input type="hidden" name="provider" value="{{ $withdrawal->provider['id'] }}">


        @if(!empty($itens) && !empty($withdrawal->quantity))
            @foreach($withdrawal->tool_ids as $i => $item)
            <input type="hidden" name="quantity[{{$i}}]" value="{{ $withdrawal->quantity[$i] }}">
            <input type="hidden" name="tool[{{$i}}]" value="{{ $item }}">
            @endforeach
        @endif


        <div class="modal fade col-12" id="withdrawal_giveback" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Devolução de Retirada</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif
                            <div class="col-12">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Funcionário</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $withdrawal->employee['name'] }}" readonly disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Motivo da Devolução</label>
                                    <div class="controls">
                                        <ul class="list list-inline">

                                            <li>
                                                <input tabindex="5" type="radio" name="giveback_reason" class="skin-square-green" value="finished_lease" checked>
                                                <label class="icheck-label form-label" for="square-checkbox-2">Término da Locação</label>
                                            </li>
                                            <li>
                                                <input tabindex="5" type="radio" name="giveback_reason" class="skin-square-green" value="broken">
                                                <label class="icheck-label form-label" for="square-checkbox-2">Defeito</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Itens</label>
                                    <div class="controls">
                                        <table id="itemTable" class="table order-list">
                                            <thead>
                                            <tr>
                                                <td>Código Produto - Descrição - Tipo</td>
                                                <td>Quantidade devolvida</td>
                                                <td>Estado</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($itens as $i => $item)
                                                @php $lastId = $i @endphp
                                                <input type="hidden" name="tool_giveback[{{$i}}][id]" required value="{{ $item->id }}">
                                                <input type="hidden" name="tool_giveback[{{$i}}][name]" required value="{{ $item->name }}">
                                                <input type="hidden" name="tool_giveback[{{$i}}][cod]" required value="{{ $item->cod }}">
                                                <input type="hidden" name="tool_giveback[{{$i}}][typename]" required value="{{ $item->itemType->name }}">
                                                <tr>
                                                    <td>
                                                        {{ $item->cod }} - {{ $item->name }} -  {{ $item->itemType->name }}
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="tool_giveback[{{$i}}][quantity]" required value="{{ $withdrawal->quantity[$i] }}"
                                                        style="width: 80px">
                                                    </td>
                                                    <td>
                                                        <div class="radio">
                                                            <label>
                                                                <input class="skin-square-green" type="radio" name="tool_giveback[{{$i}}][status]" value="no_broken" checked>
                                                                Não possui defeitos
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input class="skin-square-red" type="radio" name="tool_giveback[{{$i}}][status]" value="broken">
                                                                Possui defeitos
                                                                <input placeholder="Quais defeitos?" type="text" class="form-control text-nowrap" id="tool_giveback[{{$i}}][which_defect]" name="tool_giveback[{{$i}}][defect]"  data-rule-required="true" contenteditable="false">
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                      <textarea class="form-control autogrow" name=tool_giveback[{{$i}}][obs_iten]" placeholder="Observação no item" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 50px;"></textarea>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Observação</label>
                                    <div class="controls">
                                        <textarea class="form-control autogrow" cols="5" name="observation" id="observation" placeholder="Observações na devolução" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 50px;"></textarea>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Senha Almoxarife</label>
                                    <div class="controls">
                                        <input type="password" class="form-control" id="password" name="password" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                        <button type="submit" class="btn btn-primary" type="button"> Aprovar Devolução</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- modal end -->
    @endif

@endsection

@section('post_scripts')

    <script src="/assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->

    <script src="/assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script>
        $('#employee_confirmation').on('shown.bs.modal', function () {
            setTimeout(function (){
                $('#pin').focus();
            }, 1000);
        });

        @if(app('request')->input('employee_confirmation') == true)
        $('#employee_confirmation').modal('show');
        @endif

        @if(app('request')->input('withdrawal_giveback') == true)
        $('#withdrawal_giveback').modal('show');
        @endif

    </script>
@endsection
