@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Funcionários</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Editar Funcionário</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Editar Funcionário</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form action="{{ route("admin.funcionario.update", $employee->id) }}" method="post">
                                    @method('PATCH')
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">CPF</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="doc" value="{{ $employee->doc }}" readonly disabled>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Nome Completo</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="name" value="{{ $employee->name }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Função</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="function" value="{{ $employee->function }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Setor</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="sector" value="{{ $employee->sector }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Encarregado</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="commissionaire" value="{{ $employee->commissionaire }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Supervisão</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="supervision" value="{{ $employee->supervision }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Contato</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="contact" value="{{ $employee->contact }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <button class="btn btn-primary">Salvar</button>
                                            <button class="btn btn-danger" type="button" onclick="history.back(-1)">Cancelar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
