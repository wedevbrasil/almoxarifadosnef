@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Retiradas</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Retiradas</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Todos as Retiradas</h2>
                <div class="actions panel_actions pull-right">
                    <a href="{{ route("user.retirada.create") }}">
                        <button type="button" class="btn btn-primary btn-icon btn-sm">
                            <i class="fa fa-plus"></i> &nbsp; <span>Adicionar</span>
                        </button>
                    </a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">
                        @if(session()->get('success'))
                            <div class="alert alert-success alert-dismissible fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Sucesso!</strong> {{ session()->get('success') }}
                            </div>
                        @endif
                        @if(session()->get('error'))
                            <div class="alert alert-danger alert-dismissible fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Erro!</strong> {{ session()->get('error') }}
                            </div>
                        @endif

                        <table class="table datatable display responsive no-wrap" width="100%">
                            <thead>
                                <tr>
                                    <th>Data de Retirada</th>
                                    <th>Código Interno</th>
                                    <th>Funcionário</th>
                                    <th>Almoxarifado</th>
                                    <th>Obra</th>
                                    <th>Status</th>
                                    <th>Tipo</th>
                                    <th>Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($withdrawals as $withdrawal)
                                <tr>
                                    <td>{{ \Carbon\Carbon::parse($withdrawal->created_at)->format('d/m/Y')}}</td>
                                    <td>{{ $withdrawal->doc }}</td>
                                    <td>
                                        @if(!empty($withdrawal->employee["id"]))
                                        <a href="{{ route("user.funcionario.show", $withdrawal->employee["id"]) }}">
                                            {{ $withdrawal->employee['name'] }}
                                        </a>
                                        @else
                                            Sem funcionário para essa retirada
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route("user.almoxarifado.show", $withdrawal->warehouse["id"]) }}">
                                        {{ $withdrawal->warehouse['name'] }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route("user.obra.show", $withdrawal->warehouse['building']["id"]) }}">
                                            {{ $withdrawal->warehouse['building']['name'] }}
                                        </a>
                                    </td>
                                    <td>
                                        @if($withdrawal->status == 1)
                                        <span class="label label-warning"><i class='fa fa-clock-o'></i> Aguardando Aprovação</span>
                                        @elseif($withdrawal->status == 2)
                                        <span class="label label-danger"><i class='fa fa-times'></i> Cancelado</span>
                                        @elseif($withdrawal->status == 3)
                                            <span class="label label-success"><i class='fa fa-thumbs-up'></i> Aprovado</span>
                                        @elseif($withdrawal->status == 5)
                                            <span class="label label-primary"><i class='fa fa-undo-alt'></i> Devolvida</span>
                                        @endif
                                    </td>
                                    <td>
                                        <span class="badge">{{ $withdrawal->withdrawalType->name }}</span>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="{{ route("user.retirada.show", $withdrawal->id) }}">
                                                    <button class="btn btn-sm btn-info" title="Visualizar Retirada">
                                                        <span class="fa fa-eye"></span>
                                                    </button>
                                                </a>
                                            </li>
                                            @if($withdrawal->status == 1)
                                                <li>
                                                    <a href="/user/retirada/{{ $withdrawal->id }}?employee_confirmation=true">
                                                        <button class="btn btn-sm btn-success" title="Aprovar Retirada">
                                                            <span class="fa fa-check-circle"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                                <li>
                                                    <form action="{{ route('user.retirada.cancel', $withdrawal->id)}}" method="post">
                                                        @csrf
                                                        <meta name="_token" content="{{ csrf_token() }}">
                                                        @method('POST')
                                                        <button class="btn btn-sm btn-warning" title="Cancelar Retirada"
                                                                data-toggle="confirm"
                                                                data-title=""
                                                                data-message="Tem certeza que deseja cancelar essa retirada?"
                                                                data-type="warning">
                                                            <span class="fa fa-ban"></span>
                                                        </button>
                                                    </form>
                                                </li>
                                            @elseif($withdrawal->status == 2)
                                                <li>
                                                    <form action="{{ route('user.retirada.destroy', $withdrawal->id)}}" method="post">
                                                        @csrf
                                                        <meta name="_token" content="{{ csrf_token() }}">
                                                        @method('DELETE')
                                                        <button class="btn btn-sm btn-danger" title="Desativar Retirada"
                                                                data-toggle="confirm"
                                                                data-title=""
                                                                data-message="Tem certeza que deseja desativar essa retirada?"
                                                                data-type="danger">
                                                            <span class="fa fa-trash"></span>
                                                        </button>
                                                    </form>
                                                </li>
                                            @elseif($withdrawal->status == 3)
                                                <li>
                                                    <a href="/user/retirada/{{ $withdrawal->id }}?withdrawal_giveback=true">
                                                        <button class="btn btn-sm btn-primary" title="Devolver Retirada">
                                                            <span class="fa fa-undo-alt"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <link href="/assets/plugins/datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/datatables/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>

    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>

    <script src="/assets/plugins/datatables/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/jszip.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/pdfmake.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/vfs_fonts.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.print.min.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            var table = $('.datatable').dataTable({
                dom: 'lBfrtip',
                responsive: true,
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10 linhas', '25 linhas', '50 linhas', '100 linhas', 'Mostrar Todos' ]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        text: 'Exportar',
                        buttons: [
                            {
                                extend: 'copy',
                                text: 'Copiar',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'print',
                                text: 'Imprimir',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'excel',
                                text: 'Excel',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: 'PDF',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'csv',
                                text: 'CSV',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                        ]
                    },
                ],
                "language": {
                    buttons: {
                        copyTitle: 'Capiado para área de transferência',
                        copySuccess: {
                            _: '%d linhas copiadas',
                            1: '1 linha copiada'
                        }
                    },
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection
