@extends('layouts.app')

@section('pre_scripts')
    <link href="/assets/plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/assets/plugins/switchery/dist/switchery.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/assets/plugins/icheck/skins/square/_all.css" rel="stylesheet" type="text/css" media="screen"/>
@endsection

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Nova Retirada</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="">
                    <a href="#">Retirada</a>
                </li>
                <li class="active">
                    <a href="#"><strong>Nova Retirada</strong></a>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <form id="user_witdrawal_create" action="{{ route("user.retirada.store") }}" method="post">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">Retirada</h2>
                    <div class="actions panel_actions pull-right">
                        <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <div class="col-md-4 col-sm-5 col-xs-10">
                                @method('post')
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Tipo de Retirada</label>
                                            <div class="controls">
                                                <select class="select2" id="type" name="type" onchange="withdrawalType()">
                                                    <option value="">Selecione um tipo</option>
                                                    @foreach($withdrawalTypes as $withdrawalType)
                                                        <option value="{{ $withdrawalType->id }}" @if( $withdrawalType->id == old("type")) selected @endif>{{ $withdrawalType->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Controle Interno</label>
                                        <div class="controls">
                                            <div class="input-group">
                                                <input id="output" type="text" class="form-control" name="doc" value="{{ old("doc") }}">
                                                <div class="input-group-btn">
                                                    <button id="generate" type="button" class="btn btn-default">
                                                        <i class="fa fa-sync-alt"></i> Gerar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-5 col-xs-10">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Almoxarifado de Origem</label>
                                        <div class="controls">
                                            <select class="select2" id="warehouse-source" name="warehouse-source" required>
                                                <option value="">Selecione um Almoxarifado</option>
                                                @foreach($warehouses as $warehouse)
                                                    <option value="{{ $warehouse->id }}" {{ ( old("warehouse") == $warehouse->id ? "selected":"") }}>{{ $warehouse->name }} - {{ $warehouse->building['number'] }} - {{ $warehouse->building['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-5 col-xs-10">

                                    <div class="form-group" id="employee-div">
                                        <label class="form-label" for="field-1">Funcionário</label>
                                        <div class="controls">
                                            <select class="select2" id="employee" name="employee">
                                                <option value="">Selecione um Funcionário</option>
                                                @foreach($employees as $employee)
                                                    <option value="{{ $employee->id }}" {{ ( old("employee") == $employee->id ? "selected":"") }}>{{ $employee->name }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" id="employee-perm-div">
                                        <label class="form-label" for="field-1">Pedir aprovação do Funcionário ao final?</label>
                                        <div class="controls">
                                            <div class="form-block bottom15"><input type="checkbox" id="employee_aprovation" class="js-switch" name="employee_aprovation" /></div>
                                        </div>
                                    </div>

                                    <div class="form-group" id="warehouse-destination-div">
                                        <label class="form-label" for="field-1">Almoxarifado de Destino</label>
                                        <div class="controls">
                                            <select class="select2" id="warehouse-destination" name="warehouse-destination">
                                                <option value="">Selecione um Almoxarifado</option>
                                                @foreach($warehousesDestination as $warehouse)
                                                    <option value="{{ $warehouse->id }}" {{ ( old("warehouse") == $warehouse->id ? "selected":"") }}>{{ $warehouse->name }} - {{ $warehouse->building['number'] }} - {{ $warehouse->building['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" id="invoice-perm-div">
                                        <label class="form-label" for="field-1">Gerar fatura ao final?</label>
                                        <div class="controls">
                                            <div class="form-block bottom15">
                                                <input type="checkbox" id="invoice_aprovation" class="js-switch" name="invoice_aprovation" checked />
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">Produtos</h2>
                    <div class="actions panel_actions pull-right">
                        <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12">
                            <table id="itemTable" class="table table-bordered table-responsive order-list">
                                <thead>
                                    <tr>
                                        <td>Código Produto - Descrição - Tipo</td>
                                        <td>Valor Unitáio</td>
                                        <td>Quantidade</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody id="dynamic-container">
                                    <td>
                                        <select class="select2" name="tool[0]" required>
                                            <option value="">Selecione uma Ferramenta</option>
                                            @foreach($tools as $tool)
                                                <option value="{{ $tool->id }}">{{ $tool->cod }} - {{ $tool->name }} - {{ $tool->itemType['name'] }}</option>\n' +
                                            @endforeach
                                        </select>
                                    </td>

                                    <td style="width: 200px">
                                        <input type="text" class="form-control" name="valor[0]"
                                               data-mask="currency" data-sign="R$" required>
                                    </td>

                                    <td style="width: 200px">
                                        <input type="number" class="form-control" name="quantity[0]"
                                               min="1" required>
                                    </td>
                                    <td style="width: 110px">
                                        <button type="button" class="ibtnDel btn btn-md btn-danger pull-right" disabled>
                                            <i class="fa fa-minus-circle"></i> Remover
                                        </button>
                                    </td>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5" style="text-align: left;">
                                        <button type="button" class="btn btn-lg btn-block " id="addrow">
                                            <i class="fa fa-plus-circle"></i>  Adicionar Item
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <div class="form-group">
                <div class="controls pull-right">
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-save"></i> Salvar
                    </button>
                    <button class="btn btn-danger" type="button" onclick="history.back(-1)">
                        <i class="fa fa-ban"></i> Cancelar
                    </button>
                </div>
            </div>

        </form>
    </div>
@endsection

@section('post_scripts')
    <script src="/assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/switchery/dist/switchery.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            $(function() {
                var counter = 0;
                $("#addrow").on("click", function () {
                    counter++;

                    var newRow = $("<tr>");
                    var cols = "";

                    cols += '<td>' +
                        '<select class="select2" name="tool[' + counter + ']" required>\n' +
                        '   <option value="">Selecione uma Ferramenta</option>\n' +
                        '       @foreach($tools as $tool)\n' +
                        '           <option value="{{ $tool->id }}">{{ $tool->cod }} - {{ $tool->name }} - {{ $tool->itemType['name'] }}</option>\n' +
                        '       @endforeach\n' +
                        '</select>' +
                        '</td>';

                    cols += '<td>' +
                            '<input type="text" class="form-control" name="valor[' + counter + ']"' +
                            'data-mask="currency" data-sign="R$" required>' +
                        '</td>';

                    cols += '<td>' +
                        '<input type="text" class="form-control" name="quantity[' + counter + ']">'+
                        '</td>';

                    cols += '<td>' +
                        '<button type="button" class="ibtnDel btn btn-md btn-danger pull-right">' +
                        '<i class="fa fa-minus-circle"></i> Remover' +
                        '</td>';

                    newRow.append(cols);

                    $("#dynamic-container").append(newRow);
                    $('select.select2').select2();
                });


                $("table.order-list").on("click", ".ibtnDel", function (event) {
                    $(this).closest("tr").remove();
                });
            });
        });


        function withdrawalType() {
            if ($("#type").val() === "5d490461c70471340e0fd4ce")  {
                $("#warehouse-destination-div").show();
                $("#employee-div").hide();
                $("#employee-perm-div").hide();
                $("#giveback-date-div").hide();
                $("#invoice-perm-div").show();
            }
            else if ($("#type").val() === "5d4904b3c70471340e0fd4cf")  {
                $("#warehouse-destination-div").hide();
                $("#employee-div").show();
                $("#employee-perm-div").show();
                $("#giveback-date-div").show();
                $("#invoice-perm-div").hide();
            }
            else {
                $("#warehouse-destination-div").hide();
                $("#employee-div").show();
                $("#employee-perm-div").show();
                $("#giveback-date-div").hide();
                $("#invoice-perm-div").hide();
            }
        }

        $("#warehouse-destination-div").hide(); //Esconder almoxarifado destino no inicio
        $("#invoice-perm-div").hide();

        (function() {
            function IDGenerator() {

                this.length = 8;
                this.timestamp = +new Date;

                var _getRandomInt = function( min, max ) {
                    return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
                }

                this.generate = function() {
                    var ts = this.timestamp.toString();
                    var parts = ts.split( "" ).reverse();
                    var id = "";

                    for( var i = 0; i < this.length; ++i ) {
                        var index = _getRandomInt( 0, parts.length - 1 );
                        id += parts[index];
                    }

                    return id;
                }

            }


            document.addEventListener( "DOMContentLoaded", function() {
                var btn = document.querySelector( "#generate" ),
                    output = document.querySelector( "#output" );

                btn.addEventListener( "click", function() {
                    var generator = new IDGenerator();
                    output.value = generator.generate();

                }, false);

            });


        })();

    </script>
@endsection
