@extends('layouts.app')


@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Estoque</h1><!-- PAGE HEADING TAG - END -->
        </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Estoque</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Todos as Estoque</h2>
                <div class="actions panel_actions pull-right">
                    <a href="{{ route("user.ferramenta.create") }}">
                        <button type="button" class="btn btn-primary btn-icon btn-sm">
                            <i class="fa fa-plus"></i> &nbsp; <span>Adicionar</span>
                        </button>
                    </a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        @if(session()->get('success'))
                            <div class="alert alert-success alert-dismissible fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Sucesso!</strong> {{ session()->get('success') }}
                            </div>
                        @endif
                        <table class="table datatable display responsive no-wrap" width="100%">
                            <thead>
                            <tr>
                                <th>Código do Item</th>
                                <th>Descrição</th>
                                <th>Tipo</th>
                                <th>Unidade</th>
                                <th>Total de Entrada</th>
                                <th>Total de Retirada</th>
                                <th>Saldo</th>
                                <th>Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tools as $tool)
                                @php
                                    $qnte[$tool->id] = [];
                                    $qntw[$tool->id] = [];
                                    $qnt_ent[$tool->id] = 0;
                                    $qnt_ret[$tool->id] = 0;
                                @endphp
                                <tr>
                                    <td>{{ $tool->cod }}</td>
                                    <td>{{ $tool->name }}</td>
                                    <td>{{ $tool->itemType['name'] }}</td>
                                    <td>{{ $tool->unity['name'] }}</td>
                                    <td>
                                        @foreach ($tool->entrances as $key => $entrances)
                                                {{-- Achar a chave do array da ferramenta --}}
                                                @php $tool_id[$key] = array_search($tool->id, $entrances['tool_ids']) @endphp
                                                {{-- Novo array com as quantidades entrada da ferramenta --}}
                                                @php $qnte[$tool->id][$key] =  $entrances['quantity'][$tool_id[$key]] @endphp

                                            @if(isset($qnte[$tool->id])){{  $qnt_ent[$tool->id] = array_sum($qnte[$tool->id]) /* Soma das quantidades entrada */ }} {{ $tool->unity['name'] }}(s)@endif

                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach ($tool->withdrawals as $key => $withdrawals)
                                            {{-- Achar a chave do array da ferramenta --}}
                                            @php $tool_id[$key] = array_search($tool->id, $withdrawals['tool_ids']) @endphp
                                            {{-- Novo array com as quantidades retirada da ferramenta --}}
                                            @php $qntw[$tool->id][$key] =  $withdrawals['quantity'][$tool_id[$key]] @endphp

                                            @if(isset($qntw[$tool->id])){{  $qnt_ret[$tool->id] = array_sum($qntw[$tool->id]) /* Soma das quantidades retirada */ }} {{ $tool->unity['name'] }}(s)@endif

                                        @endforeach
                                    </td>
                                    <td> @if(isset($qnt_ent[$tool->id]) and isset($qnt_ret[$tool->id])){{ $qnt_ent[$tool->id] - $qnt_ret[$tool->id] }} {{ $tool->unity['name'] }}(s)@endif</td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="{{ route("user.ferramenta.show", $tool->id) }}">
                                                    <button class="btn btn-sm btn-info" title="Visualizar Item">
                                                        <span class="fa fa-eye"></span>
                                                    </button>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route("user.ferramenta.edit", $tool->id) }}">
                                                    <button class="btn btn-sm btn-accent" title="Editar Item">
                                                        <span class="fa fa-edit"></span>
                                                    </button>
                                                </a>
                                            </li>
                                            <li>
                                                <form action="{{ route('user.ferramenta.destroy', $tool->id)}}" method="post">
                                                    @csrf
                                                    <meta name="_token" content="{{ csrf_token() }}">
                                                    @method('DELETE')
                                                    <button class="btn btn-sm btn-danger" title="Desativar Item"
                                                            data-toggle="confirm"
                                                            data-title=""
                                                            data-message="Tem certeza que deseja desativar essa ferramenta?"
                                                            data-type="danger">
                                                        <span class="fa fa-trash"></span>
                                                    </button>
                                                </form>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <link href="/assets/plugins/datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/datatables/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>

    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>

    <script src="/assets/plugins/datatables/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/jszip.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/pdfmake.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/vfs_fonts.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.print.min.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            var table = $('.datatable').dataTable({
                dom: 'lBfrtip',
                responsive: true,
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10 linhas', '25 linhas', '50 linhas', '100 linhas', 'Mostrar Todos' ]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        text: 'Exportar',
                        buttons: [
                            {
                                extend: 'copy',
                                text: 'Copiar',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'print',
                                text: 'Imprimir',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'excel',
                                text: 'Excel',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: 'PDF',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'csv',
                                text: 'CSV',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                        ]
                    },
                ],
                "language": {
                    buttons: {
                        copyTitle: 'Capiado para área de transferência',
                        copySuccess: {
                            _: '%d linhas copiadas',
                            1: '1 linha copiada'
                        }
                    },
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection

