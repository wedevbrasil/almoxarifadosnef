@extends('layouts.app')

@php
if(!isset($lastIds)){
    $lastId = 0;
} else {
    $lastId = $lastId->cod;
}
@endphp

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Estoque</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Nova Item</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Nova Item</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form action="{{ route("admin.ferramenta.store") }}" method="post">
                                    @method('post')
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Código do Item</label>
                                        <div class="controls">
                                            <input type="number" class="form-control" name="cod"  value="{{ $lastId + 1}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Descrição</label>
                                        <span class="desc">ex: "ALAVANCA DE AÇO 1"X1,5 METROS"</span>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="name" value="{{ old("name") }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Tipo</label>
                                        <div class="controls">
                                            <select class="select2" id="type" name="type" required>
                                                <option value="">Selecione um Tipo</option>
                                                @foreach($itenTypes as $itenType)
                                                    <option value="{{ $itenType->id }}" {{ ( old("type") == $itenType->id ? "selected":"") }}>{{ $itenType->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <button class="btn btn-primary">Salvar</button>
                                            <button class="btn btn-danger" type="button" onclick="history.back(-1)">Cancelar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
