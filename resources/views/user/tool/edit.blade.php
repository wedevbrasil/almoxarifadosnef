@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Estoque</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Editar Item</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Editar Item</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form action="{{ route("admin.ferramenta.update", $tool->id) }}" method="post">
                                    @method('PATCH')
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Código da Item</label>
                                        <span class="desc">ex: "1548"</span>
                                        <div class="controls">
                                            <input type="number" class="form-control" name="cod" value="{{ $tool->cod }}" readonly disabled>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Descrição</label>
                                        <span class="desc">ex: "ALAVANCA DE AÇO 1"X1,5 METROS"</span>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="name" value="{{ $tool->name }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Tipo</label>
                                        <div class="controls">
                                            <select class="select2" id="type" name="type" >
                                                <option value="">Selecione um tipo</option>
                                                @foreach($itemTypes as $itemType)
                                                    <option value="{{ $itemType->id }}" @if($itemType->id == $tool->itemType['id']) selected @endif>{{ $itemType->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Tipo de Unidade</label>
                                        <div class="controls">
                                            <select class="select2" id="unity" name="unity" >
                                                <option value="">Selecione um tipo de unidade</option>
                                                @foreach($units as $unity)
                                                    <option value="{{ $unity->id }}" @if($unity->id == $tool->unity['id']) selected @endif>{{ $unity->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <button class="btn btn-primary">Salvar</button>
                                            <button class="btn btn-danger" type="button" onclick="history.back(-1)">Cancelar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
