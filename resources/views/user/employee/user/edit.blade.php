@extends('layouts.app')

@php
if(!isset($employee->user)){
 $role = 0;
 } else {
 $role = (string) $employee->user->role['role'];
}
@endphp

@section('pre_scripts')
    <link href="/assets/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" media="screen"/>
    <style>
        .access{
            display: none;
        }
    </style>
@endsection

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Usuário de Acesso</h1><!-- PAGE HEADING TAG - END --></div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li>
                    <a href="#">Funcionário</a>
                </li>
                <li class="active">
                    <strong>Usuário de Acesso</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Dados do Funcionário</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">CPF</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="doc" value="{{ $employee->doc }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Nome Completo</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="name" value="{{ $employee->name }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Função</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="function" value="{{ $employee->office['name'] }}" readonly disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Setor</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="sector" value="{{ $employee->sector['name'] }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Contato</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="contact" value="{{ $employee->contact }}" data-mask="(99) 9 9999-9999" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Obra</label>
                                    <span class="desc">ex: "Metrô - Transport - Linha 4"</span>
                                    <div class="controls">
                                        <select class="form-control disabled" id="building" name="building" readonly disabled>
                                            <option value="">Selecione uma Obra</option>
                                            @foreach($obras as $obra)
                                                <option value="{{ $obra->id }}" @if($obra->id == $employee->building) selected @endif>{{ $obra->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Dados de Acesso</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <form action="{{ route("user.funcionario.user.update", $employee->id) }}" method="post">
                                <div class="col-md-4 col-sm-5 col-xs-10">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <br />
                                    @endif
                                    @method('PATCH')
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Nível de acesso</label>
                                        <div class="controls">
                                            <ul class="list-inline">
                                                <li>
                                                    <input type="radio" name="access-level" id="access-level-1" class="icheck-minimal-blue" value="1" data-access="web"
                                                           @if($role === "1") checked @endif>
                                                    <label class="iradio-label form-label" for="access-level-1">Administrador</label>
                                                </li>
                                                <li>
                                                    <input type="radio" name="access-level" id="access-level-2" class="icheck-minimal-blue" value="2" data-access="web"
                                                           @if($role === "2") checked @endif>
                                                    <label class="iradio-label form-label" for="access-level-2">Gerente de Obra</label>
                                                </li>
                                                <li>
                                                    <input type="radio" name="access-level" id="access-level-3" class="icheck-minimal-blue" value="3" data-access="web"
                                                           @if($role === "3") checked @endif>
                                                    <label class="iradio-label form-label" for="access-level-3">Almoxarife</label>
                                                </li>
                                                <li>
                                                    <input type="radio" name="access-level" id="access-level-4" class="icheck-minimal-blue" value="4" data-access="pin"
                                                           @if($role === "4") checked @endif>
                                                    <label class="iradio-label form-label" for="access-level-4">Funcionário</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="web access">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Email/Usuário</label>
                                            <span class="desc">Para acesso ao Painel</span>
                                            @if ($employee->user['username'] == null)
                                                <p class="bg-warning">Email/Usuário ainda não cadastrado</p>
                                            @endif
                                            <div class="controls">
                                                <input type="text" class="form-control" name="username" value="{{ $employee->user['username'] }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Senha</label>
                                            @if ($employee->user['password'] == null)
                                                <p class="bg-warning">Senha ainda não cadastrada</p>
                                            @else
                                                <p class="bg-info">Deixe em branco para não alterar a senha</p>
                                            @endif
                                            <div class="controls">
                                                <input type="text" class="form-control" name="password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pin access">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Pin</label>
                                            <span class="desc">Para confirmação de retirada de material</span>
                                            @if ($employee->user['pin'] == null)
                                                <p class="bg-warning">Pin ainda não cadastrado</p>
                                            @else
                                                <p class="bg-info">Deixe em branco para não alterar o pin</p>
                                            @endif
                                            <div class="controls">
                                                <input type="number" class="form-control" name="pin" data-mask="9999" placeholder="Pin Numérico">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <div class="form-group">
                                        <div class="controls">
                                            <button class="btn btn-primary">Salvar</button>
                                            <button class="btn btn-danger" type="button" onclick="history.back(-1)">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <script src="/assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('input[type="radio"]').on('ifChanged', function(){
                var inputValue = $(this).data("access");
                var targetBox = $("." + inputValue);
                $(".access").not(targetBox).hide();
                $(targetBox).show();
            });
        });
    </script>
@endsection
