@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Funcionários</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Novo Funcionário</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Novo Funcionário</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="row">
                            <form action="{{ route("user.funcionario.store") }}" method="post">
                                <div class="col-md-4 col-sm-5 col-xs-10">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div><br />
                                    @endif
                                    @method('post')
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">CPF</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="doc" value="{{ old("doc") }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Nome Completo</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="name" value="{{ old("name") }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-5 col-xs-10">
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Função</label>
                                        <div class="controls">
                                            <select class="select2" id="office" name="office" >
                                                <option value="">Selecione uma Função</option>
                                                @foreach($offices as $office)
                                                    <option value="{{ $office->id }}" @if($office->id == old("office")) selected @endif>{{ $office->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Setor</label>
                                        <div class="controls">
                                            <select class="select2" id="building" name="sector" >
                                                <option value="">Selecione um Setor</option>
                                                @foreach($sectors as $sector)
                                                    <option value="{{ $sector->id }}" @if($sector->id == old("sector")) selected @endif>{{ $sector->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-5 col-xs-10">
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Contato</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="contact" value="{{ old("contact") }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Obra</label>
                                        <div class="controls">
                                            <select class="select2" id="building" name="building" >
                                                <option value="">Selecione uma Obra</option>
                                                @foreach($obras as $obra)
                                                    <option value="{{ $obra->id }}" @if($obra->id == old("building")) selected @endif>{{ $obra->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <div class="form-group">
                                        <div class="controls">
                                            <button class="btn btn-primary">Salvar</button>
                                            <button class="btn btn-danger" type="button" onclick="history.back(-1)">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </section>
    </div>
@endsection
