@extends('layouts.app')

@if($building->warehouses != null)
    @php $totalItens = 0; @endphp
    @php $totalQuantity = 0; @endphp
    @foreach($building->warehouses as $warehouse)
        @php $totalItens += $warehouse->inventories->count(); @endphp
        @php $totalQuantity += $warehouse->inventories->sum('quantity'); @endphp
    @endforeach
@else
    @php $warehouse = null; @endphp
@endif

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Obra - {{ $building->name }}</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li>
                    <a href="#">Obras</a>
                </li>
                <li class="active">
                    <strong>{{ $building->name }}</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Resumo</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Idfentificação da Obra</label>
                                    <div class="controls">
                                        <input type="number" class="form-control" name="number" value="{{ $building->number }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Total de Funcionários</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="name" value="{{ $building->employees->count() }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Total de Itens no Estoque</label>
                                    <div class="controls">
                                        <input type="number" class="form-control" name="number" value="{{ $totalItens }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Total de Unidades no Estoque</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="name" value="{{ $totalQuantity }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Endereço</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="address" value="{{ $building->address }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Gerente da Obra</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="address" value="{{ $building->manager['name'] }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#entrances" data-toggle="tab">
                    <i class="fa fa-plus"></i> Entradas
                </a>
            </li>
            <li>
                <a href="#withdrawals" data-toggle="tab">
                    <i class="fa fa-minus"></i> Retiradas
                </a>
            </li>
            <li>
                <a href="#warehouses" data-toggle="tab">
                    <i class="fa fa-warehouse"></i> Almoxarifados
                </a>
            </li>
            <li>
                <a href="#employees" data-toggle="tab">
                    <i class="fa fa-id-badge"></i> Funcionários
                </a>
            </li>
            <li>
                <a href="#tools" data-toggle="tab">
                    <i class="fa fa-toolbox"></i> Ferramentas/Equipamentos
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade in active" id="entrances">
                <table class="table datatable display responsive no-wrap" width="100%">
                    <thead>
                    <tr>
                        <th>Data de Entrada</th>
                        <th>Nº Nota Fiscal</th>
                        <th>CNPJ - Fornecedor</th>
                        <th>Almoxarifado</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($warehouse != null)
                        @foreach($warehouse->entrances as $entrance)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($entrance->created_at)->format('d/m/Y')}}</td>
                                <td>{{ $entrance->doc }}</td>
                                <td>{{ $entrance->provider['doc'] }} - {{ $entrance->provider['name'] }}</td>
                                <td>{{ $entrance->warehouse['name'] }}</td>
                                <td>
                                    <ul class="list-inline">
                                        <li>
                                            <a href="{{ route("user.entrada.show", $entrance->id) }}">
                                                <button class="btn btn-sm btn-info" title="Visualizar Entrada">
                                                    <span class="fa fa-eye"></span>
                                                </button>
                                            </a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="withdrawals">
                <table class="table datatable display responsive no-wrap" width="100%">
                    <thead>
                    <tr>
                        <th>Data de Retirada</th>
                        <th>Nº Nota Fiscal</th>
                        <th>Funcionário</th>
                        <th>Almoxarifado</th>
                        <th>Obra</th>
                        <th>Status</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($building->warehouses as $warehouse)
                        @foreach($warehouse->withdrawals as $withdrawal)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($withdrawal->created_at)->format('d/m/Y')}}</td>
                            <td>{{ $withdrawal->doc }}</td>
                            <td>
                                @if(!empty($withdrawal->employee["id"]))
                                    <a href="{{ route("user.funcionario.show", $withdrawal->employee["id"]) }}">
                                        {{ $withdrawal->employee['name'] }}
                                    </a>
                                @else
                                    Sem funcionário para essa retirada
                                @endif
                            </td>
                            <td>
                                <a href="{{ route("user.almoxarifado.show", $withdrawal->warehouse["id"]) }}">
                                    {{ $withdrawal->warehouse['name'] }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route("user.obra.show", $withdrawal->warehouse['building']["id"]) }}">
                                    {{ $withdrawal->warehouse['building']['name'] }}
                                </a>
                            </td>
                            <td>
                                @if($withdrawal->status == 1)
                                    <span class="label label-warning"><i class='fa fa-clock-o'></i> Aguardando Aprovação</span>
                                @elseif($withdrawal->status == 2)
                                    <span class="label label-danger"><i class='fa fa-times'></i> Cancelado</span>
                                @elseif($withdrawal->status == 3)
                                    <span class="label label-success"><i class='fa fa-thumbs-up'></i> Aprovado</span>
                                @elseif($withdrawal->status == 5)
                                    <span class="label label-primary"><i class='fa fa-undo-alt'></i> Devolvida</span>
                                @endif
                            </td>
                            <td>
                                <ul class="list-inline">
                                    <li>
                                        <a href="{{ route("user.retirada.show", $withdrawal->id) }}">
                                            <button class="btn btn-sm btn-info" title="Visualizar Retirada">
                                                <span class="fa fa-eye"></span>
                                            </button>
                                        </a>
                                    </li>
                                    @if($withdrawal->status == 1)
                                        <li>
                                            <a href="/user/retirada/{{ $withdrawal->id }}?employee_confirmation=true">
                                                <button class="btn btn-sm btn-success" title="Aprovar Retirada">
                                                    <span class="fa fa-check-circle"></span>
                                                </button>
                                            </a>
                                        </li>
                                        <li>
                                            <form action="{{ route('user.retirada.cancel', $withdrawal->id)}}" method="post">
                                                @csrf
                                                <meta name="_token" content="{{ csrf_token() }}">
                                                @method('POST')
                                                <button class="btn btn-sm btn-warning" title="Cancelar Retirada"
                                                        data-toggle="confirm"
                                                        data-title=""
                                                        data-message="Tem certeza que deseja cancelar essa retirada?"
                                                        data-type="warning">
                                                    <span class="fa fa-ban"></span>
                                                </button>
                                            </form>
                                        </li>
                                    @elseif($withdrawal->status == 2)
                                        <li>
                                            <form action="{{ route('user.retirada.destroy', $withdrawal->id)}}" method="post">
                                                @csrf
                                                <meta name="_token" content="{{ csrf_token() }}">
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger" title="Desativar Retirada"
                                                        data-toggle="confirm"
                                                        data-title=""
                                                        data-message="Tem certeza que deseja desativar essa retirada?"
                                                        data-type="danger">
                                                    <span class="fa fa-trash"></span>
                                                </button>
                                            </form>
                                        </li>
                                    @elseif($withdrawal->status == 3)
                                        <li>
                                            <a href="/user/retirada/{{ $withdrawal->id }}?withdrawal_giveback=true">
                                                <button class="btn btn-sm btn-primary" title="Devolver Retirada">
                                                    <span class="fa fa-undo-alt"></span>
                                                </button>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="warehouses">
                <table class="table datatable display responsive no-wrap" width="100%">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Descrição</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($building->warehouses as $warehouse)
                        <tr>
                            <td>{{ $warehouse->name }}</td>
                            <td>{{ $warehouse->description }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li>
                                        <a href="{{ route("user.almoxarifado.show", $warehouse->id) }}">
                                            <button class="btn btn-sm btn-info" title="Visualizar Almoxarifado">
                                                <span class="fa fa-eye"></span>
                                            </button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route("user.almoxarifado.edit", $warehouse->id) }}">
                                            <button class="btn btn-sm btn-accent" title="Editar Almoxarifado">
                                                <span class="fa fa-edit"></span>
                                            </button>
                                        </a>
                                    </li>
                                    <li>
                                        <form action="{{ route('user.almoxarifado.destroy', $warehouse->id)}}" method="post">
                                            @csrf
                                            <meta name="_token" content="{{ csrf_token() }}">
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger" title="Desativar Almoxarifado"
                                                    data-toggle="confirm"
                                                    data-title=""
                                                    data-message="Tem certeza que deseja desativar esse almoxarifado?"
                                                    data-type="danger">
                                                <span class="fa fa-trash"></span>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="employees">
                <table class="table datatable display responsive no-wrap" width="100%">
                    <thead>
                    <tr>
                        <th>CPF</th>
                        <th>Nome</th>
                        <th>Função</th>
                        <th>Setor</th>
                        <th>Contato</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($building->employees as $employee)
                        <tr>
                            <td>{{ $employee->doc }}</td>
                            <td>{{ $employee->name }}</td>
                            <td>{{ $employee->office['name'] }}</td>
                            <td>{{ $employee->sector['name'] }}</td>
                            <td>{{ $employee->contact }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li>
                                        <a href="{{ route("user.funcionario.show", $employee->id) }}">
                                            <button class="btn btn-sm btn-info" title="Visualizar Funcionário">
                                                <span class="fa fa-eye"></span>
                                            </button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route("user.funcionario.edit", $employee->id) }}">
                                            <button class="btn btn-sm btn-accent" title="Editar Funcionário">
                                                <span class="fa fa-edit"></span>
                                            </button>
                                        </a>
                                    </li>
                                    <li>
                                        <form action="{{ route('user.funcionario.destroy', $employee->id)}}" method="post">
                                            @csrf
                                            <meta name="_token" content="{{ csrf_token() }}">
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger" title="Desativar Funcionário"
                                                    data-toggle="confirm"
                                                    data-title=""
                                                    data-message="Tem certeza que deseja desativar esse funcionário?"
                                                    data-type="danger">
                                                <span class="fa fa-trash"></span>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="tools">
                <table class="table datatable display responsive no-wrap" width="100%">
                    <thead>
                    <tr>
                        <th>Código da Ferramenta</th>
                        <th>Descrição</th>
                        <th>Tipo</th>
                        <th>Saldo</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($warehouse->inventories != null)
                    @foreach($warehouse->inventories as $inventory)
                        <tr>
                            <td>{{ $inventory->tool['cod'] }}</td>
                            <td>{{ $inventory->tool['name'] }}</td>
                            <td>{{ $inventory->tool['itemType']['name'] }}</td>
                            <td>{{ $inventory->quantity }} {{ $inventory->tool['unity']['name'] }}(s)</td>
                            <td>
                                <ul class="list-inline">
                                    <li>
                                        <a href="@if($inventory->tool['id']){{ route("user.ferramenta.show", $inventory->tool['id']) }} @else # @endif">
                                            <button class="btn btn-sm btn-info" title="Visualizar Ferramenta">
                                                <span class="fa fa-eye"></span>
                                            </button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="@if($inventory->tool['id']){{ route("user.ferramenta.edit", $inventory->tool['id']) }} @else # @endif">
                                            <button class="btn btn-sm btn-accent" title="Editar Ferramenta">
                                                <span class="fa fa-edit"></span>
                                            </button>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <button class="btn btn-sm btn-success" title="Transferir Ferramenta">
                                                <span class="fa fa-exchange-alt"></span>
                                            </button>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('post_scripts')
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            $('.datatable').dataTable({
                "language": {
                    responsive: true,
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection
