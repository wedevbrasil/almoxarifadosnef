@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Almoxarifados</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Novo Almoxarifado</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Novo Almoxarifado</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form action="{{ route("user.almoxarifado.store") }}" method="post">
                                    @method('post')
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Nome</label>
                                        <span class="desc">ex: "Almoxarifado II"</span>
                                        <div class="controls">
                                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Obra</label>
                                        <span class="desc">ex: "Metrô - Transport - Linha 4"</span>
                                        <div class="controls">
                                            <input name="building_id" type="hidden" value="{{ Auth::user()->employee['building']['id'] }}">
                                            <select class="select2" id="building_id" disabled>
                                                <option value="{{ $obra->id }}">{{ $obra->number }} - {{ $obra->name }}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Almoxarife</label>
                                        <div class="controls">
                                            <select class="form-control disabled" id="employee" name="employee"  >
                                                <option value="">Selecione um Almoxarife</option>
                                                @foreach($employees as $employee)
                                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Descrição</label>
                                        <span class="desc">ex: "Almoxarifado do segundo andar"</span>
                                        <div class="controls">
                                            <textarea class="form-control" id="description" name="description">{{ old('description') }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <button class="btn btn-primary">Salvar</button>
                                            <button class="btn btn-danger" type="button" onclick="history.back(-1)">Cancelar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
