<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>SNEF - Controle de Almoxarifado</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="Thiago Moreira <loganguns@gmail.com>" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="apple-touch-icon" sizes="180x180" href="/assets/icons/apple-touch-icon.png?v=jw7GqM8blE">
        <link rel="icon" type="image/png" sizes="32x32" href="/assets/icons/favicon-32x32.png?v=jw7GqM8blE">
        <link rel="icon" type="image/png" sizes="192x192" href="/assets/icons/android-chrome-192x192.png?v=jw7GqM8blE">
        <link rel="icon" type="image/png" sizes="16x16" href="/assets/icons/favicon-16x16.png?v=jw7GqM8blE">
        <link rel="manifest" href="/assets/icons/site.webmanifest?v=jw7GqM8blE">
        <link rel="mask-icon" href="/assets/icons/safari-pinned-tab.svg?v=jw7GqM8blE" color="#134441">
        <link rel="shortcut icon" href="/assets/icons/favicon.ico?v=jw7GqM8blE">
        <meta name="apple-mobile-web-app-title" content="SNEF">
        <meta name="application-name" content="SNEF">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-TileImage" content="/assets/icons/mstile-144x144.png?v=jw7GqM8blE">
        <meta name="msapplication-config" content="/assets/icons/browserconfig.xml?v=jw7GqM8blE">
        <meta name="theme-color" content="#09423f">

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="{{ asset('/assets/css/app.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css" media="screen"/>
        <link href="{{ asset('/assets/plugins/pace/pace-theme-flash.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css" media="screen"/>
        <link href="{{ asset('/assets/bootstrap/css/bootstrap.min.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/plugins/bootstrap/css/bootstrap-theme.min.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/fonts/font-awesome/css/font-awesome.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/fonts/font-awesome/css/all.min.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/css/animate.min.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/plugins/perfect-scrollbar/perfect-scrollbar.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/css/bootstrap-notifications.min.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css">

        <!-- CORE CSS FRAMEWORK - END -->

        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->
        @yield('pre_scripts')
        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->

        <!-- CORE CSS TEMPLATE - START -->
        <link href="{{ asset('/assets/css/style.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/css/responsive.css?v=jw7GqM8blE') }}" rel="stylesheet" type="text/css"/>
        <!-- CORE CSS TEMPLATE - END -->

    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    @if (Auth::check())
    <body data-user-id="{{ Auth::user()->id }}">
    @else
    <body id="app">
    @endif

        <div id="app">
            <div id="main"></div>
        </div>
        <!-- START TOPBAR -->
        @include('partials.topbar')
        <!-- END TOPBAR -->
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <!-- Preloader -->
            {{--@include('partials.preloader')--}}
            <!-- End Preloader -->

            <!-- SIDEBAR - START -->
            @include('partials.sidebar')
            <!--  SIDEBAR - END -->

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        @yield('page-header')
                    </div>

                    <div class="clearfix"></div>

                    <!-- MAIN CONTENT AREA STARTS -->
                        @yield('content')

                    <!-- Footer -->
                    @include('partials.footer')
                    <!-- Footer -->

                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

        <!-- Bug report Button -->
        @include('partials.reportbutton')
        <!-- EndBug report Button -->

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

        <!-- CORE JS FRAMEWORK - START -->
        <script src="{{asset('/assets/js/app.js')}}"></script>
        <script src="/assets/js/jquery-1.11.2.min.js?v=jw7GqM8blE"></script>
        <script src="/assets/js/jquery.easing.min.js?v=jw7GqM8blE" type="text/javascript"></script>
        <script src="/assets/js//bootstrap.modal.min.js?v=jw7GqM8blE" type="text/javascript"></script>
        <script src="/assets/plugins/pace/pace.min.js?v=jw7GqM8blE" type="text/javascript"></script>
        <script src="/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js?v=jw7GqM8blE" type="text/javascript"></script>
        <script src="/assets/plugins/viewport/viewportchecker.js?v=jw7GqM8blE" type="text/javascript"></script>
        <!-- CORE JS FRAMEWORK - END -->

        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
        <script src="/assets/js/bootstrap-tabcollapse.js?v=jw7GqM8blE"></script>

        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->

        <!-- CORE TEMPLATE JS - START -->
        <script src="/assets/js/popper.min.js?v=jw7GqM8blE" type="text/javascript"></script>
        <script src="/assets/js/bootstrap.confirm.js?v=jw7GqM8blE"></script>
        <script src="/assets/js/scripts.js?v=jw7GqM8blE" type="text/javascript"></script>
        <!-- END CORE TEMPLATE JS - END -->


        <link href="/assets/plugins/select2/select2.css" rel="stylesheet" type="text/css" media="screen"/>
        <script src="/assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/select2/select2.min.js" type="text/javascript"></script>


        <script src="//cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

        <!-- Service Worker -->
        <script>
            $(function() {
                if ('serviceWorker' in navigator ) {
                    window.addEventListener('load', function() {
                        navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
                            // Registration was successful
                            console.log('ServiceWorker registration successful with scope: ', registration.scope);
                        }, function(err) {
                            // registration failed :(
                            console.log('ServiceWorker registration failed: ', err);
                        });
                    });


                }
                if ('serviceWorker' in navigator) {
                    window.addEventListener('load', () => {
                        navigator.serviceWorker.register('/sw.js').then(registration => {
                            console.log('SW registered: ', registration);
                        }).catch(registrationError => {
                            console.log('SW registration failed: ', registrationError);
                        });
                    });
                }
            });
        </script>
        <!-- End Service Worker -->

        @yield('post_scripts')
    </body>
</html>
