@extends('layouts.error')

@section('page-header')
@endsection

@section('content')
    <div class="col-xl-12">
        <section class="box nobox ">
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <h1 class="page_error_code text-primary">500</h1>
                        <h1 class="page_error_info">Oops! Erro interno no servidor.</h1>
                        <p class="text text-center text-black-50" style="font-size: 25px">Um ticket foi criado no nosso sistema interno e em breve resolveremos o problema.</p>
                        <div class="row">
                            <div class="col-12">
                                <form action="javascript:;" method="post" class="page_error_search">
                                    <div class="text-center page_error_btn">
                                        <a class="btn btn-primary btn-lg" href='{{ route('admin.dashboard.index') }}'>
                                            <i class='fa fa-location-arrow'></i> &nbsp; Voltar para Dashboard
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
