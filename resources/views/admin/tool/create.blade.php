@extends('layouts.app')

@php
    if(!isset($lastId)){
        $lastId = 0;
    } else {
        $lastId = $lastId->cod;
    }
@endphp

@section('page-header')
    <div class="page-title" xmlns="">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Novo Produto</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Novo Produto</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Produto</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form action="{{ route("admin.ferramenta.store") }}" method="post">
                                    @method('post')
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">

                                    <label class="form-label" for="field-1">Código do Produto</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" name="cod" id="cod" value="{{ $lastId + 1}}" readonly>
                                        <div class="input-group-btn">
                                            <button id="edit" type="button" class="btn btn-default">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Descrição</label>
                                        <span class="desc">ex: "ALAVANCA DE AÇO 1"X1,5 METROS"</span>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="name" value="{{ old("name") }}" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Tipo</label>
                                        <div class="controls">
                                            <select class="select2" id="type" name="type" required>
                                                <option value="">Selecione um Tipo</option>
                                                @foreach($itenTypes as $itenType)
                                                    <option value="{{ $itenType->id }}" {{ ( old("type") == $itenType->id ? "selected":"") }}>{{ $itenType->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Tipo de Unidade</label>
                                        <div class="controls">
                                            <select class="select2" id="unity" name="unity" required>
                                                <option value="">Selecione um tipo de unidade</option>
                                                @foreach($units as $unity)
                                                    <option value="{{ $unity->id }}">{{ $unity->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Estoque Minimo</label>
                                        <div class="controls">
                                            <input type="number" class="form-control" name="stock_min" value="{{ old("stock_min") }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <button class="btn btn-primary">Salvar</button>
                                            <button class="btn btn-danger" type="button" onclick="history.back(-1)">Cancelar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
<script>
    (function() {
        document.addEventListener( "DOMContentLoaded", function() {
            var btn = document.querySelector( "#edit" );
            var input = $('#cod');
            btn.addEventListener( "click", function() {
                if ( input.is('[readonly]') ) {
                    input.removeAttr("readonly");
                } else {
                    input.val('{{ $lastId + 1}}');
                    input.attr("readonly", "readonly");
                }
            }, false);

        });
    })();
</script>
@endsection
