@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Ferramenta/Equipamento - {{ $tool->name }}</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li>
                    <a href="#">Ferramenta/Equipamento</a>
                </li>
                <li class="active">
                    <strong>{{ $tool->name }}</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Item</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Código do Item</label>
                                    <div class="controls">
                                        <input type="number" class="form-control" name="number" value="{{ $tool->cod }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Descrição</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="name" value="{{ $tool->name }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Estoque Mínimo</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="stock_min" value="{{ $tool['stock_min'] }} {{ $tool['unity']['name'] }}(s)" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Unidade</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="quantity" value="{{ $tool['unity']['name'] }} ({{ $tool['unity']['symbol'] }})" readonly disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-5 col-xs-10">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Tipo</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="address" value="{{ $tool->itemType->name }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#entrances" data-toggle="tab">
                    <i class="fa fa-plus"></i> Entradas
                </a>
            </li>
            <li>
                <a href="#withdrawals" data-toggle="tab">
                    <i class="fa fa-minus"></i> Retiradas
                </a>
            </li>
            <li>
                <a href="#warehouses" data-toggle="tab">
                    <i class="fa fa-warehouse"></i> Almoxarifados
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade in active" id="entrances">
                <table class="table datatable display responsive no-wrap" width="100%">
                    <thead>
                    <tr>
                        <th>Data de Entrada</th>
                        <th>Nº Nota Fiscal</th>
                        <th>CNPJ - Fornecedor</th>
                        <th>Almoxarifado</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tool->entrances as $entrance)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($entrance->created_at)->format('d/m/Y')}}</td>
                            <td>{{ $entrance->doc }}</td>
                            <td>{{ $entrance->provider->doc }} - {{ $entrance->provider->name }}</td>
                            <td>{{ $entrance->warehouse->name }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li>
                                        <a href="{{ route("admin.entrada.show", $entrance->id) }}">
                                            <button class="btn btn-sm btn-info" title="Visualizar Entrada">
                                                <span class="fa fa-eye"></span>
                                            </button>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="withdrawals">
                <table class="table datatable display responsive no-wrap" width="100%">
                    <thead>
                    <tr>
                        <th>Data de Retirada</th>
                        <th>Nº Nota Fiscal</th>
                        <th>CNPJ - Fornecedor</th>
                        <th>Funcionário</th>
                        <th>Almoxarifado</th>
                        <th>Obra</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if($tool->withdrawals != '')
                        @foreach($tool->withdrawals as $withdrawal)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($withdrawal->created_at)->format('d/m/Y')}}</td>
                                <td>{{ $withdrawal->doc }}</td>
                                <td>
                                    @if(!empty($withdrawal->provider->id))
                                    <a href="{{ route("admin.fornecedor.show", $withdrawal->provider->id) }}">
                                        {{ $withdrawal->provider->doc }} - {{ $withdrawal->provider->name }}
                                    </a>
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($withdrawal->employee->id))
                                    <a href="{{ route("admin.funcionario.show", $withdrawal->employee->id) }}">
                                        {{ $withdrawal->employee->name }}
                                    </a>
                                    @endif
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <li>
                                            <a href="{{ route("admin.retirada.show", $withdrawal->id) }}">
                                                <button class="btn btn-sm btn-info" title="Visualizar Retirada">
                                                    <span class="fa fa-eye"></span>
                                                </button>
                                            </a>
                                        </li>
                                        <li>
                                            <form action="{{ route('admin.retirada.destroy', $withdrawal->id)}}" method="post">
                                                @csrf
                                                <meta name="_token" content="{{ csrf_token() }}">
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger" title="Desativar Retirada"
                                                        data-toggle="confirm"
                                                        data-title=""
                                                        data-message="Tem certeza que deseja desativar essa retirada?"
                                                        data-type="danger">
                                                    <span class="fa fa-trash"></span>
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>Sem retiradas</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="warehouses">
                <table class="table datatable display responsive no-wrap" width="100%">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Obra</th>
                        <th>Estoque Mínimo</th>
                        <th>Quantidade Total em Estoque</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tool->inventories as $inventory)
                        <tr>
                            <td>
                                <a href="{{ route('admin.almoxarifado.show', $inventory->warehouse->id) }}">
                                    {{ $inventory->warehouse->name }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('admin.obra.show', $inventory->warehouse->building->id) }}">
                                    {{ $inventory->warehouse->building->name }}
                                </a>
                            </td>
                            <td>{{ $inventory['stock_min']}} {{ $inventory['tool']['unity']['name'] }}(s)</td>
                            <td>{{ $inventory['quantity'] }} {{ $inventory['tool']['unity']['name'] }}(s)</td>
                            <td>
                                <ul class="list-inline">
                                    <li>
                                        <a href="{{ route('admin.almoxarifado.show', $inventory->warehouse->id) }}">
                                            <button class="btn btn-sm btn-info" title="Visualizar Almoxarifado">
                                                <span class="fa fa-eye"></span>
                                            </button>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@endsection

@section('post_scripts')
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            $('.datatable').dataTable({
                responsive: true,
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10 linhas', '25 linhas', '50 linhas', '100 linhas', 'Mostrar Todos' ]
                ],
                "language": {
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection
