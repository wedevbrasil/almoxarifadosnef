@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Estoque em {{ $inventory->warehouse->name }}</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Editar Estoque</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Dados do Almoxarifado</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Almoxarife</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" id="field-1"
                                               value="{{ $inventory->warehouse->manager->name != '' ? $inventory->warehouse->manager->name : "Sem Almoxarife definido" }}"
                                               readonly disabled >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Nome (Obra)</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" id="field-1"  value="{{ $inventory->warehouse->name }} ({{ $inventory->warehouse->building->name }})" readonly disabled >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Descrição</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" id="field-1" value="{{ $inventory->warehouse->description }}" readonly disabled >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <form action="{{ route("admin.estoque.update", $inventory->id) }}" method="post">
    @method('PATCH')
    @csrf
    <meta name="_token" content="{{ csrf_token() }}">
        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">Editar Estoque</h2>
                    <div class="actions panel_actions pull-right">
                        <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-md-4 col-sm-5 col-xs-10">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div><br />
                                    @endif


                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Código da Item</label>
                                            <span class="desc">ex: "1548"</span>
                                            <div class="controls">
                                                <input type="number" class="form-control" name="cod" value="{{ $inventory->tool->cod }}" readonly disabled>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Descrição</label>
                                            <span class="desc">ex: "ALAVANCA DE AÇO 1"X1,5 METROS"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" name="name" value="{{ $inventory->tool->name }}" readonly disabled>
                                            </div>
                                        </div>

                                    </div>
                                <div class="col-md-4 col-sm-5 col-xs-10">
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Tipo</label>
                                        <div class="controls">
                                            <select class="select2" id="type" name="type" readonly disabled>
                                                <option value="">Selecione um tipo</option>
                                                @foreach($itemTypes as $itemType)
                                                    <option value="{{ $itemType->id }}" @if($itemType->id == $inventory->tool->itemType->id) selected @endif>{{ $itemType->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Tipo de Unidade</label>
                                        <div class="controls">
                                            <select class="select2" id="unity" name="unity" readonly disabled>
                                                <option value="">Selecione um tipo de unidade</option>
                                                @foreach($units as $unity)
                                                    <option value="{{ $unity->id }}" @if($unity->id == $inventory->tool->unity->id) selected @endif>{{ $unity->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4 col-sm-5 col-xs-10">
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Estoque Minimo <small>(Apenas este almoxarifado)</small></label>
                                        <div class="input-group controls">
                                            <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2"
                                                   id="stock_min" name="stock_min" value="{{ old("stock_min", $inventory->stock_min) }}">
                                            <span class="input-group-addon" id="basic-addon2">{{ $inventory->tool->unity->name }}(s)</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Quantidade em Estoque</label>
                                        <div class="input-group controls">
                                            <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2"
                                                   id="quantity" name="quantity" value="{{ old("quantity", $inventory->quantity) }}">
                                            <span class="input-group-addon" id="basic-addon2">{{ $inventory->tool->unity->name }}(s)</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>


        <div class="col-md-12">
            <p>
                <div class="form-group">
                    <div class="controls pull-right">
                        <a href="#">
                            <button class="btn btn-accent" title="Editar Entrada">
                                <span class="fa fa-save"></span> Salvar
                            </button></a>
                        <button class="btn btn-danger" type="button" onclick="history.back(-1)"><span class="fa fa-chevron-left"></span> Voltar</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </p>
    </div>
    </form>
@endsection
