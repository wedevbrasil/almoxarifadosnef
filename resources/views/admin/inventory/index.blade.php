@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Produtos</h1><!-- PAGE HEADING TAG - END -->
        </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Produtos</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Todos os Produtos</h2>
                <div class="actions panel_actions pull-right">
                    <a href="{{ route("admin.ferramenta.create") }}">
                        <button type="button" class="btn btn-primary btn-icon btn-sm">
                            <i class="fa fa-plus"></i> &nbsp; <span>Adicionar</span>
                        </button>
                    </a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        @if(session()->get('success'))
                            <div class="alert alert-success alert-dismissible fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Sucesso!</strong> {{ session()->get('success') }}
                            </div>
                        @endif
                        <table class="table datatable display responsive no-wrap" width="100%">
                            <thead>
                            <tr>
                                <th>Código</th>
                                <th>Descrição</th>
                                <th>Grupo</th>
                                <th>Unidade</th>
                                <th>Total de Entrada</th>
                                <th>Total de Retirada</th>
                                <th>Saldo</th>
                                <th>Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tools as $tool)
                                <tr>
                                    <td>{{ $tool->cod }}</td>
                                    <td>{{ $tool->name }}</td>
                                    <td>{{ $tool->itemType->name }}</td>
                                    <td>{{ $tool->unity->name }}</td>
                                    <td>
                                        @php
                                            // Percorrer todas as entradas
                                            foreach ($tool->entrances as $key => $entrances){
                                                // Achar a chave do array da ferramenta
                                                $tool_id[$key] = array_search($tool->id, $entrances->tool_ids);
                                                // Novo array com as quantidades entrada da ferramenta
                                                $qnte[$key] =  $entrances->quantity[$tool_id[$key]];
                                            }
                                        @endphp
                                        @if(isset($qnte))
                                            {{  $qnt_ent = array_sum($qnte) /* Soma das quantidades entradas */ }}
                                            {{ $tool->unity->name }}(s)
                                        @else
                                            @php  $qnt_ent = 0 /* Sem entradas */ @endphp
                                            Sem entradas
                                        @endif
                                    </td>
                                    <td>
                                        @php
                                            // Percorrer todas as retiradas
                                            foreach ($tool->withdrawals as $key => $withdrawals){
                                                // Achar a chave do array da ferramenta
                                                $tool_id[$key] = array_search($tool->id, $withdrawals->tool_ids);
                                                // Novo array com as quantidades retirada da ferramenta
                                                $qntw[$key] =  $withdrawals->quantity[$tool_id[$key]];
                                            }
                                        @endphp
                                        @if(isset($qntw))
                                            {{ $qnt_ret = array_sum($qntw) /* Soma das quantidades retiradas */ }}
                                            {{ $tool->unity->name }}(s)
                                        @else
                                            @php $qnt_ret = 0 /* Sem retiradas */ @endphp
                                            Sem retiradas
                                        @endif
                                    </td>
                                    <td>
                                        @if(isset($qnt_ent) and isset($qnt_ret))
                                            {{ $qnt_ent - $qnt_ret }}
                                            {{ $tool->unity->name }}(s)
                                        @endif
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="{{ route("admin.ferramenta.show", $tool->id) }}">
                                                    <button class="btn btn-sm btn-info" title="Visualizar Item">
                                                        <span class="fa fa-eye"></span>
                                                    </button>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route("admin.ferramenta.edit", $tool->id) }}">
                                                    <button class="btn btn-sm btn-accent" title="Editar Item">
                                                        <span class="fa fa-edit"></span>
                                                    </button>
                                                </a>
                                            </li>
                                            <li>
                                                <form action="{{ route('admin.ferramenta.destroy', $tool->id)}}" method="post">
                                                    @csrf
                                                    <meta name="_token" content="{{ csrf_token() }}">
                                                    @method('DELETE')
                                                    <button class="btn btn-sm btn-danger" title="Desativar Item"
                                                            data-toggle="confirm"
                                                            data-title=""
                                                            data-message="Tem certeza que deseja desativar essa ferramenta?"
                                                            data-type="danger">
                                                        <span class="fa fa-trash"></span>
                                                    </button>
                                                </form>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                @php
                                    unset($qnte);
                                    unset($qntw);
                                    unset($qnt_ent);
                                    unset($qnt_ret);
                                @endphp
                            @endforeach
                            </tbody>
                        </table>
                        {{--<div class="pull-right">{{ $tools->links() }}</div>--}}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <link href="/assets/plugins/datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/datatables/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>

    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>

    <script src="/assets/plugins/datatables/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/jszip.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/pdfmake.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/vfs_fonts.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.print.min.js" type="text/javascript"></script>
    <!--script>
        $(document).ready(function() {
            $('.datatable').dataTable( {
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('admin.ferramenta.datatables') }}"
            } );
        } );
    </script-->
    <script>
        $(document).ready( function () {
            var table = $('.datatable').dataTable({
                dom: 'lBfrtip',
                responsive: true,
                columns: [
                    { "type": "num" },
                    { "type": "string" },
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ],
                order: [[ 0, "desc" ]],
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10 linhas', '25 linhas', '50 linhas', '100 linhas', 'Mostrar Todos' ]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        text: 'Exportar',
                        buttons: [
                            {
                                extend: 'copy',
                                text: 'Copiar',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'print',
                                text: 'Imprimir',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'excel',
                                text: 'Excel',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: 'PDF',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'csv',
                                text: 'CSV',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                        ]
                    },
                ],
                "language": {
                    buttons: {
                        copyTitle: 'Capiado para área de transferência',
                        copySuccess: {
                            _: '%d linhas copiadas',
                            1: '1 linha copiada'
                        }
                    },
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection

