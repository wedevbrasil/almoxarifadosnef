@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Dashboard</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Dashboard</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box nobox">
            <div class="content-body">
                <div class="row">
                    @foreach($buildingsCards as $building)
                            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <a href="{{ route('admin.obra.show', $building->id) }}">
                                    <div class="tile">
                                        <div class="wrapper">
                                            <div class="header">{{ $building->name }}</div>

                                            <div class="stats">

                                                <div>
                                                    <strong>Transferências</strong> ----
                                                </div>

                                                <div>
                                                    <strong>Entradas</strong> ----
                                                </div>

                                                <div>
                                                    <strong>Retiradas</strong>  ----
                                                </div>

                                            </div>

                                            <div class="footer">
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
@endsection
