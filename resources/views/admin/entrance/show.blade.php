@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Entradas</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Visualizar Entrada</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Entrada</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Tipo</label>
                                    <div class="controls">
                                        <select class="select2" id="type" name="type" readonly disabled>
                                            <option value="{{ $entrance->entranceType->id }}">{{ $entrance->entranceType->name }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Nº Nota Fiscal</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $entrance->doc }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Fornecedor</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $entrance->provider->name }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Almoxarifado - Obra</label>
                                    <div class="controls">
                                        <input type="text" class="form-control"
                                               value="{{ $entrance->warehouse->name }} - {{ $entrance->warehouse->building->name }}"
                                               readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Itens</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ count($entrance->tool_ids) }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Status</label>
                                    <div class="controls">
                                        @if($entrance->status === 'NEW')
                                            <p class="uilabels text-lg">
                                                <span class="label label-warning"><i class='fa fa-clock-o'></i> Aguardando aprovação</span>
                                            </p>
                                        @elseif($entrance->status === 'CANCELED')
                                            <p class="uilabels text-lg">
                                                <span class="label label-danger"><i class='fa fa-times'></i> Cancelado</span>
                                            </p>
                                        @elseif($entrance->status === 'APPROVED')
                                            <p class="uilabels text-lg">
                                                <span class="label label-success"><i class='fa fa-thumbs-up'></i> Aprovado</span>
                                            </p>
                                        @elseif($entrance->status === 'PROCESSED')
                                            <p class="uilabels text-lg">
                                                <span class="label label-info"><i class='fa fa-vote-yea'></i> Processado</span>
                                            </p>
                                        @elseif($entrance->status === 'RETURNED')
                                            <p class="uilabels text-lg">
                                                <span class="label label-primary"><i class='fa fa-undo-alt'></i> Devolvido</span>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12">
        <ul class="nav nav-tabs content-tabs" id="maincontent" role="tablist">
            <li class="active">
                <a href="#itens" data-toggle="tab">
                    <i class="fa fa-plus"></i> Itens
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade in active" id="itens">
                <table id="itemTable" class="table order-list">
                    <thead>
                        <tr>
                            <td>Código Produto</td>
                            <td>Descrição</td>
                            <td>Tipo</td>
                            <td>Quantidade</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    @foreach($entrance->products as $i => $product)
                        {{ $product }}
                        <tr>
                            <td>{{ $product['cod'] }}</td>
                            <td>{{ $product['name'] }}</td>
                            <td>{{ $product['type_name'] }}</td>
                            <td>{{ $product['quantity'] }} {{ $product['unity_name'] }}(s)</td>
                        </tr>
                    @endforeach
                    <tr>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <p>
            <div class="form-group">
                <div class="controls pull-right">
                    <a href="{{ route("admin.entrada.edit", $entrance->id) }}">
                        <button class="btn btn-accent" title="Editar Entrada">
                            <span class="fa fa-edit"></span> Editar Entrada
                        </button></a>
                    <button class="btn btn-danger" type="button" onclick="history.back(-1)"><span class="fa fa-chevron-left"></span> Voltar</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </p>
    </div>
@endsection
