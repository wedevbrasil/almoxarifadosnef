@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Transferências</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Transferências</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Todos as Transferências</h2>
                <div class="actions panel_actions pull-right">
                    <a href="{{ route("admin.transferencia.create") }}">
                        <button type="button" class=" btn btn-primary btn-icon btn-sm">
                            <i class="fa fa-plus"></i> &nbsp; <span>Adicionar</span>
                        </button>
                    </a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        @if(session()->get('success'))
                            <div class="alert alert-success alert-dismissible fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Sucesso!</strong> {{ session()->get('success') }}
                            </div>
                        @endif
                        @if(session()->get('error'))
                            <div class="alert alert-danger alert-dismissible fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Erro!</strong> {{ session()->get('error') }}
                            </div>
                        @endif

                        <table class="table datatable display responsive no-wrap" width="100%">
                            <thead>
                            <tr>
                                <th>Data de Transferência</th>
                                <th>Data de Devolução</th>
                                <th>Tipo de Transferência</th>
                                <th>Código Interno</th>
                                <th>Obra - Almoxarifado Origem</th>
                                <th>Obra - Almoxarifado Destino</th>
                                <th>Quantidade de Itens</th>
                                <th>Status</th>
                                <th>Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transfers as $transfer)
                                <tr>
                                    <td>{{ \Carbon\Carbon::parse($transfer->created_at)->format('d/m/Y')}}</td>
                                    <td>{{ !empty($r_date = $transfer->return_date['date']) ? \Carbon\Carbon::parse($r_date)->format('d/m/Y') : 'Sem devolução' }}</td>
                                    <td>{{ $transfer->transferType->name }}</td>
                                    <td>{{ $transfer->internal_code }}</td>
                                    <td>
                                        <a href="{{ route("admin.obra.show", $transfer->fromWarehouse->building->id) }}">
                                            {{ $transfer->fromWarehouse->building->name }}
                                        </a>
                                        -
                                        <a href="{{ route("admin.almoxarifado.show", $transfer->fromWarehouse->id) }}">
                                            {{ $transfer->fromWarehouse->name }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route("admin.obra.show", $transfer->toWarehouse->building->id) }}">
                                            {{ $transfer->toWarehouse->building->name }}
                                        </a>
                                        -
                                        <a href="{{ route("admin.almoxarifado.show", $transfer->toWarehouse->id) }}">
                                            {{ $transfer->toWarehouse->name }}
                                        </a>
                                    </td>
                                    <td>
                                        @if($transfer->tool_ids > 0)
                                            {{ count($transfer->tool_ids) }} itens
                                        @endif
                                    <td>
                                        @if($transfer->status == 'NEW')
                                            <span class="label label-warning"><i class='fa fa-clock-o'></i> Aguardando Aprovação</span>
                                        @elseif($transfer->status == 'CANCELED')
                                            <span class="label label-danger"><i class='fa fa-times'></i> Cancelado</span>
                                        @elseif($transfer->status == 'APPROVED')
                                            <span class="label label-success"><i class='fa fa-thumbs-up'></i> Aprovado</span>
                                        @elseif($transfer->status == 'PROCESSED')
                                            <span class="label label-info"><i class='fa fa-vote-yea'></i> Processado</span>
                                        @endif
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="{{ route("admin.transferencia.show", $transfer->id) }}">
                                                    <button class=" btn btn-sm btn-info btn-responsive" title="Visualizar Transferência">
                                                        <span class="fa fa-eye"></span>
                                                    </button>
                                                </a>
                                            </li>
                                            @if($transfer->status != 'CANCELED')
                                            <li>
                                                <a href="#">
                                                    <form action="{{ route('admin.transferencia.cancel', $transfer->id) }}" method="post">
                                                        @csrf
                                                        <meta name="_token" content="{{ csrf_token() }}">
                                                        @method('POST')
                                                        <button class="btn btn-sm btn-danger" title="Cancelar Transferência"
                                                                data-toggle="confirm"
                                                                data-title=""
                                                                data-message="Tem certeza que deseja cancelar essa transferência?"
                                                                data-type="danger">
                                                            <span class="fa fa-ban"></span>
                                                        </button>
                                                    </form>
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <link href="/assets/plugins/datatables/css/jquery.dataTables.css?v=jw7GqM8blE" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/datatables/css/buttons.dataTables.min.css?v=jw7GqM8blE" rel="stylesheet" type="text/css"/>

    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js?v=jw7GqM8blE" type="text/javascript"></script>

    <script src="/assets/plugins/datatables/js/dataTables.buttons.min.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.flash.min.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/jszip.min.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/pdfmake.min.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/vfs_fonts.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.html5.min.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/buttons.print.min.js?v=jw7GqM8blE" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.12/sorting/date-eu.js"></script>
    <script>
        $(document).ready( function () {
            var table = $('.datatable').dataTable({
                dom: 'lBfrtip',
                responsive: true,
                columns: [
                    { "type": "date-eu" },
                    { "type": "date-eu" },
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ],
                order: [[ 0, "desc" ]],
                lengthMenu: [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10 linhas', '25 linhas', '50 linhas', '100 linhas', 'Mostrar Todos' ]
                ],
                buttons: [
                    {
                        extend: 'collection',
                        text: 'Exportar',
                        buttons: [
                            {
                                extend: 'copy',
                                text: 'Copiar',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'print',
                                text: 'Imprimir',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'excel',
                                text: 'Excel',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: 'PDF',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                            {
                                extend: 'csv',
                                text: 'CSV',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                }
                            },
                        ]
                    },
                ],
                "language": {
                    buttons: {
                        copyTitle: 'Capiado para área de transferência',
                        copySuccess: {
                            _: '%d linhas copiadas',
                            1: '1 linha copiada'
                        }
                    },
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection
