@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Visualizar Transferência</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li>
                    <a href="{{ route('admin.transferencia.index') }}">Transferências</a>
                </li>
                <li class="active">
                    <strong>Visualizar Transferência</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Dados da Transferência</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">

                        <div class="row">
                            @if(session()->get('error'))
                                <div class="alert alert-danger alert-dismissible fade in">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <strong>Erro!</strong> {{ session()->get('error') }}
                                </div>
                            @endif
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Almoxarifado de Origem</label>
                                    <div class="controls">
                                        <input type="text" class="form-control"
                                               value="{{ $transfer->fromWarehouse->name }}"
                                               readonly disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Retirada no Almoxarifado de Origem</label>
                                    <div class="controls">
                                        <a href="{{ route('admin.retirada.show', $transfer->withdrawal_id) }}">Ver Retirada</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Almoxarifado de Destino</label>
                                    <div class="controls">
                                        <input type="text" class="form-control"
                                               value="{{ $transfer->toWarehouse->name }}"
                                               readonly disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Entrada no Almoxarifado de Destino</label>
                                    <div class="controls">
                                        <a href="{{ route('admin.entrada.show', $transfer->entrance_id) }}">Ver Entrada</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Código Interno</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="{{ $transfer->internal_code }}" readonly disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Status</label>
                                    <div class="controls">
                                        <p class="uilabels text-lg">
                                            @if($transfer->status == 'NEW')
                                                <span class="label label-warning"><i class='fa fa-clock-o'></i> Aguardando Aprovação</span>
                                            @elseif($transfer->status == 'PROCESSED')
                                                <span class="label label-info"><i class='fa fa-vote-yea'></i> Processada</span>
                                            @elseif($transfer->status == 'CANCELED')
                                                <span class="label label-danger"><i class='fa fa-times'></i> Cancelado</span>
                                            @elseif($transfer->status == 'APPROVED')
                                                <span class="label label-success"><i class='fa fa-thumbs-up'></i> Aprovado</span>
                                            @endif
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12">
        <ul class="nav nav-tabs content-tabs" id="maincontent" role="tablist">
            <li class="active">
                <a href="#itens" data-toggle="tab">
                    <i class="fa fa-plus"></i> Itens Transferidos
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade in active" id="itens">
                <table id="itemTable" class="table order-list">
                    <thead>
                        <tr>
                            <td>Código Produto</td>
                            <td>Descrição</td>
                            <td>Tipo</td>
                            <td>Quantidade</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    @foreach($transfer->products as $i => $product)
                        {{ $product }}
                        <tr>
                            <td>{{ $product['cod'] }}</td>
                            <td>{{ $product['name'] }}</td>
                            <td>{{ $product['type_name'] }}</td>
                            <td>{{ $product['quantity'] }} {{ $product['unity_name'] }}(s)</td>
                        </tr>
                    @endforeach
                    <tr>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <p>
        <div class="form-group">
            <div class="controls pull-right">
                <button class="btn btn-danger" type="button" onclick="history.back(-1)"><span class="fa fa-chevron-left"></span> Voltar</button>
            </div>
            <div class="clearfix"></div>
        </div>
        </p>
    </div>

    @if($transfer->status == 1 && !empty($transfer->employee))
        <!-- modal start -->
        <form action="{{ route('user.transferencia.approve', $transfer->id)}}" method="post">
            <div class="modal fade col-xs-12" id="employee_confirmation" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Aprovação do Funcionario</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Funcionário</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" value="{{ $transfer->employee->name }}" readonly disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Itens</label>
                                        <div class="controls">
                                            <ul class="list">
                                                @foreach($itens as $i => $item)
                                                    <li>{{ $item->name }} - {{ $transfer->quantity[$i] }} {{ $item->unity->name }}(s)</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Pin</label>
                                        <div class="controls">
                                            @if($transfer->employee->user->pin == null)
                                                <p class="uilabels text-lg">
                                                    <span class="label label-warning"></i>Funcionário sem PIN registrado.</span>
                                                </p>
                                                <a href="{{ route("user.funcionario.user.edit", $transfer->employee->id) }}">
                                                    Clique aqui para registrar um pin para esse funcionário.
                                                </a>
                                            @else
                                                <input type="number" class="form-control" id="pin" name="pin" data-mask="9999" placeholder="Pin Numérico" required>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                            @if($transfer->employee->user->pin != null)
                                <button type="submit" class="btn btn-success" type="button"> Aprovar</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- modal end -->

    @elseif($transfer->status == 3)
        <!-- modal start -->
        <form action="{{ route('user.transferencia.giveback', $transfer->id)}}" method="post">
            <div class="modal fade col-xs-12" id="transfer_giveback" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Devolução de Transferência</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Funcionário</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" value="{{ $transfer->employee->name }}" readonly disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Itens</label>
                                        <div class="controls">
                                            @foreach($itens as $i => $item)
                                                <div class="input-group">
                                                    <span class="input-group-addon">{{ $item->name }}</span>
                                                    <input type="number" class="form-control" id="quantity" name="quantity"
                                                           value="{{ $transfer->quantity[$i] }}" max="{{ $transfer->quantity[$i] }}" min="0" required>
                                                    <span class="input-group-addon" id="basic-addon2">{{ $item->unity->name }}(s)</span>
                                                </div>
                                                <br>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Observação</label>
                                        <div class="controls">
                                            <textarea class="form-control autogrow" cols="5" id="observation" placeholder="Observações na devolução" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 50px;"></textarea>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Senha Almoxarife</label>
                                        <div class="controls">
                                            <input type="password" class="form-control" id="password" name="password" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                            <button type="submit" class="btn btn-primary" type="button"> Aprovar Devolução</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- modal end -->
    @endif

@endsection

@section('post_scripts')
    <script src="/assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script>
        $('#employee_confirmation').on('shown.bs.modal', function () {
            setTimeout(function (){
                $('#pin').focus();
            }, 1000);
        })

        @if(app('request')->input('employee_confirmation') == true)
        $('#employee_confirmation').modal('show');
        @endif

        @if(app('request')->input('transfer_giveback') == true)
        $('#transfer_giveback').modal('show');
        @endif

    </script>
@endsection
