@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Tipos</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Visualizar Tipo</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Tipo</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Nome</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="name" value="{{ $entranceType->name }}" readonly disabled >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="controls">
                                        <button class="btn btn-primary" onclick="history.back(-1)">Voltar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Entradas</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <table id="entrances_tab" class="table datatable display responsive no-wrap" width="100%">
                                <thead>
                                <tr>
                                    <th>Data de Entrada</th>
                                    <th>Nº Nota Fiscal</th>
                                    <th>CNPJ - Fornecedor</th>
                                    <th>Almoxarifado</th>
                                    <th>Obra</th>
                                    <th>Opções</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($entranceType->entrances as $entrance)
                                    <tr>
                                        <td>{{ $entrance->created_at }}</td>
                                        <td>{{ $entrance->doc }}</td>
                                        <td>{{ $entrance->provider['doc'] }} - {{ $entrance->provider['name'] }}</td>
                                        <td>{{ $entrance->warehouse['name'] }}</td>
                                        <td>{{ $entrance->warehouse['building']['name'] }}</td>
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{ route("admin.entrada.show", $entrance->id) }}">
                                                        <button class="btn btn-sm btn-info" title="Visualizar Entrada">
                                                            <span class="fa fa-eye"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            $('.datatable').dataTable({
                responsive: true,
                "language": {
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection
