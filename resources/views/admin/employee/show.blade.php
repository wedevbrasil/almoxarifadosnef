@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Funcionários</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Visualizar Funcionário</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Funcionário</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">CPF</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="doc" value="{{ $employee->doc }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Nome Completo</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="name" value="{{ $employee->name }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Função</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="function" value="{{ $employee->office['name'] }}" readonly disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Setor</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="sector" value="{{ $employee->sector['name'] }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Contato</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="contact" value="{{ $employee->contact }}" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Visualizar Retiradas</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <table class="table datatable display responsive no-wrap" width="100%">
                                <thead>
                                <tr>
                                    <th>Data de Retirada</th>
                                    <th>Nº Nota Fiscal</th>
                                    <th>CNPJ - Fornecedor</th>
                                    <th>Funcionário</th>
                                    <th>Almoxarifado</th>
                                    <th>Obra</th>
                                    <th>Status</th>
                                    <th>Opções</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($employee->withdrawals as $withdrawal)
                                    <tr>
                                        <td>{{ \Carbon\Carbon::parse($withdrawal->created_at)->format('d/m/Y')}}</td>
                                        <td>{{ $withdrawal->doc }}</td>
                                        <td>
                                            <a href="{{ route("admin.fornecedor.show", $withdrawal->provider["id"]) }}">
                                                {{ $withdrawal->provider['doc'] }} - {{ $withdrawal->provider['name'] }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route("admin.funcionario.show", $withdrawal->employee["id"]) }}">
                                                {{ $withdrawal->employee['name'] }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route("admin.almoxarifado.show", $withdrawal->warehouse["id"]) }}">
                                                {{ $withdrawal->warehouse['name'] }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route("admin.obra.show", $withdrawal->warehouse['building']["id"]) }}">
                                                {{ $withdrawal->warehouse['building']['name'] }}
                                            </a>
                                        </td>
                                        <td>
                                            @if($withdrawal->status == 1)
                                                <span class="label label-warning"><i class='fa fa-clock-o'></i> Aguardando Aprovação</span>
                                            @elseif($withdrawal->status == 2)
                                                <span class="label label-danger"><i class='fa fa-times'></i> Cancelado</span>
                                            @elseif($withdrawal->status == 3)
                                                <span class="label label-success"><i class='fa fa-thumbs-up'></i> Aprovado</span>
                                            @elseif($withdrawal->status == 5)
                                                <span class="label label-primary"><i class='fa fa-undo-alt'></i> Devolvida</span>
                                            @endif
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{ route("admin.retirada.show", $withdrawal->id) }}">
                                                        <button class="btn btn-sm btn-info" title="Visualizar Retirada">
                                                            <span class="fa fa-eye"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
