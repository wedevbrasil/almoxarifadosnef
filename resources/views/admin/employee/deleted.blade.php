@extends('layouts.app')

@section('pre_scripts')
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Funcionários Arquivados</h1><!-- PAGE HEADING TAG - END -->
        </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Funcionários</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Todos os Funcionários</h2>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">
                        @if(session()->get('success'))
                            <div class="alert alert-success alert-dismissible fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Sucesso!</strong> {{ session()->get('success') }}
                            </div>
                        @endif

                            <table class="table datatable display responsive no-wrap" width="100%">
                                <thead>
                                <tr>
                                    <th>CPF</th>
                                    <th>Nome</th>
                                    <th>Função</th>
                                    <th>Obra</th>
                                    <th>Setor</th>
                                    <th>Usuário</th>
                                    <th>Contato</th>
                                    <th>Opções</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($employees as $employee)
                                    <tr>
                                        <td data-mask="999.999.999-99">{{ $employee->doc }}</td>
                                        <td>{{ $employee->name }}</td>
                                        <td>{{ $employee->function }}</td>
                                        <td>{{ $employee->sector['name'] }}</td>
                                        <td>
                                            <a href="{{ route("admin.obra.show", $employee->building["id"]) }}">
                                                {{ $employee->building['name'] }}
                                            </a>
                                        </td>
                                        <td>{{ $employee->user['username'] }}</td>
                                        <td data-mask="(99) 9 9999-999[9]">{{ $employee->contact }}</td>
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{ route("admin.funcionario.show", $employee->id) }}">
                                                        <button class="btn btn-sm btn-info" title="Visualizar Funcionário">
                                                            <span class="fa fa-eye"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route("admin.funcionario.edit", $employee->id) }}">
                                                        <button class="btn btn-sm btn-accent" title="Editar Funcionário">
                                                            <span class="fa fa-edit"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route("admin.funcionario.user.edit", $employee->id) }}">
                                                        <button class="btn btn-sm btn-success" title="Adcionar/Editar Usuário">
                                                            <span class="fa fa-user"></span>
                                                        </button>
                                                    </a>
                                                </li>
                                                <li>
                                                    <form action="{{ route('admin.funcionario.restore', $employee->id)}}" method="post">
                                                        @csrf
                                                        <meta name="_token" content="{{ csrf_token() }}">
                                                        @method('POST')
                                                        <button class="btn btn-sm btn-default" title="Desativar Funcionário"
                                                                data-toggle="confirm"
                                                                data-title=""
                                                                data-message="Tem certeza que deseja restaurar esse funcionário?"
                                                                data-type="success">
                                                            <span class="fa fa-recycle"></span>
                                                        </button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <script src="/assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            $('.datatable').dataTable({
                responsive: true,
                "language": {
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection
