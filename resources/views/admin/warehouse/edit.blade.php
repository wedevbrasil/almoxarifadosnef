@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Almoxarifados</h1><!-- PAGE HEADING TAG - END -->                            </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Editar Almoxarifado</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Editar Almoxarifado</h2>
                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-10">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form action="{{ route("admin.almoxarifado.update", $warehouse->id) }}" method="POST">
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">
                                    @method('PATCH')
                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Nome</label>
                                        <span class="desc">ex: "Almoxarifado II"</span>
                                        <div class="controls">
                                            <input type="text" class="form-control" id="name" name="name" value="{{ $warehouse->name }}" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Obra</label>
                                        <div class="controls">
                                            <select class="select2 disabled" id="building_id" name="building_id"  >
                                                <option value="">Selecione uma Obra</option>
                                                @foreach($obras as $obra)
                                                    <option value="{{ $obra->id }}" @if($obra->id == $warehouse->building['id']) selected @endif>{{ $obra->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Almoxarife</label>
                                        <div class="controls">
                                            <select class="select2 disabled" id="manager" name="manager"  >
                                                <option value="">Selecione um Almoxarife</option>
                                                @foreach($employees as $employee)
                                                    <option value="{{ $employee->id }}" @if($employee->id == $warehouse->manager()->first()['id']) selected @endif>{{ $employee->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Descrição</label>
                                        <span class="desc">ex: "Almoxarifado do segundo andar"</span>
                                        <div class="controls">
                                            <textarea type="text" class="form-control" id="description" name="description">{{ $warehouse->description }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="controls">
                                            <button class="btn btn-primary" type="submit">Salvar</button>
                                            <button class="btn btn-danger" type="button" onclick="history.back(-1)">Cancelar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
