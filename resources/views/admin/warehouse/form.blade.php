<div class="form-group">
    <label class="form-label" for="field-1">Nome</label>
    <span class="desc">ex: "Almoxarifado II"</span>
    <div class="controls">
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
    </div>
</div>

<div class="form-group">
    <label class="form-label" for="field-1">Obra</label>
    <span class="desc">ex: "Metrô - Transport - Linha 4"</span>
    <div class="controls">
        <input type="text" class="form-control" id="building" name="building" value="{{ old('building') }}">
    </div>
</div>

<div class="form-group">
    <label class="form-label" for="field-1">Descrição</label>
    <span class="desc">ex: "Almoxarifado do segundo andar"</span>
    <div class="controls">
        <textarea class="form-control" id="description" name="description">{{ old('description') }}</textarea>
    </div>
</div>
