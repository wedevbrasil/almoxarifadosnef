@extends('layouts.app')

@section('pre_scripts')
    <link href="/assets/plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/assets/plugins/switchery/dist/switchery.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/assets/plugins/icheck/skins/square/_all.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="/assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Nova Retirada</h1><!-- PAGE HEADING TAG - END -->
        </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="">
                    <a href="#">Retirada</a>
                </li>
                <li class="active">
                    <a href="#"><strong>Nova Retirada</strong></a>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <form id="user_witdrawal_create" action="{{ route("admin.retirada.store") }}" method="post">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">Dados da Retirada</h2>
                    <div class="actions panel_actions pull-right">
                        <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <div class="col-md-4 col-sm-5 col-xs-10">
                                    @method('post')
                                    @csrf
                                    <meta name="_token" content="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Tipo de Retirada</label>
                                        <div class="controls">
                                            <select class="select2" id="type" name="type" onchange="withdrawalType()">
                                                <option value="">Selecione um tipo</option>
                                                @foreach($withdrawalTypes as $withdrawalType)
                                                    @if($withdrawalType->id != "5d6f51e020dd0f284f624b39") {{-- Retirar "Movimentação Interna" --}}
                                                    <option value="{{ $withdrawalType->id }}" @if( $withdrawalType->id == old("type")) selected @endif>{{ $withdrawalType->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Controle Interno</label>
                                        <div class="controls">
                                            <div class="input-group">
                                                <input id="output" type="text" class="form-control" name="doc" value="{{ old("doc") }}">
                                                <div class="input-group-btn">
                                                    <button id="generate" type="button" class="btn btn-default">
                                                        <i class="fa fa-sync-alt"></i> Gerar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-5 col-xs-10">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Almoxarifado de Origem</label>
                                        <div class="controls">
                                            <select class="select2" id="warehouse" name="warehouse" required>
                                                <option value="">Selecione um Almoxarifado</option>
                                                @foreach($warehouses as $warehouse)
                                                    <option value="{{ $warehouse->id }}" {{ ( old("warehouse") == $warehouse->id ? "selected":"") }}>{{ $warehouse->name }} - {{ $warehouse->building['number'] }} - {{ $warehouse->building['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <style>
                                        input[type="date"]::-webkit-calendar-picker-indicator {
                                            background: transparent;
                                            bottom: 0;
                                            color: transparent;
                                            cursor: pointer;
                                            height: auto;
                                            left: 0;
                                            position: absolute;
                                            right: 0;
                                            top: 0;
                                            width: 85%;
                                        }
                                    </style>

                                    <div class="form-group input-container" id="giveback-date-div">
                                        <div class="form-group">
                                            <label class="form-label" >Data de Devolução</label>
                                            <div class="controls">
                                                <input type="date" id="giveback_date" name="giveback_date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4 col-sm-5 col-xs-10">

                                    <div class="form-group" id="employee-div">
                                        <label class="form-label" for="field-1">Funcionário</label>
                                        <div class="controls">
                                            <select class="select2" id="employee" name="employee">
                                                <option value="">Selecione um Funcionário</option>
                                                @foreach($employees as $employee)
                                                    <option value="{{ $employee->id }}" {{ ( old("employee") == $employee->id ? "selected":"") }}>{{ $employee->name }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    {{--<div class="form-group" id="employee-perm-div">
                                        <label class="form-label" for="field-1">Pedir aprovação do Funcionário ao final?</label>
                                        <div class="controls">
                                            <div class="form-block bottom15">
                                                <input type="checkbox" id="employee_aprovation" class="js-switch" name="employee_aprovation" />
                                            </div>
                                        </div>
                                    </div>--}}

                                    {{--<div class="form-group" id="warehouse-destination-div">
                                        <label class="form-label" for="field-1">Almoxarifado de Destino</label>
                                        <div class="controls">
                                            <select class="select2" id="warehouse-destination" name="warehouse-destination">
                                                <option value="">Selecione um Almoxarifado</option>
                                                @foreach($warehousesDestination as $warehouse)
                                                    <option value="{{ $warehouse->id }}" {{ ( old("warehouse") == $warehouse->id ? "selected":"") }}>{{ $warehouse->name }} - {{ $warehouse->building['number'] }} - {{ $warehouse->building['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" id="invoice-perm-div">
                                        <label class="form-label" for="field-1">Gerar fatura ao final?</label>
                                        <div class="controls">
                                            <div class="form-block bottom15">
                                                <input type="checkbox" id="invoice" class="js-switch" name="invoice" value="true" checked />
                                            </div>
                                        </div>
                                    </div>--}}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="box ">
                <header class="panel_header">
                    <h2 class="title pull-left">Produtos</h2>
                    <div class="actions panel_actions pull-right">
                        <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12">
                            <table id="itemTable" class="table table-bordered table-responsive order-list">
                                <thead>
                                <tr>
                                    <td>Código Produto - Descrição - Tipo</td>
                                    <td>Valor Unitário</td>
                                    <td>Quantidade Retirada</td>
                                    <td>Subtotal</td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody id="dynamic-container">
                                <tr>
                                    <td style="max-width: 350px">
                                        <select class="select2 tool" name="tool[0][id]" required>
                                            <option value="">Selecione uma Ferramenta</option>
                                            @foreach($tools as $i => $tool)
                                                <option value="{{ $tool->id }}"
                                                        data-unity="{{ $tool->unity['symbol'] }}"
                                                        data-value="{{ $tool->value }}"
                                                        data-key="0"
                                                >
                                                    {{ $tool->cod }} - {{ $tool->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td style="width: 200px">

                                        <div class="input-group transparent">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control num1" id="value0" data-key="0" name="tool[0][value]"
                                               {{--data-mask="currency" data-sign="R$"--}} required>
                                        </div>
                                    </td>

                                    <td style="width: 200px">

                                        <div class="input-group transparent">
                                            <input type="number" class="form-control num2" id="quantity0" data-key="0" name="tool[0][quantity]"
                                                   min="1" required>
                                            <span id="unity0" class="input-group-addon"></span>
                                        </div>
                                    </td>
                                    <td style="width: 200px">
                                        <div class="input-group transparent">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control sum" data-key="0" id="total0" readonly>
                                        </div>
                                    </td>
                                    <td style="width: 60px">
                                        <button type="button" class="ibtnDel btn btn-md btn-danger pull-right" disabled>
                                            <i class="fa fa-minus-circle"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td class="no-line" colspan="3"></td>
                                    <td class="no-line text-center" colspan="3">
                                        <h4 class="pull-left">Total</h4>
                                        <h3 style='margin:0px; padding-top: 5px;' class="text-primary pull-right"
                                            data-total-value="0">R$ <span id="total">0,00</span></h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="text-align: left;">
                                        <button type="button" class="btn btn-lg btn-block " id="addrow">
                                            <i class="fa fa-plus-circle"></i>  Adicionar Item
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            {{--<section class="box ">
                <br />
                <div class="content-body">
                    <div class="row">
                        <div class="col-xl-10 col-md-12 col-sm-12 col-xs-12">

                            <div class="form-group">
                                <div class="controls pull-right">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-save"></i> Salvar
                                    </button>
                                    <button class="btn btn-danger" type="button" onclick="history.back(-1)">
                                        <i class="fa fa-ban"></i> Cancelar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>--}}

            <div class="form-group">
                <div class="controls pull-right">
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-save"></i> Salvar
                    </button>
                    <button class="btn btn-danger" type="button" onclick="history.back(-1)">
                        <i class="fa fa-ban"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('post_scripts')
    <script src="/assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/switchery/dist/switchery.js" type="text/javascript"></script>
    <script src="/assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            $(function() {
                var counter = 0;
                $("#addrow").on("click", function () {
                    counter++;

                    var newRow = $("<tr>");
                    var cols = "";

                    cols += '<td>' +
                        '<select class="select2 tool" name="tool[' + counter + '][id]" required>' +
                        '   <option value="">Selecione uma Ferramenta</option>' +
                        '       @foreach($tools as $tool)' +
                        '           <option value="{{ $tool->id }}" data-unity="{{ $tool->unity['symbol'] }}"' +
                        '           data-value="{{ $tool->value }}"' +
                        '           data-key="' + counter + '">{{ $tool->cod }} - {{ $tool->name }}</option>' +
                        '       @endforeach' +
                        '</select>' +
                        '</td>';

                    cols += '<td>' +
                        '<div class="input-group transparent">'+
                        '<span class="input-group-addon">R$</span>'+
                        '<input type="text" class="form-control num1" ' +
                        'id="value' + counter + '" ' +
                        'data-key="' + counter + '" ' +
                        'name="tool[' + counter + '][value]"' +
                        '{{-- data-mask="currency" data-sign="R$"--}} required>' +
                        '</div>'+
                        '</td>';

                    cols += '<td>' +
                        '<div class="input-group transparent">'+
                        '   <input type="text" class="form-control num2" ' +
                        'id="quantity' + counter + '" ' +
                        'data-key="' + counter + '" ' +
                        'name="tool[' + counter + '][quantity]">'+
                        '   <span id="unity' + counter + '" class="input-group-addon"></span>'+
                        '</div>'+
                        '</td>';

                    cols += '<td>' +
                        '<div class="input-group transparent">'+
                        '<span class="input-group-addon">R$</span>'+
                        '<input type="text" class="form-control sum" ' +
                        'data-key="' + counter + '" id="total' + counter + '" readonly>' +
                        '</div>'+
                        '</td>';

                    cols += '<td>' +
                        '<button type="button" class="ibtnDel btn btn-md btn-danger pull-right">' +
                        '<i class="fa fa-minus-circle"></i>' +
                        '</td>';

                    newRow.append(cols);

                    $("#dynamic-container").append(newRow);
                    $('select.select2').select2();
                });


                $("table.order-list").on("click", ".ibtnDel", function (event) {
                    $(this).closest("tr").remove();
                });
            });
        });


        function withdrawalType() {
            if ($("#type").val() === "5d490461c70471340e0fd4ce")  {
                $("#warehouse-destination-div").show();
                $("#employee-div").hide();
                $("#employee-perm-div").hide();
                $("#giveback-date-div").hide();
                $("#invoice-perm-div").show();
            }
            else if ($("#type").val() === "5d4904b3c70471340e0fd4cf")  {
                $("#warehouse-destination-div").hide();
                $("#employee-div").show();
                $("#employee-perm-div").show();
                $("#giveback-date-div").show();
                $("#invoice-perm-div").hide();
            }
            else {
                $("#warehouse-destination-div").hide();
                $("#employee-div").show();
                $("#employee-perm-div").show();
                $("#giveback-date-div").hide();
                $("#invoice-perm-div").hide();
            }
        }

        $("#warehouse-destination-div").hide(); //Esconder almoxarifado destino no inicio
        $("#invoice-perm-div").hide();

        (function() {
            function IDGenerator() {

                this.length = 8;
                this.timestamp = +new Date;

                var _getRandomInt = function( min, max ) {
                    return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
                }

                this.generate = function() {
                    var ts = this.timestamp.toString();
                    var parts = ts.split( "" ).reverse();
                    var id = "";

                    for( var i = 0; i < this.length; ++i ) {
                        var index = _getRandomInt( 0, parts.length - 1 );
                        id += parts[index];
                    }

                    return id;
                }

            }


            document.addEventListener( "DOMContentLoaded", function() {
                var btn = document.querySelector( "#generate" ),
                    output = document.querySelector( "#output" );

                btn.addEventListener( "click", function() {
                    var generator = new IDGenerator();
                    output.value = generator.generate();

                }, false);

            });


        })();

    </script>

    <script>
        $(document).on('change', '.tool', function () {
                $('#unity' + $('option:selected', this).data('key')).html($('option:selected', this).data('unity') + '(s)');
                $('#value' + $('option:selected', this).data('key')).val($('option:selected', this).data('value'));
        });

        $(document).on('keydown keyup change', '.num1, .num2', function () {
                $('#total' + $(this).data('key')).val(
                    Number(
                        $('#value' + $(this).data('key')).val()
                    ) *
                    Number(
                        $('#quantity' + $(this).data('key')).val()
                    )
                );
                var sum = 0.00;
                $('.sum').each(function(){
                    sum += parseFloat(this.value)
                });
                $('#total').html(sum);
            });

        $(document).on('keydown keyup change', '.sum', function () {

        });

    </script>
@endsection
