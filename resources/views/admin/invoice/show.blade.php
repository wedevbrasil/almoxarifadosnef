@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Faturas</h1><!-- PAGE HEADING TAG - END -->
        </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Faturas</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Fatura</h2>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="invoice-head row">
                                    <div class="col-xs-12 col-md-2 invoice-title">
                                        <h2 class="text-center bg-primary ">#{{$invoice->order}}</h2>
                                    </div>
                                    <div class="col-xs-12 col-md-3 invoice-head-info">
                                    <span class='text-muted'>
                                        795 Folsom Ave, Suite 600<br>
                                        San Francisco, CA 94107<br>
                                        P: (123) 456-7890<br>
                                    </span>
                                    </div>
                                    <div class="col-xs-12 col-md-3 invoice-head-info">
                                        <span class='text-muted'>Nº Ordem #{{$invoice->order}}<br>
                                            @php \Carbon\Carbon::setLocale('pt_BR'); @endphp
                                           {{\Carbon\Carbon::createFromTimeStamp(strtotime($invoice->created_at))->toDayDateTimeString()}}</span>
                                    </div>
                                    <div class="col-xs-12 col-md-3 invoice-logo col-md-offset-1">
                                        <img alt="" src="../data/invoice/invoice-logo.png" class="img-reponsive">
                                    </div>
                                </div>
                                <div class="clearfix"></div><br>
                            </div>


                            <div class="col-xs-6 invoice-infoblock pull-left">
                                <h4>Faturado para:</h4>
                                <h3>{{ $invoice->toWarehouse->name }}</h3>
                                <address>
                                    <span class='text-muted'>1234 Main Street<br>Apt. 34/4B<br>
                                        Springfield<br>ST 54321</span>
                                </address>
                            </div>

                            <div class="col-xs-6 invoice-infoblock text-right">
                                <h4>Payment Method:</h4>
                                <h3>Credit Card</h3>
                                <address>
                                    <span class='text-muted'>Visa ending **** 4242<br>
                                        jsmith@email.com</span>
                                </address>

                                <div class="invoice-due">
                                    <h3 class="text-muted">Total:</h3> &nbsp; <h2 class="text-primary">R$ {{ $invoice->total_price }}</h2>
                                </div>
                                <div class="clearfix"></div><br>
                            </div>




                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <h3>Itens da fatura</h3><br>
                                <div class="table-responsive">
                                    <table class="table table-hover invoice-table">
                                        <thead>
                                        <tr>
                                            <td><h4>Item</h4></td>
                                            <td class="text-center"><h4>Valor Unitário</h4></td>
                                            <td class="text-center"><h4>Quantidade</h4></td>
                                            <td class="text-right"><h4>Subtotal</h4></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($invoice->products as $product)
                                        <tr>
                                            <td>{{ $product['name'] }}</td>
                                            <td class="text-center">R$ {{ $product['valor'] }}</td>
                                            <td class="text-center">{{$product['quantity']}}</td>
                                            <td class="text-right">R$ {{ $product['valor'] *  $product['quantity'] }}</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line text-center"><h4>Total</h4></td>
                                            <td class="no-line text-right"><h3 style='margin:0px;' class="text-primary">R$ {{ $invoice->total_price }}</h3></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="clearfix"></div><br>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
