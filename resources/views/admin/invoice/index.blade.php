@extends('layouts.app')

@section('page-header')
    <div class="page-title">

        <div class="pull-left">
            <!-- PAGE HEADING TAG - START --><h1 class="title">Faturas</h1><!-- PAGE HEADING TAG - END -->
        </div>

        <div class="pull-right hidden-xs">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active">
                    <strong>Faturas</strong>
                </li>
            </ol>
        </div>

    </div>
@endsection

@section('content')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Todas as Faturas</h2>
            </header>
            <div class="content-body">
                <div class="row">
                    @if(session()->get('success'))
                        <div class="alert alert-success alert-dismissible fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <strong>Sucesso!</strong> {{ session()->get('success') }}
                        </div>
                    @endif
                    <div class="col-xs-12">

                        <table class="table table-striped table-bordered nowrap" width="100%">
                            <thead>
                            <tr>
                                <th>Numero da Ordem</th>
                                <th>Tipo da Transação</th>
                                <th>Emissor</th>
                                <th>Para</th>
                                <th>Transação</th>
                                <th>Data Emissão</th>
                                <th>Preço</th>
                                <th>Status</th>
                                <th>Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invoices as $invoice)
                                <tr>
                                    <td>{{ $invoice->order }}</td>
                                    <td>
                                        @if($invoice->type == 'TRANSFER')
                                            <span class="badge badge-info"><i class='fa fa-exchange-alt'></i> Movimentação Interna</span>
                                       @endif
                                    </td>
                                    <td>{{ $invoice->fromWarehouse->name }}</td>
                                    <td>{{ $invoice->toWarehouse->name }}</td>
                                    <td>
                                        <a href="#">Ver Transação</a>
                                    </td>
                                    <td>{{ \Carbon\Carbon::parse($invoice->created_at)->format('d/m/Y')}}</td>
                                    <td>R$ {{ money_format('%i', $invoice->total_price) }}</td>
                                    <td>
                                        @if($invoice->status == 'NEW')
                                            <span class="label label-warning"><i class='fa fa-clock-o'></i> Aguardando Pagamento</span>
                                        @elseif($invoice->status == 'CANCELED')
                                            <span class="label label-danger"><i class='fa fa-times'></i> Cancelado</span>
                                        @elseif($invoice->status == 'APPROVED')
                                            <span class="label label-success"><i class='fa fa-thumbs-up'></i> Aprovado</span>
                                        @elseif($invoice->status == 'PAID')
                                            <span class="label label-primary"><i class='fa fa-check'></i> Pago</span>
                                        @endif
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="{{ route('admin.fatura.show', $invoice->id) }}">
                                                    <button class="btn btn-sm btn-info" title="Visualizar Fatura">
                                                        <span class="fa fa-eye"></span>
                                                    </button>
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('post_scripts')
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
    <script>
        $(document).ready( function () {
            $('.datatable').dataTable({
                responsive: true,
                "language": {
                    "url": "/assets/js/datatables-1.10.18/Portuguese-Brasil.json"
                }
            });
        } );
    </script>
@endsection
