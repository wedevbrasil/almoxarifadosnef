<!DOCTYPE html>
<html class=" ">
<head>
    <!--
     * @Package: Complete Admin - Responsive Theme
     * @Subpackage: Bootstrap
     * @Version: 2.2
     * This file is part of Complete Admin Theme.
    -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>SNEF - Controle de Almoxarifado</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="Controle de Almoxarifado" name="description" />
    <meta content="ConstructWeb" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/icons/apple-touch-icon.png?v=jw7GqM8blE">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/icons/favicon-32x32.png?v=jw7GqM8blE">
    <link rel="icon" type="image/png" sizes="192x192" href="/assets/icons/android-chrome-192x192.png?v=jw7GqM8blE">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/icons/favicon-16x16.png?v=jw7GqM8blE">
    <link rel="manifest" href="/assets/icons/site.webmanifest?v=jw7GqM8blE">
    <link rel="mask-icon" href="/assets/icons/safari-pinned-tab.svg?v=jw7GqM8blE" color="#134441">
    <link rel="shortcut icon" href="/assets/icons/favicon.ico?v=jw7GqM8blE">
    <meta name="apple-mobile-web-app-title" content="SNEF">
    <meta name="application-name" content="SNEF">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/assets/icons/mstile-144x144.png?v=jw7GqM8blE">
    <meta name="msapplication-config" content="/assets/icons/browserconfig.xml?v=jw7GqM8blE">
    <meta name="theme-color" content="#09423f">

    <!-- All Login Style -->
    <link href="/assets/css/login.css" rel="stylesheet" type="text/css" media="screen"/>

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class=" login_page">

<div class="container-fluid">
    <div class="login-wrapper row">
        <div id="login" class="login loginpage col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 col-xs-12 col-sm-6 col-lg-4">
            <h1>
                <a href="#" title="Login Page" tabindex="-1">SNEF - Controle de Almoxarifado</a>
            </h1>

            <form name="loginform" id="loginform" action="{{ route('login') }}" method="POST">
                @csrf
                <meta name="_token" content="{{ csrf_token() }}">
                <p>
                    <label for="user_login">Usuário<br />
                        <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                        @if ($errors->has('username'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </label>
                </p>
                <p>
                    <label for="user_pass">Senha<br />
                        <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </label>
                </p>
                <p class="forgetmenot">
                    <label class="icheck-label form-label" for="rememberme">
                        <input class="skin-square-green" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Lembrar Login</label>
                </p>



                <p class="submit">
                    <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-accent btn-block" value="Entrar" />
                </p>
            </form>

            <p id="nav">
                <a class="pull-left" href="#" title="Password Lost and Found">Esqueceu a Senha?</a>
            </p>


        </div>
    </div>
</div>

<!-- CORE JS FRAMEWORK - START -->
<script src="/assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
<!-- CORE JS FRAMEWORK - END -->


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->

<script src="/assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


<!-- CORE TEMPLATE JS - START -->
<script src="/assets/js/scripts.login.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS - END -->

<!-- PWA -->
<script>
    if ('serviceWorker' in navigator ) {
        window.addEventListener('load', function() {
            navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
                // Registration was successful
                console.log('ServiceWorker registration successful with scope: ', registration.scope);
            }, function(err) {
                // registration failed :(
                console.log('ServiceWorker registration failed: ', err);
            });
        });
    }
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('/sw.js').then(registration => {
                console.log('SW registered: ', registration);
            }).catch(registrationError => {
                console.log('SW registration failed: ', registrationError);
            });
        });
    }
</script>
</body>
</html>
