<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes(['register' => true]);

// Links Painel Admin
Route::group([
    'middleware' => 'auth',
    'prefix' => 'admin',
    'namespace' => 'Admin'], function() {

    // Listar Inativos
    Route::get('/almoxarifado/arquivado', 'WarehouseController@deleted')->name('admin.almoxarifado.deleted');
    Route::get('/fornecedor/arquivado', 'ProviderController@deleted')->name('admin.fornecedor.deleted');
    Route::get('/funcionario/arquivado', 'EmployeeController@deleted')->name('admin.funcionario.deleted');
    Route::get('/ferramenta/arquivado', 'ToolController@deleted')->name('admin.ferramenta.deleted');
    Route::get('/obra/arquivado', 'BuildingController@deleted')->name('admin.obra.deleted');
    Route::get('/setor/arquivado', 'SectorController@deleted')->name('admin.setor.deleted');
    Route::get('/funcao/arquivado', 'OfficeController@deleted')->name('admin.funcao.deleted');
    Route::get('/unidade/arquivado', 'UnityController@deleted')->name('admin.unidade.deleted');
    Route::get('/tipoitem/arquivado', 'ItemTypeController@deleted')->name('admin.tipoitem.deleted');
    Route::get('/tipoentrada/arquivado', 'ItemTypeController@deleted')->name('admin.tipoentrada.deleted');
    Route::get('/entrada/arquivado', 'EntranceController@deleted')->name('admin.entrada.deleted');
    Route::get('/retirada/arquivado', 'WithdrawalController@deleted')->name('admin.retirada.deleted');

    // Restaurar Inativos
    Route::post('/almoxarifado/restore/{id}', 'WarehouseController@restore')->name('admin.almoxarifado.restore');
    Route::post('/fornecedor/restore/{id}', 'ProviderController@restore')->name('admin.fornecedor.restore');
    Route::post('/funcionario/restore/{id}', 'EmployeeController@restore')->name('admin.funcionario.restore');
    Route::post('/ferramenta/restore/{id}', 'ToolController@restore')->name('admin.ferramenta.restore');
    Route::post('/obra/restore/{id}', 'BuildingController@restore')->name('admin.obra.restore');
    Route::post('/setor/restore/{id}', 'SectorController@restore')->name('admin.setor.restore');
    Route::post('/funcao/restore/{id}', 'OfficeController@restore')->name('admin.funcao.restore');
    Route::post('/unidade/restore/{id}', 'UnityController@restore')->name('admin.unidade.restore');
    Route::post('/tipoitem/restore/{id}', 'ItemTypeController@restore')->name('admin.tipoitem.restore');
    Route::post('/tipoentrada/restore/{id}', 'EntranceTypeController@restore')->name('admin.tipoentrada.restore');
    Route::post('/entrada/restore/{id}', 'EntranceController@restore')->name('admin.entrada.restore');
    Route::post('/retirada/restore/{id}', 'WithdrawalController@restore')->name('admin.retirada.restore');

    Route::get('/funcionario/usuario/{id}', 'EmployeeController@userEdit')->name('admin.funcionario.user.edit');
    Route::patch('/funcionario/usuario/{id}', 'EmployeeController@userUpdate')->name('admin.funcionario.user.update');

    // Resources
    Route::resource('fornecedor', 'ProviderController', ['as' => 'admin']);
    Route::resource('almoxarifado', 'WarehouseController', ['as' => 'admin']);
    Route::resource('funcionario', 'EmployeeController', ['as' => 'admin']);
    Route::resource('obra', 'BuildingController', ['as' => 'admin']);
    Route::resource('ferramenta', 'ToolController', ['as' => 'admin']);
    Route::resource('entrada', 'EntranceController', ['as' => 'admin']);
    Route::resource('retirada', 'WithdrawalController', ['as' => 'admin']);
    Route::resource('setor', 'SectorController', ['as' => 'admin']);
    Route::resource('funcao', 'OfficeController', ['as' => 'admin']);
    Route::resource('unidade', 'UnityController', ['as' => 'admin']);
    Route::resource('tipoitem', 'ItemTypeController', ['as' => 'admin']);
    Route::resource('tipoentrada', 'EntranceTypeController', ['as' => 'admin']);
    Route::resource('usuario', 'UserController', ['as' => 'admin']);
    Route::resource('fatura', 'InvoiceController', ['as' =>'admin']);
    Route::resource('transferencia', 'TransferController', ['as' =>'admin']);
    Route::resource('estoque', 'InventoryController', ['as' => 'admin']);


    Route::get('/usuarios', 'UserController@index')->name('admin.user.index');

    Route::get('/ferramentas/datatables.json', 'ToolController@datatablesAjax')->name('admin.ferramenta.datatables');

    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard.index');

    Route::post('/transferencia/cancel/{id}', 'TransferController@cancel')->name('admin.transferencia.cancel');

    Route::post('/import/{type}', 'ImportController@import')->name('admin.import.submit');
});

Route::group([
    'middleware' => 'auth',
    'prefix' => 'user',
    'namespace' => 'User'], function() {

    // Listar Inativos
    Route::get('/almoxarifado/arquivado', 'WarehouseController@deleted')->name('user.almoxarifado.deleted');
    Route::get('/fornecedor/arquivado', 'ProviderController@deleted')->name('user.fornecedor.deleted');
    Route::get('/funcionario/arquivado', 'EmployeeController@deleted')->name('user.funcionario.deleted');
    Route::get('/ferramenta/arquivado', 'ToolController@deleted')->name('user.ferramenta.deleted');
    Route::get('/obra/arquivado', 'BuildingController@deleted')->name('user.obra.deleted');
    Route::get('/setor/arquivado', 'SectorController@deleted')->name('user.setor.deleted');
    Route::get('/funcao/arquivado', 'OfficeController@deleted')->name('user.funcao.deleted');
    Route::get('/unidade/arquivado', 'UnityController@deleted')->name('user.unidade.deleted');
    Route::get('/tipoitem/arquivado', 'ItemTypeController@deleted')->name('user.tipoitem.deleted');
    Route::get('/tipoentrada/arquivado', 'ItemTypeController@deleted')->name('user.tipoentrada.deleted');

    // Restaurar Inativos
    Route::post('/almoxarifado/restore/{id}', 'WarehouseController@restore')->name('user.almoxarifado.restore');
    Route::post('/fornecedor/restore/{id}', 'ProviderController@restore')->name('user.fornecedor.restore');
    Route::post('/funcionario/restore/{id}', 'EmployeeController@restore')->name('user.funcionario.restore');
    Route::post('/ferramenta/restore/{id}', 'ToolController@restore')->name('user.ferramenta.restore');
    Route::post('/obra/restore/{id}', 'BuildingController@restore')->name('user.obra.restore');
    Route::post('/setor/restore/{id}', 'SectorController@restore')->name('user.setor.restore');
    Route::post('/funcao/restore/{id}', 'OfficeController@restore')->name('user.funcao.restore');
    Route::post('/unidade/restore/{id}', 'UnityController@restore')->name('user.unidade.restore');
    Route::post('/tipoitem/restore/{id}', 'ItemTypeController@restore')->name('user.tipoitem.restore');
    Route::post('/tipoentrada/restore/{id}', 'EntranceTypeController@restore')->name('user.tipoentrada.restore');

    Route::get('/funcionario/usuario/{id}', 'EmployeeController@userEdit')->name('user.funcionario.user.edit');
    Route::patch('/funcionario/usuario/{id}', 'EmployeeController@userUpdate')->name('user.funcionario.user.update');

    // Resources
    Route::resource('fornecedor', 'ProviderController', ['as' => 'user']);
    Route::resource('almoxarifado', 'WarehouseController', ['as' => 'user']);
    Route::resource('funcionario', 'EmployeeController', ['as' => 'user']);
    Route::resource('obra', 'BuildingController', ['as' => 'user']);
    Route::resource('ferramenta', 'ToolController', ['as' => 'user']);
    Route::resource('entrada', 'EntranceController', ['as' => 'user']);
    Route::resource('retirada', 'WithdrawalController', ['as' => 'user']);
    Route::resource('setor', 'SectorController', ['as' => 'user']);
    Route::resource('funcao', 'OfficeController', ['as' => 'user']);
    Route::resource('unidade', 'UnityController', ['as' => 'user']);
    Route::resource('tipoitem', 'ItemTypeController', ['as' => 'user']);
    Route::resource('tipoentrada', 'EntranceTypeController', ['as' => 'user']);

    Route::get('/usuarios', 'UserController@index')->name('user.user.index');
    Route::post('/retirada/{id}/cancel', 'WithdrawalController@cancel')->name('user.retirada.cancel');
    Route::post('/retirada/{id}/approve', 'WithdrawalController@approve')->name('user.retirada.approve');
    Route::post('/retirada/{id}/giveback', 'WithdrawalController@giveback')->name('user.retirada.giveback');

    Route::get('/profile', 'UserController@profile')->name('user.profile');

    Route::get('/dashboard', 'DashboardController@index')->name('user.dashboard.index');
});

Route::post('/reportbug', 'ReportController@report')->name('reportbug');
