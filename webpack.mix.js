const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/assets/js')
    .sass('resources/sass/app.scss', 'public/assets/css');


var SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
var workboxPlugin = require('workbox-webpack-plugin');

mix.webpackConfig({
    plugins: [
        new SWPrecacheWebpackPlugin({
            cacheId: 'pwa',
            filename: 'service-worker.js',
            staticFileGlobs: ['public/**/*.{css,eot,svg,ttf,woff,woff2,js,html,png}'],
            minify: true,
            stripPrefix: 'public/',
            handleFetch: true,
            dynamicUrlToDependencies: { //you should add the path to your blade files here so they can be cached
                //and have full support for offline first (example below)
                '/': ['resources/views/home.blade.php'],
                '/admin/dashboard': ['resources/views/admin/dashboard.blade.php'],
                '/login': ['resources/views/auth/login.blade.php'],
                //'/admin/almoxarifado': ['resources/views/admin/warehouse/index.blade.php'],
                //'/user/almoxarifado': ['resources/views/user/warehouse/index.blade.php']
            },
            staticFileGlobsIgnorePatterns: [/\.map$/, /mix-manifest\.json$/, /manifest\.json$/, /service-worker\.js$/],
            navigateFallback: '/',
            runtimeCaching: [
                {
                    urlPattern: /^https:\/\/fonts\.googleapis\.com\//,
                    handler: 'cacheFirst'
                }
            ],
            //importScripts: ['./js/**/*.*']
        }),
        new workboxPlugin.GenerateSW({
            swDest: 'sw.js',
            clientsClaim: true,
            skipWaiting: true,runtimeCaching: [{
                urlPattern: new RegExp('https://snef.com.co'),
                handler: 'NetworkFirst'
            }]
        })
    ]
});

mix.styles([
    'public/assets/plugins/bootstrap/css/bootstrap.min.css',
    'public/assets/plugins/bootstrap/css/bootstrap-theme.min.cs',
    'public/assets/fonts/font-awesome/css/font-awesome.css',
    'public/assets/plugins/icheck/skins/square/_all.css',
    'public/assets/css/style.css',
    'public/assets/css/responsive.css'
], 'public/assets/css/login.css');

mix.styles([
], 'public/assets/css/app.css');

mix.js([
    'public/assets/js/jquery-1.11.2.min.js',
    'public/assets/js/jquery.easing.min.js',
    'public/assets/plugins/bootstrap/js/bootstrap.min.js',
    'public/assets/plugins/pace/pace.min.js',
    'public/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js',
    'public/assets/plugins/viewport/viewportchecker.js',
    'public/assets/js/popper.min.js',
    'public/assets/js/scripts.js',
    'public/assets/js/bootstrap.confirm.js',
    'public/assets/js/bootstrap-tabcollapse.js'
], 'public/assets/js/all.js');

mix.js([
    'public/assets/js/jquery-1.11.2.min.js',
    'public/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js',
    'public/assets/plugins/icheck/icheck.min.js',
    'public/assets/js/scripts.js'
], 'public/assets/js/login.js');
/*
mix.js([
    'public/assets/plugins/datatables/js/jquery.dataTables.min.js',
    'public/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
    'public/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js',
    'public/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js',
    'public/assets/plugins/datatables/js/dataTables.buttons.min.js',
    'public/assets/plugins/datatables/js/buttons.flash.min.js',
    'public/assets/plugins/datatables/js/jszip.min.js',
    'public/assets/plugins/datatables/js/pdfmake.min.js',
    'public/assets/plugins/datatables/js/vfs_fonts.js',
    'public/assets/plugins/datatables/js/buttons.html5.min.js',
    'public/assets/plugins/datatables/js/buttons.print.min.js'
], 'public/assets/js/datatables.js');
*/
mix.minify('public/assets/js/login.js');
mix.minify('public/assets/css/login.css');
mix.minify('public/assets/js/all.js');
//mix.minify('public/assets/js/datatables.js');
mix.minify('public/assets/css/app.css');

mix.autoload({
  jquery: ['$', 'window.jQuery', 'jQuery', 'jquery']
});
