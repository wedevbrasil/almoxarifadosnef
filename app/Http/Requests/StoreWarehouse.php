<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreWarehouse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|min:3',
            'building_id' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'O nome do armazenamento é obrigatório',
            'name.min' => 'O nome do armazenamento deve ter no mínimo 3 caracteres',
            'name.max' => 'O nome do armazenamento deve ter no máximo 255 caracteres',

            'building_id.required'  => 'Uma obra é obrigatoria',

            'description.required'  => 'A descrição é obrigatória',
            'description.min' => 'A descrição deve ter no mínimo 3 caracteres',
        ];
    }
}
