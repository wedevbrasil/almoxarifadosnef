<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTool extends FormRequest
{
    /**
 * Determine if the user is authorized to make this request.
 *
 * @return bool
 */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod' => 'required|min:1|unique:tools|numeric',
            'name' => 'required|max:255|min:3',
            'unity' => 'required',
            'type' => 'required',
            'stock_min' => ''
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cod.required' => 'O código da ferramenta é obrigatório',
            'cod.min' => 'O código da ferramenta deve ter no mínimo 1 caracter',
            'cod.numeric' => 'O código da ferramenta deve ser numérico.',
            'cod.unique' => 'O código da ferramenta já esta sendo utilizado.',

            'name.required' => 'O nome da ferramenta é obrigatório',
            'name.min' => 'O nome da ferramenta deve ter no mínimo 3 caracteres',
            'name.max' => 'O nome da ferramenta deve ter no máximo 255 caracteres',

            'unity.required' => 'A unidade da ferramenta é obrigatório',

            'type.required'  => 'O tipo é obrigatorio',
        ];
    }
}
