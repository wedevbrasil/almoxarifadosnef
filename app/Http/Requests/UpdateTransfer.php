<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTransfer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'doc' => 'required|unique:withdrawal,' . $this->withdrawal,
            'tool' => 'required',
            'tool_giveback' => '',
            'warehouse' => 'required',
            'employee' => 'required',
            'quantity' => 'required',
            'quantity_givedback' => '',
            'giveback_reason' => ''
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'doc.required' => 'O número da nota fiscal é obrigatório',
            'doc.unique' => 'O número da nota fiscal deve ser único.',

            'provider.required' => 'O fornecedor é obrigatório',

            'employee.required' => 'O funcionário é obrigatório',

            'tool.required'  => 'A ferramenta é obrigatoria',

            'warehouse.required'  => 'O armazenamento é obrigatorio',

            'quantity.required'  => 'A quantidades é obrigatorio'
        ];
    }
}
