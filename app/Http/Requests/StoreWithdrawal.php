<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreWithdrawal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'doc' => 'required|unique:withdrawal',
            'doc' => '',
            'tool' => 'required',
            'warehouse' => 'required',
            'employee' => '',
            //'quantity' => 'required',
            'type' => 'required',
            'giveback_date' => '',
            'invoice' => '',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'doc.required' => 'O número da nota fiscal é obrigatório',
            'doc.unique' => 'O número da nota fiscal deve ser único.',

            'provider.required' => 'O fornecedor é obrigatório',

            'employee.required' => 'O funcionário é obrigatório',

            'tool.required'  => 'O produto é obrigatorio',

            'warehouse-source.required'  => 'O armazenamento de Origem é obrigatorio',
            'warehouse-destination.required'  => 'O armazenamento de Destino é obrigatorio',

            'quantity.required'  => 'A quantidades é obrigatorio',

            'type.required'  => 'O tipo de retirada é obrigatorio'
        ];
    }
}
