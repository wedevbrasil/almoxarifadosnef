<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmitImport extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'mimes:xlsx,xlsm,xltx,xltm,xls,xlt,ods,ots,slk,xml,htm,html,csv|max:50000|required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file.required' => 'O arquivo de importação é obrigatório',
            'file.mimes' => 'O arquivo de importação não é aceito pelo sistema',
        ];
    }
}
