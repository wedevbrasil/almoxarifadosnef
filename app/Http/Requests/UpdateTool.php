<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTool extends FormRequest
{
    /**
 * Determine if the user is authorized to make this request.
 *
 * @return bool
 */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|min:3',
            'stock_min' => 'integer',
            'type' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'O nome da ferramenta é obrigatório',
            'name.min' => 'O nome da ferramenta deve ter no mínimo 3 caracteres',
            'name.max' => 'O nome da ferramenta deve ter no máximo 255 caracteres',

            'minimum.integer' => 'O valor minimo deve ser inteiro',

            'type.required'  => 'O tipo é obrigatorio',
        ];
    }
}
