<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEntrance extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'doc' => 'required|unique:entrances',
            'provider' => 'required',
            'tool' => 'required',
            'warehouse' => 'required',
            'type' => 'required',
            //'quantity' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'doc.required' => 'O número da nota fiscal é obrigatório',
            'doc.unique' => 'O número da nota fiscal deve ser único.',

            'provider.required' => 'O fornecedor é obrigatório',

            'tool.required'  => 'A ferramenta é obrigatoria',

            'type.required'  => 'O tipo de entrada é obrigatorio',

            'warehouse.required'  => 'O armazenamento é obrigatorio',

            'quantity.required'  => 'A quantidades é obrigatorio'
        ];
    }
}
