<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|min:3',
            'office' => 'required|min:3',
            'sector' => 'required|min:3',
            'contact' => 'required|min:10',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'O nome do funcionário é obrigatório',
            'name.min' => 'O nome do funcionário deve ter no mínimo 3 caracteres',
            'name.max' => 'O nome do funcionário deve ter no máximo 255 caracteres',

            'office.required'  => 'A função  é obrigatorio',
            'office.min' => 'A função deve ter no mínimo 3 caracteres',

            'sector.required'  => 'O setor  é obrigatorio',
            'sector.min' => 'O setor deve ter no mínimo 3 caracteres',

            'contact.required'  => 'O contato é obrigatório',
            'contact.min' => 'O contato deve ter no mínimo 10 caracteres',
        ];
    }
}
