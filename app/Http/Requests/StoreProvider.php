<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProvider extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'doc' => 'required|max:255|min:14|unique:providers',
            'name' => 'required|max:255|min:3',
            'address' => 'required|min:3',
            'contact' => 'required|min:10',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'doc.required' => 'O CNPJ do fornecedor é obrigatório',
            'doc.min' => 'O CNPJ do fornecedor deve ter 14 caracteres',
            'doc.max' => 'O CNPJ do fornecedor deve ter 14 caracteres',
            'doc.unique' => 'O CNPJ deve ser único.',

            'name.required' => 'O nome do armazenamento é obrigatório',
            'name.min' => 'O nome do armazenamento deve ter no mínimo 3 caracteres',
            'name.max' => 'O nome do armazenamento deve ter no máximo 255 caracteres',

            'address.required'  => 'O endereço é obrigatorio',
            'address.min' => 'O endereço deve ter no mínimo 3 caracteres',

            'contact.required'  => 'O contato é obrigatória',
            'contact.min' => 'O contato deve ter no mínimo 10 caracteres',
        ];
    }
}
