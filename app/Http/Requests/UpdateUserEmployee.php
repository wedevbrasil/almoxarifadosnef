<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:255|min:3|unique:users,username'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => 'O usuaŕio do funcionário é obrigatório',
            'username.min' => 'O usuaŕio do funcionário deve ter no mínimo 3 caracteres',
            'username.max' => 'O usuaŕio do funcionário deve ter no máximo 255 caracteres',
            'username.unique' => 'O usuaŕio de login deve ser único',
        ];
    }
}
