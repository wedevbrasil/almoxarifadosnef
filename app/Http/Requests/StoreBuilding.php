<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBuilding extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|min:1|unique:buildings|numeric',
            'name' => 'required|max:255|min:3'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'number.required' => 'O identificador da obra é obrigatório',
            'number.min' => 'O identificador do obra deve ter no mínimo 1 caracter',
            'number.numeric' => 'O identificador deve ser numérico.',

            'name.required' => 'O nome do obra é obrigatório',
            'name.min' => 'O nome do obra deve ter no mínimo 3 caracteres',
            'name.max' => 'O nome do obra deve ter no máximo 255 caracteres',

            'address.required'  => 'O endereço é obrigatorio',
            'address.min' => 'O endereço deve ter no mínimo 3 caracteres',
        ];
    }
}
