<?php

namespace App\Http\Controllers\User;

use App\Models\Models\Building;
use App\Models\Entrance;
use App\Models\EntranceType;
use App\Models\Http\Requests\StoreEntrance;
use App\Models\Inventory;
use App\Models\Provider;
use App\Models\Tool;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EntranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entrances = Entrance::whereHas('warehouse', function ($query) {
            $query->where('building_id', '=', Auth::user()->employee['building']['id']);
        })->get();

        return view('user.entrance.index',compact('entrances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tools = Tool::all();
        $warehouses = Warehouse::all();
        $providers = Provider::all();
        $entranceTypes = EntranceType::all();

        return view('user.entrance.create', compact('tools', 'warehouses', 'providers', 'entranceTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEntrance $request)
    {
        $validated = $request->validated();

        $entrance = new Entrance($validated);

        $provider = Provider::find($request->get('provider'));
        $provider->entrances()->save($entrance);

        $tool = Tool::find($request->get('tool'));
        $tool->entrances()->save($entrance);

        $warehouse = Warehouse::find($request->get('warehouse'));
        $warehouse->entrances()->save($entrance);

        $entranceType = EntranceType::find($request->get('type'));
        $entranceType->entrances()->save($entrance);

        $aux = Inventory::where('warehouse_id', '=', $request->get('warehouse'))
            ->where('tool_id', '=', $request->get('tool'))
            ->first();
        if($aux != null){
            $aux->quantity = $aux->quantity + $request->get('quantity');
            $aux->save();
        } else {
            //$amount = 0;
            $inventory = new Inventory();
            $inventory->quantity = $request->get('quantity');

            $tool = Tool::find($request->get('tool'));
            $tool->entrances()->save($inventory);

            $warehouse = Warehouse::find($request->get('warehouse'));
            $warehouse->entrances()->save($inventory);
        }

        /*
        Inventory::updateOrCreate([
            'warehouse' => $request->get('warehouse'),
            'tool' => $request->get('tool')],
            ['quantity' => $amount + $request->get('quantity')
        ]);
        */

        return redirect('/user/entrada')->with('success', 'Nova entrada adicionada.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entrance = Entrance::withTrashed()->find($id);
        $entranceTypes = EntranceType::all();

        foreach ($entrance->tool_ids as $i => $item){
            $itens[$i] = Tool::find($item);
        }

        return view('user.entrance.show',compact('entrance', 'entranceTypes', 'itens'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entrance = Entrance::find($id);
        $entrance->delete();

        return redirect('/user/entrada')->with('success', 'Entrada deletada.');
    }
}
