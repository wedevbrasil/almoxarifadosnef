<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\StoreTool;
use App\Http\Requests\UpdateTool;
use App\Models\ItemType;
use App\Models\Tool;
use App\Models\Unity;
use App\Repositories\ToolRepository;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ToolController extends Controller
{
    protected $toolRepository;

    public function __construct()
    {
        $this->toolRepository = new ToolRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tools = $this->toolRepository->all();

        return view('user.tool.index', compact('tools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lastId = Tool::orderBy('created_at', 'desc')->first();
        $itenTypes = ItemType::all();
        $units = Unity::all();

        return view("user.tool.create", compact('lastId','units', 'itenTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTool $request)
    {
        $validated = $request->validated();

        $tool = new Tool($validated);

        $itemType = ItemType::find($request->input('type'));
        $itemType->tools()->save($tool);

        $unity = Unity::find($request->get('unity'));
        $unity->tools()->save($tool);

        return redirect('/user/ferramenta')->with('success', 'Novo item adicionado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tool = $this->toolRepository->find($id);

        return view('user.tool.show',compact('tool'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $$tool = $this->toolRepository->find($id);
        $units = Unity::all();
        $itemTypes = ItemType::all();

        return view('user.tool.edit',compact('tool', 'units', 'itemTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTool $request, $id)
    {
        $validated = $request->validated();

        $tool = $this->toolRepository->find($id);
        $tool->name = $request->get('name');

        $unity = Unity::find($request->get('unity'));
        $unity->tools()->save($tool);

        $itemType = ItemType::find($request->get('type'));
        $itemType->tools()->save($tool);

        //$tool->save();

        return redirect('/user/ferramenta')->with('success', 'Item editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tool = Tool::find($id);
        $tool->delete();

        return redirect('/user/ferramenta')->with('success', 'Item deletado.');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $tool = Tool::withTrashed()->find($id);
        $tool->restore();

        return redirect('/user/ferramenta')->with('success', 'Item restaurada.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $tools = Tool::onlyTrashed()->get();

        return view('user.tool.deleted',compact('tools'));
    }

    public function datatablesAjax()
    {
        return Laratables::recordsOf(Tool::class, function()
        {
            return $this->toolRepository->datatables();
        });
    }
}
