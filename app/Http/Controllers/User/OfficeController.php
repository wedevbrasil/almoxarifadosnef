<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\StoreOffice;
use App\Http\Requests\UpdateOffice;
use App\Models\Office;
use Illuminate\Http\Request;

class OfficeController extends Controller
{/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $offices = Office::all();

        return view('user.office.index',compact('offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.office.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOffice $request)
    {
        $validated = $request->validated();

        $office = new Office($validated);
        $office->save();

        return redirect('/user/funcao')->with('success', 'Nova função adicionada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $office = Office::withTrashed()->find($id);

        return view('user.office.show',compact('office'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $office = Office::withTrashed()->find($id);

        return view('user.office.edit',compact('office'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOffice $request, $id)
    {
        $validated = $request->validated();

        $office = Office::find($id);
        //$office->doc = $request->get('doc');
        $office->name = $request->get('name');
        $office->save();

        return redirect('/user/funcao')->with('success', 'Função editada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $office = Office::find($id);
        $office->delete();

        return redirect('/user/funcao')->with('success', 'Função desativada.');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $office = Office::withTrashed()->find($id);
        $office->restore();

        return redirect('/user/funcao')->with('success', 'Função restaurada.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $offices = Office::onlyTrashed()->get();

        return view('user.office.deleted',compact('offices'));
    }
}
