<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\StoreSector;
use App\Http\Requests\UpdateSector;
use App\Models\Sector;
use Illuminate\Http\Request;

class SectorController extends Controller
{/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $sectors = Sector::all();

        return view('user.sector.index',compact('sectors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.sector.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSector $request)
    {
        $validated = $request->validated();

        $sector = new Sector($validated);
        $sector->save();

        return redirect('/user/setor')->with('success', 'Novo setor adicionado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sector = Sector::withTrashed()->find($id);

        return view('user.sector.show',compact('sector'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sector = Sector::withTrashed()->find($id);

        return view('user.sector.edit',compact('sector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSector $request, $id)
    {
        $validated = $request->validated();

        $sector = Sector::find($id);
        //$sector->doc = $request->get('doc');
        $sector->name = $request->get('name');
        $sector->save();

        return redirect('/user/setor')->with('success', 'Setor editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sector = Sector::find($id);
        $sector->delete();

        return redirect('/user/setor')->with('success', 'Setor deletado.');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $sector = Sector::withTrashed()->find($id);
        $sector->restore();

        return redirect('/user/setor')->with('success', 'Setor restaurado.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $sectors = Sector::onlyTrashed()->get();

        return view('user.sector.deleted',compact('sectors'));
    }
}
