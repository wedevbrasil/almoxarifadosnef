<?php

namespace App\Http\Controllers\User;

use App\Models\Building;
use App\Models\Employee;
use App\Models\Http\Requests\StoreEmployee;
use App\Models\Http\Requests\UpdateEmployee;
use App\Models\Http\Requests\UpdateUserEmployee;
use App\Models\Office;
use App\Models\Sector;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::where('building_id', Auth::user()->employee['building']['id'])->get();

        return view('user.employee.index',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $obras = Building::all();
        $sectors = Sector::all();
        $offices = Office::all();

        return view('user.employee.create', compact('obras', 'sectors', 'offices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployee $request)
    {
        $validated = $request->validated();

        $employee = new Employee($validated);

        $building = Building::find($request->get('building'));
        $building->employees()->save($employee);

        $sector = Sector::find($request->get('sector'));
        $sector->employees()->save($employee);


        return redirect('/user/funcionario')->with('success', 'Novo funcionário adicionado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::withTrashed()->find($id);

        return view('user.employee.show',compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::withTrashed()->find($id);
        $obras = Building::all();
        $sectors = Sector::all();
        $offices = Office::all();

        return view('user.employee.edit',compact('employee', 'obras', 'sectors', 'offices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployee $request, $id)
    {
        $validated = $request->validated();

        $employee = Employee::find($id);
        $employee->name = $request->get('name');
        $employee->function = $request->get('function');
        $employee->sector = $request->get('sector');
        $employee->contact = $request->get('contact');


        $building = Building::find($request->get('building'));
        $building->employees()->save($employee);

        $sector = Sector::find($request->get('sector'));
        $sector->employees()->save($employee);

        $function = Office::find($request->get('office'));
        $function->employees()->save($employee);

        return redirect('/user/funcionario')->with('success', 'Funcionario editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();

        return redirect('/user/funcionario')->with('success', 'Funcionário deletado.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $employees = Employee::onlyTrashed()->get();

        return view('user.employee.deleted',compact('employees'));
    }

    public function userEdit($id)
    {
        $employee = Employee::withTrashed()->find($id);
        $obras = Building::all();

        return view('user.employee.user.edit',compact('employee', 'obras'));

    }

    public function userUpdate(Request $request, $id)
    {
        $employee = Employee::find($id);

        if($request->get('access-level') < 4){
            // Update Email
            if($employee->user['username'] != $request->get('username')) {
                if (
                $user = User::where('employee_id', '=', $employee->id)->first()
                ) {
                    $user->username = $request->get('username');
                    $user->password = Hash::make($request->get('password'));
                    $user->role = $request->get('access-level');
                    $user->save();

                } else {
                    $user = new User();
                    $user->username = $request->get('username');
                    $user->role = $request->get('access-level');

                    $employee->user()->save($user);
                }
            }

            // Update Password
            if($request->get('password') != null){
                $user = User::where('employee_id', '=', $employee->id)->first();
                $user->password = Hash::make($request->get('password'));
                $user->role = $request->get('access-level');
                $user->save();
            }
        }
        elseif($request->get('access-level') == 4){
            // Update Pin
            if($request->get('pin') != null){
                if (
                $user = User::where('employee_id', '=', $employee->id)->first()
                ) {
                    $user->pin = $request->get('pin');
                    $user->role = $request->get('access-level');
                    $user->save();

                } else {
                    $user = new User();
                    $user->pin = $request->get('pin');
                    $user->role = $request->get('access-level');

                    $employee->user()->save($user);

                }
            }
        }

        return redirect('/user/funcionario')->with('success', 'Usuário de acesso do funcionario editado.');
    }
}
