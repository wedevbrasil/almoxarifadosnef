<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\UpdateWithdrawal;
use App\Models\Building;
use App\Models\Employee;
use App\Models\EntranceType;
use App\Models\Withdrawal;
use App\Http\Requests\StoreWithdrawal;
use App\Models\Inventory;
use App\Models\Provider;
use App\Models\Tool;
use App\Models\Warehouse;
use App\Models\WithdrawalType;
use App\Repositories\EntranceRepository;
use App\Repositories\ProviderRepository;
use App\Repositories\ToolRepository;
use App\Repositories\WarehouseRepository;
use App\Repositories\WithdrawalRepository;
use App\Repositories\WithdrawalTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class WithdrawalController extends Controller
{
    protected $entranceRepository;
    protected $withdrawalRepository;
    protected $toolRepository;
    protected $warehouseRepository;
    protected $providerRepository;
    protected $withdrawalTypeRepository;

    public function __construct()
    {
        $this->entranceRepository = new EntranceRepository();
        $this->withdrawalRepository = new WithdrawalRepository();
        $this->toolRepository = new ToolRepository();
        $this->warehouseRepository = new WarehouseRepository();
        $this->providerRepository = new ProviderRepository();
        $this->withdrawalTypeRepository = new WithdrawalTypeRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('status') != null){
            $withdrawals = Withdrawal::whereHas('warehouse', function ($query) {
                $query->where('building_id', '=', Auth::user()->employee['building']['id']);
            })->where('status', (int)  $request->input('status'))
                ->get();
        } else {
            $withdrawals = Withdrawal::whereHas('warehouse', function ($query) {
                $query->where('building_id', '=', Auth::user()->employee['building']['id']);
            })->get();
        }


        return view('user.withdrawal.index',compact('withdrawals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $tools = $this->toolRepository->with('itemType');
        $warehouses = $this->warehouseRepository->with('building');
        $warehousesDestination = $this->warehouseRepository->all();
        $providers = $this->providerRepository->all();
        $withdrawalTypes = $this->withdrawalTypeRepository->all();


        $expiration = 60 * 60 *24;
        $key = 'employees';
        $employees = Cache::remember($key, $expiration, function () {
            return  Employee::with([
                'building',
                'user',
                'sector',
                'office',
                'withdrawals',
                'warehouse'
            ])->get();
        });

        return view('user.withdrawal.create', compact(
            'tools', 'warehouses', 'warehousesDestination', 'providers', 'withdrawalTypes', 'employees'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWithdrawal $request)
    {
        // Validaçao Dados
        $validated = $request->validated();

        $withdrawal = $this->withdrawalRepository->store($validated);

        if($request->get('employee_aprovation') == true){
            return redirect()->route('user.retirada.show', $withdrawal->id . '?employee_confirmation=true');
        }

        return redirect()->route('user.retirada.index')->with('success', 'Nova retirada adicionada.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $withdrawal = Withdrawal::withTrashed()->find($id);

        if(Withdrawal::find($id)) {
            foreach ($withdrawal->tool_ids as $i => $item) {
                $itens[$i] = Tool::find($item);
            }
        } else {
            return redirect('/user/retirada')->with('error', 'Retirada não encontrada.');
        }

        return view('user.withdrawal.show',compact('withdrawal', 'itens'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $withdrawal = Withdrawal::find($id);
        $withdrawal->delete();

        return redirect('/user/retirada')->with('success', 'Retirada deletada.');
    }

    /**
     * Cancel the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $withdrawal = Withdrawal::find($id);
        $withdrawal->status = 2;
        $withdrawal->save();

        return redirect('/user/retirada')->with('success', 'Retirada cancelada.');
    }

    /**
     * Approve the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id, Request $request)
    {
        if(Withdrawal::find($id)){
            // Retirada Encontrada
            $withdrawal = Withdrawal::find($id);
            // Pin Correto
            if($request->get('pin') == $withdrawal->employee['user']['pin']) {
                $withdrawal->status = 3;
                $withdrawal->save();

                return redirect('/user/retirada')->with('success', 'Retirada aprovada.');
            } else {
                return redirect()->back()->with('error', 'Pin não confere com o do funcionário.');
            }
        } else {
            return redirect('/user/retirada')->with('error', 'Retirada não encontrada.');
        }
    }

    /**
     * Give back the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function giveback(UpdateWithdrawal $request, $id)
    {
        $validated = $request->validated();
        //dd($request->validated());

        if(Withdrawal::find($id)){

            // Retirada Encontrada
            $withdrawal = Withdrawal::find($id);

            // Pin Correto
            if(Hash::check($request->get('password'),$withdrawal->warehouse['manager']['user']['password'])) {
                $withdrawal->status = 5;

                // Percorre todos os itens da retirada para atualizar inventario
                foreach($validated["tool_giveback"] as $i => $item) {

                        // Procura pelo invetario do almoxarifado e do item selecionado
                        $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                            ->where('tool_id', '=', $item['id'])
                            ->first();

                        // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
                        $inventory->quantity += $item['quantity'];
                        $inventory->save();
                }


                $withdrawal->update($validated);

                return redirect()->route('user.retirada.index')->with('success', 'Retirada devolvida.');

            } else {
                return redirect()->route('user.retirada.index', 'withdrawal_giveback=true')->with('error', 'Senha não confere com a do almoxarife.');
            }
        } else {
            return redirect()->route('user.retirada.index')->with('error', 'Retirada não encontrada.');
        }
    }
}
