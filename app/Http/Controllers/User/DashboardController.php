<?php

namespace App\Http\Controllers\User;

use App\Models\Building;
use App\Models\Tool;
use App\Models\User;
use App\Models\Warehouse;
use App\Repositories\BuildingRepository;
use Illuminate\Filesystem\Cache;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $buildingRepository;

    public function __construct()
    {
        $this->buildingRepository = new BuildingRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        $obraCount = Building::count();
        $almoxarifadoCount = Warehouse::count();
        $toolCount = Tool::count();
        $usersCount = User::count();
        */

        return view('user.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
