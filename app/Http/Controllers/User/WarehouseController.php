<?php

namespace App\Http\Controllers\User;

use App\Models\Building;
use App\Models\Employee;
use App\Models\Http\Requests\StoreWarehouse;
use App\Models\Http\Requests\UpdateWarehouse;
use App\Models\Inventory;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warehouses = Warehouse::where('building_id', 'like', Auth::user()->employee['building']['id'])->get();

        return view('user.warehouse.index',compact('warehouses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $obra = Building::find( Auth::user()->employee['building']['id']);

        $employees = Employee::whereHas('user.role', function ($query) {
            $query->where('role', 'like', (int) 3);
        })
            ->where('building_id', Auth::user()->employee['building']['id'])
            ->get();

        return view('user.warehouse.create',compact('obra', 'employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWarehouse $request)
    {
        $validated = $request->validated();

        $warehouse = new Warehouse([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
        ]);

        $building = Building::find(Auth::user()->employee['building']['id']);
        $building->warehouses()->save($warehouse);

        $employee = Employee::find($request->get('employee'));
        $employee->warehouse()->save($warehouse);

        return redirect('/user/almoxarifado')->with('success', 'Novo almoxarifado adicionado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $warehouse = Warehouse::withTrashed()->find($id);

        $inventory = Inventory::where("warehouse", '=', $id)->get();

        return view('user.warehouse.show',compact('warehouse', 'inventory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obras = Building::all();
        $warehouse = Warehouse::withTrashed()->find($id);

        $employees = Employee::whereHas('user.role', function ($query) {
            $query->where('role', 'like', 3);
        })
            ->where('building_id', 'like', $warehouse->building['id'])
            ->get();

        return view('user.warehouse.edit',compact('warehouse', 'obras', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWarehouse $request, $id)
    {
        $validated = $request->validated();

        $warehouse = Warehouse::find($id);
        $warehouse->name = $request->get('name');
        $warehouse->description = $request->get('description');


        $building = Building::find($request->get('building_id'));
        $building->warehouses()->save($warehouse);

        $employee = Employee::find($request->get('manager'));
        $employee->warehouse()->save($warehouse);

        //$warehouse->save();

        return redirect('/user/almoxarifado')->with('success', 'Armazenamento editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $warehouse = Warehouse::find($id);
        $warehouse->delete();

        return redirect('/user/almoxarifado')->with('success', 'Armazenamento deletado.');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $warehouses = Warehouse::onlyTrashed()->get();

        return view('user.warehouse.deleted',compact('warehouses'));
    }
}
