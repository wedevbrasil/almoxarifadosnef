<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateInventory;
use App\Models\Inventory;
use App\Models\ItemType;
use App\Models\Unity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inventory = Inventory::all();

        return view('admin.inventory.index', compact('inventory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inventory = Inventory::find($id);
        return view('admin.inventory.show', compact('inventory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inventory = Inventory::find($id);
        $units = Unity::all();
        $itemTypes = ItemType::all();

        return view('admin.inventory.edit', compact('inventory', 'units', 'itemTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInventory $request, $id)
    {
        $validated = $request->validated();

        $inventory = Inventory::find($id);
        $inventory->quantity = $request->get('quantity');
        $inventory->stock_min = $request->get('stock_min');
        $inventory->save();

        Cache::forget('warehouse_id_' . $inventory->warehouse->id);

        return redirect('/admin/almoxarifado/' . $inventory->warehouse->id)->with('success', 'Estoque editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
