<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreItemType;
use App\Http\Requests\UpdateItemType;
use App\Models\ItemType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ItemTypeController extends Controller
{/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $itemTypes = ItemType::all();

        return view('admin.itemtype.index',compact('itemTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.itemtype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreItemType $request)
    {
        $validated = $request->validated();

        $itemType = new ItemType($validated);
        $itemType->save();

        return redirect('/admin/tipoitem')->with('success', 'Nova tipo de item adicionado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemType  $itemType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expiration = 60 * 60 *24;
        $key = 'itemtype_id_' . $id;

        $itemType = Cache::remember($key, $expiration, function () use ($id) {
            return ItemType::with([
                'tools.entrances.provider',
                'tools.entrances.warehouse',

                'tools.withdrawals.provider',
                'tools.withdrawals.warehouse',

                'tools.inventories',
                'tools.itemType',
                'tools.unity'
            ])->withTrashed()->find($id);
        });

        return view('admin.itemtype.show',compact('itemType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemType  $itemType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expiration = 60 * 60 *24;
        $key = 'itemtype_id_' . $id;

        $itemType = Cache::remember($key, $expiration, function () use ($id) {
            return ItemType::with([
                'tools.entrances.provider',
                'tools.entrances.warehouse',

                'tools.withdrawals.provider',
                'tools.withdrawals.warehouse',

                'tools.inventories',
                'tools.itemType',
                'tools.unity'
            ])->withTrashed()->find($id);
        });

        return view('admin.itemtype.edit',compact('itemType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemType  $itemType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateItemType $request, $id)
    {
        $validated = $request->validated();

        $itemType = ItemType::find($id);
        //$itemType->doc = $request->get('doc');
        $itemType->name = $request->get('name');
        $itemType->save();

        return redirect('/admin/tipoitem')->with('success', 'Tipo de item editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemType  $itemType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemType = ItemType::find($id);
        $itemType->delete();

        return redirect('/admin/tipoitem')->with('success', 'Tipo de item desativado.');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $itemType = ItemType::withTrashed()->find($id);
        $itemType->restore();

        return redirect('/admin/tipoitem')->with('success', 'Tipo de item restaurado.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $itemTypes = ItemType::onlyTrashed()->get();

        return view('admin.itemtype.deleted',compact('itemTypes'));
    }
}
