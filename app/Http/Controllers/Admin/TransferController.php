<?php

namespace App\Http\Controllers\Admin;

use App\Models\Employee;
use App\Http\Requests\StoreTransfer;
use App\Http\Requests\UpdateTransfer;
use App\Repositories\InvoiceRepository;
use App\Repositories\TransferRepository;
use App\Repositories\TransferTypeRepository;
use App\Repositories\ProviderRepository;
use App\Repositories\ToolRepository;
use App\Repositories\WarehouseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TransferController extends Controller
{
    protected $transferRepository;
    protected $invoiceRepository;
    protected $toolRepository;
    protected $warehouseRepository;
    protected $providerRepository;
    protected $transferTypeRepository;

    public function __construct()
    {
        $this->transferRepository = new TransferRepository();
        $this->invoiceRepository = new InvoiceRepository();
        $this->toolRepository = new ToolRepository();
        $this->warehouseRepository = new WarehouseRepository();
        $this->providerRepository = new ProviderRepository();
        $this->transferTypeRepository = new TransferTypeRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $transfers = $this->transferRepository->all();

        return view('admin.transfer.index',compact('transfers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $tools = $this->toolRepository->with(['itemType','unity']);
        $warehouses = $this->warehouseRepository->with('building');
        $warehousesDestination = $this->warehouseRepository->all();
        $providers = $this->providerRepository->all();
        $transferTypes = $this->transferTypeRepository->all();

        return view('admin.transfer.create', compact(
            'tools', 'warehouses', 'warehousesDestination', 'providers', 'transferTypes', 'employees'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransfer $request)
    {
        // Validaçao Dados
        $validated = $request->validated();

        $transfer = $this->transferRepository->store($validated);
        $invoice = $this->invoiceRepository->store($validated, $transfer);

        return redirect()->route('admin.transferencia.index')->with('success', 'Nova transferência adicionada.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $transfer = $this->transferRepository->find($id);
        $itens = $this->toolRepository->showItensTransfer($transfer->tool_ids);

        if($request->query('notification')) {
            $notification = auth()->user()->notifications()->where('id', $request->query('notification'));
            if ($notification) {
                $notification->update(['read_at' => now()]);
            }
        }

        return view('admin.transfer.show',compact('transfer', 'itens'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transfer = $this->transferRepository->find($id);

        $itens = $this->toolRepository->showItensTransfer($transfer->tool_ids);

        $tools = $this->toolRepository->with('itemType');
        $warehouses = $this->warehouseRepository->with('building');
        $providers = $this->providerRepository->all();
        $transferTypes = $this->transferTypeRepository->all();

        $expiration = 60 * 60 *24;
        $key = 'employees';
        $employees = Cache::remember($key, $expiration, function () {
            return  Employee::with([
                'building',
                'user',
                'sector',
                'office',
                'transfers',
                'warehouse'
            ])->get();
        });

        return view('admin.transfer.edit', compact(
            'tools',
            'warehouses',
            'providers',
            'transferTypes',
            'transfer',
            'itens',
            'employees'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTransfer $request, $id)
    {
        // Validaçao Dados
        $validated = $request->validated();

        $transfer = $this->transferRepository->update($id, $validated);

        return redirect()->route('admin.transferencia.index')->with('success', 'Retirada editada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transfer = $this->transferRepository->delete($id);

        return redirect()->route('admin.transferencia.index')->with('success', 'Retirada deletada.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {

        $transfers = $this->transferRepository->allTrashed();

        return view('admin.transfer.deleted',compact('transfers'));
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $restore = $this->transferRepository->restore($id);

        return redirect()->route('admin.transferencia.index')->with('success', 'Retirada restaurada.');
    }

    /**
     * Cancel the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $transfer = $this->transferRepository->cancel($id);

        return redirect()->route('admin.transferencia.index')->with('success', 'Transferência cancelada.');
    }
}
