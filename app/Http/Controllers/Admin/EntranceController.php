<?php

namespace App\Http\Controllers\Admin;

use App\Models\Building;
use App\Models\Entrance;
use App\Models\EntranceType;
use App\Http\Requests\StoreEntrance;
use App\Http\Requests\UpdateEntrance;
use App\Models\Inventory;
use App\Models\Provider;
use App\Models\Tool;
use App\Models\Warehouse;
use App\Repositories\EntranceRepository;
use App\Repositories\EntranceTypeRepository;
use App\Repositories\ProviderRepository;
use App\Repositories\ToolRepository;
use App\Repositories\WarehouseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use MongoDB\Operation\Update;

class EntranceController extends Controller
{
    protected $entranceRepository;
    protected $toolRepository;
    protected $warehouseRepository;
    protected $providerRepository;
    protected $entranceTypeRepository;

    public function __construct()
    {
        $this->entranceRepository = new EntranceRepository();
        $this->toolRepository = new ToolRepository();
        $this->warehouseRepository = new WarehouseRepository();
        $this->providerRepository = new ProviderRepository();
        $this->entranceTypeRepository = new EntranceTypeRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $entrances = $this->entranceRepository->all();

        return view('admin.entrance.index',compact('entrances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tools = $this->toolRepository->with('itemType');
        $warehouses = $this->warehouseRepository->with('building');
        $providers = $this->providerRepository->all();
        $entranceTypes = $this->entranceTypeRepository->all();

        return view('admin.entrance.create', compact('tools', 'warehouses', 'providers', 'entranceTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEntrance $request)
    {
        // Validaçao Dados
        $validated = $request->validated();

        $entrance = $this->entranceRepository->store($validated);


        return redirect()->route('admin.entrada.index')->with('success', 'Nova entrada adicionada.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entrance = $this->entranceRepository->find($id);
/*
        foreach ($entrance->tool_ids as $i => $item){
            $itens[$i] = $this->toolRepository->showItensEntrance($item);
        }
*/
        $itens = $this->toolRepository->showItensEntrance($entrance->tool_ids);

        return view('admin.entrance.show',compact('entrance', 'itens'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $entrance = $this->entranceRepository->find($id);

        $itens = $this->toolRepository->showItensEntrance($entrance->tool_ids);

        $tools = $this->toolRepository->with('itemType');
        $warehouses = $this->warehouseRepository->with('building');
        $providers = $this->providerRepository->all();
        $entranceTypes = $this->entranceTypeRepository->all();

        return view('admin.entrance.edit', compact('tools', 'warehouses', 'providers', 'entranceTypes', 'entrance', 'itens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEntrance $request, $id)
    {
        $validated = $request->validated();

        $entrance = $this->entranceRepository->update($id, $validated);

        return redirect()->route('admin.entrada.index')->with('success', 'Entrada editada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entrance = $this->entranceRepository->delete($id);

        return redirect()->route('admin.entrada.index')->with('success', 'Entrada deletada.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {

        $entrances = $this->entranceRepository->allTrashed();

        return view('admin.entrance.deleted',compact('entrances'));
    }
}
