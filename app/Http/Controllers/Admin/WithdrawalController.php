<?php

namespace App\Http\Controllers\Admin;

use App\Models\Building;
use App\Models\Employee;
use App\Models\Withdrawal;
use App\Models\WithdrawalType;
use App\Http\Requests\StoreWithdrawal;
use App\Http\Requests\UpdateWithdrawal;
use App\Models\Inventory;
use App\Models\Provider;
use App\Models\Tool;
use App\Models\Warehouse;
use App\Repositories\WithdrawalRepository;
use App\Repositories\WithdrawalTypeRepository;
use App\Repositories\ProviderRepository;
use App\Repositories\ToolRepository;
use App\Repositories\WarehouseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use MongoDB\Operation\Update;

class WithdrawalController extends Controller
{
    protected $withdrawalRepository;
    protected $toolRepository;
    protected $warehouseRepository;
    protected $providerRepository;
    protected $withdrawalTypeRepository;

    public function __construct()
    {
        $this->withdrawalRepository = new WithdrawalRepository();
        $this->toolRepository = new ToolRepository();
        $this->warehouseRepository = new WarehouseRepository();
        $this->providerRepository = new ProviderRepository();
        $this->withdrawalTypeRepository = new WithdrawalTypeRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $withdrawals = $this->withdrawalRepository->all();

        return view('admin.withdrawal.index',compact('withdrawals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $tools = $this->toolRepository->with(['itemType','unity']);
        $warehouses = $this->warehouseRepository->with('building');
        $warehousesDestination = $this->warehouseRepository->all();
        $providers = $this->providerRepository->all();
        $withdrawalTypes = $this->withdrawalTypeRepository->all();


        $expiration = 60 * 60 *24;
        $key = 'employees';
        $employees = Cache::remember($key, $expiration, function () {
            return  Employee::with([
                'building',
                'user',
                'sector',
                'office',
                'withdrawals',
                'warehouse'
            ])->get();
        });

        return view('admin.withdrawal.create', compact(
            'tools', 'warehouses', 'warehousesDestination', 'providers', 'withdrawalTypes', 'employees'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWithdrawal $request)
    {
        // Validaçao Dados
        $validated = $request->validated();

        $withdrawal = $this->withdrawalRepository->store($validated);

        return redirect()->route('admin.retirada.index')->with('success', 'Nova retirada adicionada.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $withdrawal = $this->withdrawalRepository->find($id);
        $itens = $this->toolRepository->showItensWithdrawal($withdrawal->tool_ids);

        return view('admin.withdrawal.show',compact('withdrawal', 'itens'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $withdrawal = $this->withdrawalRepository->find($id);

        $itens = $this->toolRepository->showItensWithdrawal($withdrawal->tool_ids);

        $tools = $this->toolRepository->with('itemType');
        $warehouses = $this->warehouseRepository->with('building');
        $providers = $this->providerRepository->all();
        $withdrawalTypes = $this->withdrawalTypeRepository->all();

        $expiration = 60 * 60 *24;
        $key = 'employees';
        $employees = Cache::remember($key, $expiration, function () {
            return  Employee::with([
                'building',
                'user',
                'sector',
                'office',
                'withdrawals',
                'warehouse'
            ])->get();
        });

        return view('admin.withdrawal.edit', compact(
            'tools',
            'warehouses',
            'providers',
            'withdrawalTypes',
            'withdrawal',
            'itens',
            'employees'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWithdrawal $request, $id)
    {
        // Validaçao Dados
        $validated = $request->validated();

        $withdrawal = $this->withdrawalRepository->update($id, $validated);

        return redirect()->route('admin.retirada.index')->with('success', 'Retirada editada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $withdrawal = $this->withdrawalRepository->delete($id);

        return redirect()->route('admin.retirada.index')->with('success', 'Retirada deletada.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {

        $withdrawals = $this->withdrawalRepository->allTrashed();

        return view('admin.withdrawal.deleted',compact('withdrawals'));
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $restore = $this->withdrawalRepository->restore($id);

        return redirect()->route('admin.retirada.index')->with('success', 'Retirada restaurada.');
    }
}
