<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RequestProvider;
use App\Http\Requests\StoreProvider;
use App\Http\Requests\UpdateProvider;
use App\Models\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expiration = 60 * 60 *24;
        $key = 'providers';

        $providers = Cache::remember($key, $expiration, function () {
            return  Provider::all();
        });

        return view('user.provider.index',compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.provider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProvider $request)
    {
        $validated = $request->validated();

        $provider = new Provider($validated);
        $provider->save();

        return redirect('/admin/fornecedor')->with('success', 'Novo fornecedor adicionado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = Provider::withTrashed()->find($id);

        return view('admin.provider.show',compact('provider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provider = Provider::withTrashed()->find($id);

        return view('admin.provider.edit',compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProvider $request, $id)
    {
        $validated = $request->validated();

        $provider = Provider::find($id);
        //$provider->doc = $request->get('doc');
        $provider->name = $request->get('name');
        $provider->address = $request->get('address');
        $provider->contact = $request->get('contact');
        $provider->save();

        return redirect('/admin/fornecedor')->with('success', 'Fornecedor editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provider = Provider::find($id);
        $provider->delete();

        return redirect('/admin/fornecedor')->with('success', 'Fornecedor desativado.');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $providers = Provider::onlyTrashed()->get();

        return view('admin.provider.deleted',compact('providers'));
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $provider = Provider::onlyTrashed()->find($id);
        $provider->restore();

        return redirect('/admin/fornecedor')->with('success', 'Fornecedor restaurado.');
    }
}
