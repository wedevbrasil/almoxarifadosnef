<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreTool;
use App\Http\Requests\UpdateTool;
use App\Models\ItemType;
use App\Models\Tool;
use App\Models\Unity;
use App\Repositories\ToolRepository;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ToolController extends Controller
{
    protected $toolRepository;

    public function __construct()
    {
        $this->toolRepository = new ToolRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tools = $this->toolRepository->all();

        /*
        foreach($tools as $tool){
            $tool->entrance_ids = [];
            $tool->withdrawal_ids = [];
            $tool->update();
        }
        */

        return view('admin.tool.index', compact('tools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lastId = Tool::orderBy('created_at', 'desc')->first();
        $itenTypes = ItemType::all();
        $units = Unity::all();

        return view("admin.tool.create", compact('lastId','units', 'itenTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTool $request)
    {
        $validated = $request->validated();

        $tool = $this->toolRepository->store($validated);

        return redirect('/admin/ferramenta')->with('success', 'Novo item adicionado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tool = $this->toolRepository->find($id);

        return view('admin.tool.show',compact('tool'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tool = Tool::withTrashed()->find($id);
        $units = Unity::all();
        $itemTypes = ItemType::all();

        return view('admin.tool.edit',compact('tool', 'units', 'itemTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTool $request, $id)
    {
        $validated = $request->validated();

        $tool = Tool::find($id);
        //$tool->minimum = $validated['minimun'];

        $unity = Unity::find($request->get('unity'));
        $unity->tools()->save($tool);

        $itemType = ItemType::find($request->get('type'));
        $itemType->tools()->save($tool);

        //$tool->update($validated);
        //dd($tool);

        return redirect('/admin/ferramenta')->with('success', 'Item editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tool = Tool::find($id);
        $tool->delete();

        return redirect('/admin/ferramenta')->with('success', 'Item deletado.');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $tool = Tool::withTrashed()->find($id);
        $tool->restore();

        return redirect('/admin/ferramenta')->with('success', 'Item restaurada.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $tools = Tool::onlyTrashed()->get();

        return view('admin.tool.deleted',compact('tools'));
    }

    public function datatablesAjax()
    {
        return Laratables::recordsOf(Tool::class, function()
        {
            return $this->toolRepository->datatables();
        });
    }
}
