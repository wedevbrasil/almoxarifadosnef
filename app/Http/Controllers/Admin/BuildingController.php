<?php

namespace App\Http\Controllers\Admin;

use App\Models\Building;
use App\Models\Employee;
use App\Http\Requests\StoreBuilding;
use App\Http\Requests\UpdateBuilding;
use App\Repositories\BuildingRepository;
use Illuminate\Http\Request;

class BuildingController extends Controller
{
    protected $buildingRepository;

    public function __construct()
    {
        $this->buildingRepository = new BuildingRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $obras = $this->buildingRepository->all();

        return view('admin.building.index',compact('obras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::whereHas('user.role', function ($query) {
            $query->where('role', 'like', 2);
        })->get();

        return view('admin.building.create', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBuilding $request)
    {
        $validated = $request->validated();

        $building = new Building();
        $building->name = $request->get('name');
        $building->address = $request->get('address');
        $building->save();

        return redirect('/admin/obra')->with('success', 'Nova obra adicionada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $building = $this->buildingRepository->find($id);

        return view('admin.building.show',compact('building'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $building = Building::withTrashed()->find($id);

        $employees = Employee::whereHas('user.role', function ($query) {
            $query->where('role', 'like', 2);
        })
            ->where('building_id', 'like', $id)
            ->get();

        return view('admin.building.edit',compact('building', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBuilding $request, $id)
    {
        $validated = $request->validated();

        $building = Building::find($id);
        $building->name = $request->get('name');
        $building->address = $request->get('address');

        if($request->get('manager') != null) {
            $employee = Employee::find($request->get('manager'));
            $employee->buildingManaged()->save($building);
        }

        $building->save();

        return redirect('/admin/obra')->with('success', 'Obra editada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Building  $building
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $building = Building::find($id);
        $building->delete();

        return redirect('/admin/obra')->with('success', 'Obra deletada.');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {

        $obras = Building::onlyTrashed()->get();

        return view('admin.building.deleted',compact('obras'));
    }
}
