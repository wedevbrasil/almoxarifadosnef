<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreUnity;
use App\Http\Requests\UpdateUnity;
use App\Models\Unity;
use Illuminate\Http\Request;

class UnityController extends Controller
{/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $units = Unity::all();

        return view('admin.unity.index',compact('units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.unity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUnity $request)
    {
        $validated = $request->validated();

        $unity = new Unity($validated);
        $unity->save();

        return redirect('/admin/unidade')->with('success', 'Nova unidade adicionada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unity  $unity
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unity = Unity::withTrashed()->find($id);

        return view('admin.unity.show',compact('unity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unity  $unity
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unity = Unity::withTrashed()->find($id);

        return view('admin.unity.edit',compact('unity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unity  $unity
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUnity $request, $id)
    {
        $validated = $request->validated();

        $unity = Unity::find($id);
        //$unity->doc = $request->get('doc');
        $unity->name = $request->get('name');
        $unity->save();

        return redirect('/admin/unidade')->with('success', 'Unidade editada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unity  $unity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unity = Unity::find($id);
        $unity->delete();

        return redirect('/admin/unidade')->with('success', 'Unidade desativada.');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $unity = Unity::withTrashed()->find($id);
        $unity->restore();

        return redirect('/admin/unidade')->with('success', 'Unidade restaurada.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $units = Unity::onlyTrashed()->get();

        return view('admin.unity.deleted',compact('units'));
    }
}
