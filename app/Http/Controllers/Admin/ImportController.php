<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SubmitImport;
use App\Http\Controllers\Controller;
use App\Imports\ToolsImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function import(SubmitImport $request, $type)
    {
        // Validaçao Dados
        $validated = $request->validated();

        //$file = $this::fileUpload($request);

        $collection = Excel::toArray(new ToolsImport, request()->file('file'));

        return $collection;

    }

    public static function fileUpload($request)
    {

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $destinationPath = storage_path('app/tmp/');
            $file->move($destinationPath, $name);

            return $destinationPath . $name;
        }
    }
}
