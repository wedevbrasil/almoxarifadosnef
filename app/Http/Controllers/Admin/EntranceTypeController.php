<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreEntranceType;
use App\Http\Requests\UpdateEntranceType;
use App\Models\EntranceType;
use Illuminate\Http\Request;

class EntranceTypeController extends Controller
{/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $entranceTypes = EntranceType::all();

        return view('admin.entrancetype.index',compact('entranceTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.entrancetype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEntranceType $request)
    {
        $validated = $request->validated();

        $entranceType = new EntranceType($validated);
        $entranceType->save();

        return redirect('/admin/tipoentrada')->with('success', 'Nova tipo de entrance adicionado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EntranceType  $entranceType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entranceType = EntranceType::withTrashed()->find($id);

        return view('admin.entrancetype.show',compact('entranceType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EntranceType  $entranceType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entranceType = EntranceType::withTrashed()->find($id);

        return view('admin.entrancetype.edit',compact('entranceType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EntranceType  $entranceType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEntranceType $request, $id)
    {
        $validated = $request->validated();

        $entranceType = EntranceType::find($id);
        //$entranceType->doc = $request->get('doc');
        $entranceType->name = $request->get('name');
        $entranceType->save();

        return redirect('/admin/tipoentrada')->with('success', 'Tipo de entrance editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EntranceType  $entranceType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entranceType = EntranceType::find($id);
        $entranceType->delete();

        return redirect('/admin/tipoentrada')->with('success', 'Tipo de entrance desativado.');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $entranceType = EntranceType::withTrashed()->find($id);
        $entranceType->restore();

        return redirect('/admin/tipoentrada')->with('success', 'Tipo de entrance restaurado.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $entranceTypes = EntranceType::onlyTrashed()->get();

        return view('admin.entrancetype.deleted',compact('entranceTypes'));
    }
}
