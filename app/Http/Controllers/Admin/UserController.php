<?php

namespace App\Http\Controllers\Admin;

use App\Models\Building;
use App\Models\Employee;
use App\Models\Office;
use App\Models\Role;
use App\Models\Sector;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $expiration;
    protected $key;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'user';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $key = $this->key . '_all';
        $users = Cache::remember($key, $this->expiration, static function () {
            return User::with([
                    'employee',
                    'provider',
                    'role'
                ])->get();
        });

        return view('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::withTrashed()->find($id);
        $employee = Employee::withTrashed()->find($user->employee['id']);
        $obras = Building::all();
        $sectors = Sector::all();
        $offices = Office::all();


        return view('admin.user.edit',compact('employee', 'obras', 'sectors', 'offices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);

        if($request->get('access-level') < 4 or $request->get('access-level') == 5 ){
            // Update Email
            if($user->username != $request->get('username')) {
                $user->username = $request->get('username');
                $user->password = Hash::make($request->get('password'));
                $user->role = $request->get('access-level');

                $role = Role::where('role', $request->get('access-level'))->first();
                $role->user()->save($user);
            }

            // Update Password
            if($request->get('password') != null){
                $user->password = Hash::make($request->get('password'));
                $user->role = $request->get('access-level');

                $role = Role::where('role', $request->get('access-level'))->first();
                $role->user()->save($user);
            }
        }
        elseif($request->get('access-level') == 4){
            // Update Pin
            if($request->get('pin') != null){
                $user->pin = $request->get('pin');
                $user->role = $request->get('access-level');

                $role = Role::where('role', $request->get('access-level'))->first();
                $role->user()->save($user);
            }
        }

        // Update Level
        if($user->role != $request->get('access-level')) {
            $user->role = $request->get('access-level');

            $role = Role::where('role', $request->get('access-level'))->first();
            $role->user()->save($user);
        }

        return redirect('/admin/usuario')->with('success', 'Usuário de acesso editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
