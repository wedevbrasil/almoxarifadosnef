<?php

namespace App\Http\Controllers\Admin;

use App\Models\Building;
use App\Models\Employee;
use App\Http\Requests\StoreWarehouse;
use App\Http\Requests\UpdateWarehouse;
use App\Models\Inventory;
use App\Models\Tool;
use App\Models\Warehouse;
use App\Repositories\WarehouseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WarehouseController extends Controller
{
    protected $warehouseRepository;

    public function __construct()
    {
        $this->warehouseRepository = new WarehouseRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Obras Agrupadas
        //$warehouses = Warehouse::groupBy('building_id')->orderBy('created_at', 'asc')->get();
        $warehouses = Warehouse::all();

        /*
        // Cria Inventario
        $tools = Tool::all();
        foreach($warehouses as $warehouse) {
            foreach($tools as $i => $tool) {
                $newInventory = new Inventory();
                $newInventory->quantity = 0;
                $newInventory->tool_id = $tool->id;
                $newInventory->warehouse_id = $warehouse->id;
                $newInventory->save();
            }
        }
        */

        return view('admin.warehouse.index',compact('warehouses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $obras = Building::all();

        $employees = Employee::whereHas('user.role', function ($query) {
            $query->where('role', 'like', 3);
        });

        $employees->get();

        return view('admin.warehouse.create',compact('obras', 'employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWarehouse $request)
    {
        $validated = $request->validated();

        $warehouse = new Warehouse([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
        ]);

        $building = Building::find($request->get('building_id'));
        $building->warehouses()->save($warehouse);

        if($request->get('employee') != null) {
            $employee = Employee::find($request->get('employee'));
            $employee->warehouse()->save($warehouse);
        }

        // Cria Inventario
        $tools = Tool::with('unity')->get();
        foreach($tools as $i => $tool) {
            $newInventory = new Inventory();
            $newInventory->quantity = 0;
            $newInventory->tool_id = $tool->id;
            $newInventory->warehouse_id = $warehouse->id;
            $newInventory->save();
        }

        return redirect('/admin/almoxarifado')->with('success', 'Novo almoxarifado adicionado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $warehouse = $this->warehouseRepository->find($id);

        return view('admin.warehouse.show',compact('warehouse'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obras = Building::all();
        $warehouse = Warehouse::withTrashed()->find($id);

        $employees = Employee::whereHas('user.role', function ($query) {
            $query->where('role', 'like', 3);
        })
            ->where('building_id', 'like', $warehouse->building['id'])
            ->get();

        return view('admin.warehouse.edit',compact('warehouse', 'obras', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWarehouse $request, $id)
    {
        $validated = $request->validated();

        $warehouse = Warehouse::find($id);
        $warehouse->name = $request->get('name');
        $warehouse->description = $request->get('description');


        $building = Building::find($request->get('building_id'));
        $building->warehouses()->save($warehouse);

        if($request->get('manager') != null) {
            $employee = Employee::find($request->get('manager'));
            $employee->warehouse()->save($warehouse);
        }

        //$warehouse->save();

        return redirect('/admin/almoxarifado')->with('success', 'Armazenamento editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $warehouse = Warehouse::find($id);
        $warehouse->delete();

        return redirect('/admin/almoxarifado')->with('success', 'Armazenamento deletado.');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleted()
    {
        $warehouses = Warehouse::onlyTrashed()->get();

        return view('admin.warehouse.deleted',compact('warehouses'));
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $warehouse = Warehouse::withTrashed()->find($id);
        $warehouse->restore();

        return redirect()->route('admin.almoxarifado.index')->with('success', 'Almoxarifado restaurado.');
    }

}
