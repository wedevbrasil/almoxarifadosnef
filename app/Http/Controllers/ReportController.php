<?php

namespace App\Http\Controllers;

use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GrahamCampbell\GitLab\Facades\GitLab;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function report(Request $request){

        $type = static function ($type){

            if($type === 'report-bug'){
                return 'report-bug';
            }
            elseif ($type === 'report-problem'){
                return 'report-problem';
            }
            elseif ($type === 'suggestion'){
                return 'suggestion';
            } else {
                return 'report';
            }

        };

        $title = static function ($type){

            if($type === 'report-bug'){
                return 'Bug Reportado';
            }
            elseif ($type === 'report-problem'){
                return 'Problema Reportado';
            }
            elseif ($type === 'suggestion'){
                return 'Sugestão';
            } else {
                return 'report';
            }

        };

        $screenshot = static function ($screenshot){

            if(isset($screenshot)) {
                return 'screenshot: <img src="' . $screenshot . '" />';
            } else {
                return null;
            }
        };
        GitLab::issues()->create('10382770', [
            'title' => $title($request->get('type')),
            'labels' => $type($request->get('type')),
            'description' =>
                'comment: '. $request->get('comment').
                ' <br />'.
                'type: '. $request->get('type').
                ' <br />'.
                $screenshot($request->get('screenshot'))
            ,
            'created_at' => now()
        ]);

        return response()->json(['result'=>'success']);

    }
}
