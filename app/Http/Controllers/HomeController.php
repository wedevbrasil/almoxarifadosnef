<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth()->user()->role['role'] == 1){
            return redirect()->route('admin.dashboard.index');
        }
        elseif(Auth()->user()->role['role'] == 2){
            return redirect()->route('user.dashboard.index');
        }
        elseif(Auth()->user()->role['role'] == 3){
            return redirect()->route('user.dashboard.index');
        }
    }
}
