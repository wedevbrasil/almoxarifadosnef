<?php

namespace App\Providers;

use App\Models\Building;
use App\Repositories\BuildingRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use OwenIt\Auditing\Models\Audit;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('app.debug')) DB::connection('mongodb')->enableQueryLog();

        View::composer('*', function($view){

            $buildingRepository = new BuildingRepository();
            $buildings = $buildingRepository->all();

            $view->with('buildings', $buildings);
        });

        /* Laravle Auditable Package
        Audit::creating(function (Audit $model) {
            if (empty($model->old_values) && empty($model->new_values)) {
                return false;
            }
        });
        */
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $bugsnag = \Bugsnag\Client::make(config('bugsnag.api_key'));
        \Bugsnag\Handler::register($bugsnag);
    }
}
