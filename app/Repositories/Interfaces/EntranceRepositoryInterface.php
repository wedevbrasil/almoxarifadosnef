<?php

namespace App\Repositories\Interfaces;

interface EntranceRepositoryInterface
{
    /**
     * Get's a post by it's ID
     *
     * @param int
     */
    public function get($entrance_id);

    /**
     * Get's all entrances.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes a post.
     *
     * @param int
     */
    public function delete($entrance_id);

    /**
     * Updates a post.
     *
     * @param int
     * @param array
     */
    public function update($entrance_id, array $entrance_data);
}
