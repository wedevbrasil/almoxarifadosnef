<?php

namespace App\Repositories;

use App\Http\Requests\StoreTransfer;
use App\Models\Employee;
use App\Models\Inventory;
use App\Models\Invoice;
use App\Models\Provider;
use App\Models\Tool;
use App\Models\Transfer;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\TransferType;
use App\Notifications\NewInvoiceNotification;
use App\Notifications\NewTransferNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;

class InvoiceRepository
{
    protected $expiration;
    protected $key;

    protected $entranceRepository;
    protected $withdrawalRepository;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'invoice';
        $this->entranceRepository = new EntranceRepository();
        $this->withdrawalRepository = new WithdrawalRepository();
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $transfers = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  Transfer::with([
                'warehouse',
                'warehouse.building',
                'provider',
                'employee.user'
            ])->orderBy($column, $direction)->get();
        });

        return $transfers;

    }

    /**
     * Get's a transfer by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($transfer_id)
    {
        $key = $this->key . '_id_' . $transfer_id;

        $transfer = Cache::remember($key, $this->expiration, function () use ($transfer_id){
            return  Transfer::with([
                'fromWarehouse.building',
                'toWarehouse.building',
                'transferType'
            ])->withTrashed()->find($transfer_id);
        });

        return $transfer;
    }

    /**
     * Get's a transfer by it's ID
     *
     * @param int
     * @return collection
     */
    public function show($transfer_id)
    {
        $key = $this->key . $transfer_id;

        $transfer = Cache::remember($key, $this->expiration, function () use ($transfer_id){
            return  Transfer::with([
                'warehouse',
                'provider',
                'transferType'
            ])->withTrashed()->find($transfer_id);
        });

        return $transfer;
    }

    /**
     * Get's all transfers.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';
        $transfers = Cache::remember($key, $this->expiration, static function (){
            return  Transfer::with([
                'fromWarehouse.building',
                'toWarehouse.building',
                'transferType'
            ])->orderBy('created_at', 'desc')->get();
        });
        return $transfers;
    }

    /**
     * Deletes a transfer.
     *
     * @param int
     */
    public function delete($transfer_id)
    {

        Transfer::destroy($transfer_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $transfer_id);
    }

    /**
     * Updates a transfer.
     *
     * @param int
     * @param array
     */
    public function update($transfer_id, array $validated)
    {
        $transfer = Transfer::find($transfer_id);

        $warehouse = Warehouse::find($validated["warehouse"]);

        $provider = Provider::find($validated["provider"]);

        // Percorre todos os itens da transferencia para atualizar inventario
        foreach($transfer->tool_ids as $i => $actItem) {
            foreach($validated["quantity"] as $j => $quantity) {

                // Procura pelo invetario do almoxarifado e do item selecionado
                $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                    ->where('tool_id', '=', $actItem)
                    ->first();

                // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
                $inventory->quantity = $inventory->quantity - $quantity;
                $inventory->save();
            }
            // Salva a entrada nos itens
            $tool = Tool::find($actItem);
            $tool->transfers()->save($transfer);

        }

        // Zera os Arrays de itens e quantidades //
        $transfer->tool_ids = [];
        $transfer->quantity = [];

        // Percorre todos os itens da transferencia para atualizar inventario
        foreach($validated["tool"] as $i => $item) {
            foreach($validated["quantity"] as $j => $quantity) {

                $arrQnt = $validated["quantity"];
                $arrItem = $validated["tool"];

                // Procura pelo invetario do almoxarifado e do item selecionado
                $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                    ->where('tool_id', '=', $item)
                    ->first();

                // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
                if ($inventory != null) {
                    $inventory->quantity += $quantity;
                    $inventory->save();
                } // Senao encontrar cria uma transferencia nova para o item no inventario do almoxarifado
                else {
                    //$amount = 0;
                    $newInventory = new Inventory();
                    $newInventory->quantity = $quantity;
                    $newInventory->tool_id = $item;

                    $warehouse->transfers()->save($newInventory);
                }

                // Salva a transferencia nos itens
                $tool = Tool::find($item);
                $tool->transfers()->save($transfer);
            }
        }

        // Atualiza os itens e quantidades na transferencia
        $transfer->quantity = $validated["quantity"];
        $transfer->tool_ids = $validated["tool"];

        $provider->transfers()->save($transfer);
        $warehouse->transfers()->save($transfer);

        //$transferType = TransferType::find($validated["type"]);
        //$transferType->transfers()->save($transfer);

        $transfer->update($validated);

        Cache::forget($this->key . '_id_' . $transfer_id);
        Cache::forget($this->key . '_all');
        Cache::forget('tool_all');
    }

    /**
     * restore a transfer.
     *
     * @param int
     * @param array
     */
    public function restore($transfer_id)
    {
        $transfer = Transfer::withTrashed()->find($transfer_id);
        $transfer->restore();

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $transfer_id);

        return $transfer;
    }

    /**
     * Get's all transfers.
     *
     * @return mixed
     */
    public function allTrashed()
    {
        $key = $this->key . '_all_trashed_';
        $transfers = Cache::remember($key, $this->expiration, static function (){
            return  Transfer::with([
                'warehouse.building',
                'employee',
                'provider'
            ])->orderBy('created_at', 'desc')->onlyTrashed()->get();
        });
        return $transfers;
    }

    public function store(array $validated, $transacion = null){

        //dd($transacion->products);
        $invoice = new Invoice();
        $invoice->status = 'NEW';
        $invoice->type = 'TESTE';
        $invoice->order = (empty($validated["doc"]) ? rand() : $validated["doc"]);

        $fromWarehouse = Warehouse::find($validated["warehouse-source"]);
        $fromWarehouse->invoicesFrom()->save($invoice);

        $toWarehouse = Warehouse::find($validated["warehouse-destination"]);
        $toWarehouse->invoicesTo()->save($invoice);

        $invoiceTotal = 0;
        // Percorre todos os itens da entrada
        foreach($transacion->products as $i => $item) {
            $tool = Tool::find($item['id']);
            $tool->invoices()->save($invoice);
            $invoiceTotal += $item['quantity'] * $item['value'];
        }
        $invoice->products = $transacion->products;

        $invoice->total_price = $invoiceTotal;
        $invoice->save();

        ////// Notificaçao Almoxarifes
        $almoxarifeOrigem = User::where('employee_id', $fromWarehouse->manager_id)->first();
        $almoxarifeDestino = User::where('employee_id', $toWarehouse->manager_id)->first();

        $invoiceLink = route('admin.fatura.show', $invoice->id);

        Notification::send($almoxarifeOrigem, new NewInvoiceNotification(Auth::user()->employee['name'], $invoiceLink));
        //Notification::send($almoxarifeDestino, new NewInvoiceNotification(Auth::user()->employee['name'], $invoiceLink));

        Cache::forget($this->key . '_all');

        //dd($transfer);
        return $invoice;
    }

    /**
     * Cancel a transfer by it's ID
     *
     * @param int
     * @return collection
     */
    public function cancel($transfer_id)
    {

        $transfer = Transfer::with([
            'entrance',
            'withdrawal'
        ])->withTrashed()->find($transfer_id);
        $transfer->status = 'CANCELED';
        $transfer->update();

        $cancelEntrance = EntranceRepository::cancel($transfer->entrance_id);
        $cancelWithdrawal = WithdrawalRepository::cancel($transfer->withdrawal_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $transfer_id);

        return $transfer;
    }
}
