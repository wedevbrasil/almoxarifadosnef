<?php

namespace App\Repositories;

use App\Models\TransferType;
use Illuminate\Support\Facades\Cache;

class TransferTypeRepository
{
    protected $expiration;
    protected $key;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'transferType';
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $transferTypes = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  TransferType::orderBy($column, $direction)->get();
        });

        return $transferTypes;

    }

    /**
     * Get's a transferType by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($transferType_id)
    {
        $key = $this->key . '_id_' . $transferType_id;
        $transferType = Cache::remember($key, $this->expiration, function () use ($transferType_id) {
            return TransferType::withTrashed()->find($transferType_id);
        });

        return $transferType;
    }

    /**
     * Get's a transferType lazy loading
     *
     * @param int
     * @return collection
     */
    public function with($with)
    {
        $key = $this->key . '_with_' . $with;
        $transferTypes = Cache::remember($key, $this->expiration, function () use ($with){
            return  TransferType::with($with)->get();
        });

        return $transferTypes;
    }

    /**
     * Get's all transferTypes.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';
        $transferTypes = Cache::remember($key, $this->expiration, static function (){
            return  TransferType::with([
                'transfers'
            ])->orderBy('created_at', 'desc')->get();
        });

        return $transferTypes;
    }

    /**
     * Deletes a transferType.
     *
     * @param int
     */
    public function delete($transferType_id)
    {

        TransferType::destroy($transferType_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $transferType_id);
    }

    /**
     * Updates a transferType.
     *
     * @param int
     * @param array
     */
    public function update($transferType_id, array $transferType_data)
    {
        TransferType::withTrashed()->find($transferType_id)->update($transferType_data);
    }


}
