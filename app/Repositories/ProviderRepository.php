<?php

namespace App\Repositories;

use App\Models\Provider;
use Illuminate\Support\Facades\Cache;

class ProviderRepository
{
    protected $expiration;
    protected $key;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'provider';
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $providers = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  Provider::orderBy($column, $direction)->get();
        });

        return $providers;

    }

    /**
     * Get's a provider by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($provider_id)
    {
        $key = $this->key . '_id_' . $provider_id;
        $provider = Cache::remember($key, $this->expiration, function () use ($provider_id) {
            return Provider::withTrashed()->find($provider_id);
        });

        return $provider;
    }

    /**
     * Get's a provider lazy loading
     *
     * @param int
     * @return collection
     */
    public function with($with)
    {
        $key = $this->key . '_with_' . $with;
        $providers = Cache::remember($key, $this->expiration, function () use ($with){
            return  Provider::with($with)->get();
        });

        return $providers;
    }

    /**
     * Get's all providers.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';
        $providers = Cache::remember($key, $this->expiration, static function (){
            return  Provider::with(['entrances', 'withdrawals', 'user'])->orderBy('created_at', 'desc')->get();
        });

        return $providers;
    }

    /**
     * Deletes a provider.
     *
     * @param int
     */
    public function delete($provider_id)
    {

        Provider::destroy($provider_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $provider_id);
    }

    /**
     * Updates a provider.
     *
     * @param int
     * @param array
     */
    public function update($provider_id, array $provider_data)
    {
        Provider::withTrashed()->find($provider_id)->update($provider_data);
    }


}
