<?php

namespace App\Repositories;

use App\Models\Entrance;
use App\Models\EntranceType;
use App\Models\Inventory;
use App\Models\Provider;
use App\Models\Tool;
use App\Models\Warehouse;
use App\Models\Withdrawal;
use Illuminate\Support\Facades\Cache;

class EntranceRepository
{
    protected $expiration;
    protected $key;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'entrance';
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $entrances = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  Entrance::orderBy($column, $direction)->get();
        });

        return $entrances;

    }

    /**
     * Get's a entrance by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($entrance_id)
    {
        $key = $this->key . '_id_' . $entrance_id;

        $entrance = Cache::remember($key, $this->expiration, function () use ($entrance_id){
            return  Entrance::with([
                'warehouse.building',
                'provider',
                'entranceType'
            ])->withTrashed()->find($entrance_id);
        });

        return $entrance;
    }

    /**
     * Get's a entrance by it's ID
     *
     * @param int
     * @return collection
     */
    public function show($entrance_id)
    {
        $key = $this->key . $entrance_id;

        $entrance = Cache::remember($key, $this->expiration, function () use ($entrance_id){
            return  Entrance::with([
                'warehouse',
                'provider',
                'entranceType'
            ])->withTrashed()->find($entrance_id);
        });

        return $entrance;
    }

    /**
     * Get's all entrances.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';
        $entrances = Cache::remember($key, $this->expiration, static function (){
            return  Entrance::with([
                'warehouse.building',

                'warehouse.inventories.tool.entrances',
                'warehouse.inventories.tool.withdrawals',
                'warehouse.inventories.tool.inventories',
                'warehouse.inventories.tool.itemType',

                'warehouse.entrances.warehouse.building.employees',

                'warehouse.withdrawals.warehouse.building.employees',

                'provider.withdrawals',
                'provider.entrances',

                'entranceType'
            ])->orderBy('created_at', 'desc')->get();
        });

        return $entrances;
    }

    /**
     * Get's all entrances.
     *
     * @return mixed
     */
    public function allTrashed()
    {
        $key = $this->key . '_all_trashed_';
        $entrances = Cache::remember($key, $this->expiration, static function (){
            return  Entrance::with([
                'warehouse.building',

                'warehouse.inventories.tool.entrances',
                'warehouse.inventories.tool.withdrawals',
                'warehouse.inventories.tool.inventories',
                'warehouse.inventories.tool.itemType',

                'warehouse.entrances.warehouse.building.employees',

                'warehouse.withdrawals.warehouse.building.employees',

                'provider.withdrawals',
                'provider.entrances',

                'entranceType'
            ])->orderBy('created_at', 'desc')->onlyTrashed()->get();
        });

        return $entrances;
    }

    /**
     * Deletes a entrance.
     *
     * @param int
     */
    public function delete($entrance_id)
    {

        Entrance::destroy($entrance_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $entrance_id);
    }

    /**
     * Updates a entrance.
     *
     * @param int
     * @param array
     */
    public function update($entrance_id, array $validated)
    {

        $entrance = Entrance::find($entrance_id);

        $warehouse = Warehouse::find($validated["warehouse"]);

        $provider = Provider::find($validated["provider"]);

        // Percorre todos os itens da entrada para atualizar inventario
        foreach($entrance->tool_ids as $i => $actItem) {
//dd($i);
            // Procura pelo invetario do almoxarifado e do item selecionado
            $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                ->where('tool_id', '=', $actItem)
                ->first();

            // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
            $inventory->quantity = $inventory->quantity - $entrance->quantity[$i];
            $inventory->save();

            // Zera os Arrays de itens e quantidades //
            $entrance->tool_ids = [];
            $entrance->quantity = [];

            // Salva a entrada nos itens
            $tool = Tool::find($actItem);
            $tool->entrances()->save($entrance);

        }

        // Percorre todos os itens da entrada para atualizar inventario
        foreach($validated["tool"] as $i => $item) {

            $arrQnt = $validated["quantity"];

            // Procura pelo invetario do almoxarifado e do item selecionado
            $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                ->where('tool_id', '=', $item)
                ->first();

            // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
            if ($inventory != null) {
                $inventory->quantity += $arrQnt[$i];
                $inventory->save();
            }
            // Senao encontrar cria uma entrada nova para o item no inventario do almoxarifado
            else {
                //$amount = 0;
                $newInventory = new Inventory();
                $newInventory->quantity = $arrQnt[$i];
                $newInventory->tool_id = $item;

                $warehouse->entrances()->save($newInventory);
            }

            // Salva a entrada nos itens
            $tool = Tool::find($item);
            $tool->entrances()->save($entrance);
        }

        // Atualiza os itens e quantidades na entrada
        $entrance->quantity = $validated["quantity"];
        $entrance->tool_ids = $validated["tool"];

        $provider->entrances()->save($entrance);
        $warehouse->entrances()->save($entrance);

        $entranceType = EntranceType::find($validated["type"]);
        $entranceType->entrances()->save($entrance);

        $entrance->update($validated);

        Cache::forget($this->key . '_id_' . $id);
        Cache::forget($this->key . '_all');
        Cache::forget('tool_all');
    }

    public function store(array $validated){

        $entrance = new Entrance($validated);
        //$entrance->status = 'NEW';
        $entrance->status = 'PROCESSED';

        $warehouse = Warehouse::find($validated["warehouse"]);

        // Percorre todos os itens da transferencia
        foreach($validated["tool"] as $i => $item) {
            //dd($validated["tool"]);

            $tool = Tool::find($item["id"]);
            $tool->entrances()->save($entrance);

            $validated["products"][$i]['name'] = $tool->name;
            $validated["products"][$i]['id'] = $tool->id;
            $validated["products"][$i]['cod'] = $tool->cod;
            $validated["products"][$i]['quantity'] = $validated["tool"][$i]['quantity'];
            $validated["products"][$i]['type_name'] = $tool->itemType->name;
            $validated["products"][$i]['unity_name'] = $tool->unity->name;
        }

        $entrance->products = $validated["products"];

        if(!empty($validated["provider"])) {
            $provider = Provider::find($validated["provider"]);
            $provider->entrances()->save($entrance);
        }

        /*
        if($validated["type"] == '5c6104bb0ccaa90007361e69') {
            $withdrawal = Withdrawal::find($validated["withdrawal_id"]);
            $withdrawal->entrance()->save($entrance);
        }
        */

        // Percorre todos os itens da entrada para atualizar inventario
        foreach($validated["products"] as $i => $product) {

            // Procura pelo invetario do almoxarifado e do item selecionado
            $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                ->where('tool_id', '=', $product['id'])
                ->first();

            // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
            if ($inventory != null) {
                $inventory->quantity += $product['quantity'];
                $inventory->save();
            }

            $tool = Tool::find($product['id']);
            $tool->entrances()->save($entrance);
        }

        // Atualiza os itens e quantidades na entrada
        //$entrance->quantity = $validated["quantity"];
        //$entrance->tool_ids = $validated["tool"];

        $warehouse->entrances()->save($entrance);

        $entranceType = EntranceType::find($validated["type"]);
        $entranceType->entrances()->save($entrance);

        Cache::forget($this->key . '_all');
        Cache::forget('tool_all');
        Cache::forget('warehouse_id_' . $validated["warehouse"]);

        //dd($entrance);
        return $entrance;
    }

    /**
     * Cancel a entrance by it's ID
     *
     * @param int
     * @return collection
     */
    public function cancel($entrance_id)
    {

        $entrance = Entrance::withTrashed()->find($entrance_id);
        $entrance->status = 'CANCELED';
        $entrance->update();

        // Percorre todos os itens da entrada para atualizar inventario
        foreach($entrance["products"] as $i => $product) {

            // Procura pelo invetario do almoxarifado e do item selecionado
            $inventory = Inventory::where('warehouse_id', '=', $entrance["warehouse_id"])
                ->where('tool_id', '=', $product['id'])
                ->first();

            // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
            if ($inventory != null) {
                $inventory->quantity -= $product['quantity'];
                $inventory->save();
            }
        }

        Cache::forget('entrance_all');
        Cache::forget('entrance_id_' . $entrance_id);
        Cache::forget('tool_all');
        Cache::forget('warehouse_id_' . $entrance["warehouse_id"]);

        return $entrance;
    }
}
