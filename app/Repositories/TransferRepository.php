<?php

namespace App\Repositories;

use App\Http\Requests\StoreTransfer;
use App\Models\Employee;
use App\Models\Inventory;
use App\Models\Invoice;
use App\Models\Provider;
use App\Models\Tool;
use App\Models\Transfer;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\TransferType;
use App\Notifications\NewTransferNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;

class TransferRepository
{
    protected $expiration;
    protected $key;

    protected $entranceRepository;
    protected $withdrawalRepository;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'transfer';
        $this->entranceRepository = new EntranceRepository();
        $this->withdrawalRepository = new WithdrawalRepository();
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $transfers = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  Transfer::with([
                'warehouse',
                'warehouse.building',
                'provider',
                'employee.user'
            ])->orderBy($column, $direction)->get();
        });

        return $transfers;

    }

    /**
     * Get's a transfer by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($transfer_id)
    {
        $key = $this->key . '_id_' . $transfer_id;

        $transfer = Cache::remember($key, $this->expiration, function () use ($transfer_id){
            return  Transfer::with([
                'fromWarehouse.building',
                'toWarehouse.building',
                'transferType'
            ])->withTrashed()->find($transfer_id);
        });

        return $transfer;
    }

    /**
     * Get's a transfer by it's ID
     *
     * @param int
     * @return collection
     */
    public function show($transfer_id)
    {
        $key = $this->key . $transfer_id;

        $transfer = Cache::remember($key, $this->expiration, function () use ($transfer_id){
            return  Transfer::with([
                'warehouse',
                'provider',
                'transferType'
            ])->withTrashed()->find($transfer_id);
        });

        return $transfer;
    }

    /**
     * Get's all transfers.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';
        $transfers = Cache::remember($key, $this->expiration, static function (){
            return  Transfer::with([
                'fromWarehouse.building',
                'toWarehouse.building',
                'transferType'
            ])->orderBy('created_at', 'desc')->get();
        });
        return $transfers;
    }

    /**
     * Deletes a transfer.
     *
     * @param int
     */
    public function delete($transfer_id)
    {

        Transfer::destroy($transfer_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $transfer_id);
    }

    /**
     * Updates a transfer.
     *
     * @param int
     * @param array
     */
    public function update($transfer_id, array $validated)
    {
        $transfer = Transfer::find($transfer_id);

        $warehouse = Warehouse::find($validated["warehouse"]);

        $provider = Provider::find($validated["provider"]);

        // Percorre todos os itens da transferencia para atualizar inventario
        foreach($transfer->tool_ids as $i => $actItem) {
            foreach($validated["quantity"] as $j => $quantity) {

                // Procura pelo invetario do almoxarifado e do item selecionado
                $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                    ->where('tool_id', '=', $actItem)
                    ->first();

                // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
                $inventory->quantity = $inventory->quantity - $quantity;
                $inventory->save();
            }
            // Salva a entrada nos itens
            $tool = Tool::find($actItem);
            $tool->transfers()->save($transfer);

        }

        // Zera os Arrays de itens e quantidades //
        $transfer->tool_ids = [];
        $transfer->quantity = [];

        // Percorre todos os itens da transferencia para atualizar inventario
        foreach($validated["tool"] as $i => $item) {
            foreach($validated["quantity"] as $j => $quantity) {

                $arrQnt = $validated["quantity"];
                $arrItem = $validated["tool"];

                // Procura pelo invetario do almoxarifado e do item selecionado
                $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                    ->where('tool_id', '=', $item)
                    ->first();

                // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
                if ($inventory != null) {
                    $inventory->quantity += $quantity;
                    $inventory->save();
                } // Senao encontrar cria uma transferencia nova para o item no inventario do almoxarifado
                else {
                    //$amount = 0;
                    $newInventory = new Inventory();
                    $newInventory->quantity = $quantity;
                    $newInventory->tool_id = $item;

                    $warehouse->transfers()->save($newInventory);
                }

                // Salva a transferencia nos itens
                $tool = Tool::find($item);
                $tool->transfers()->save($transfer);
            }
        }

        // Atualiza os itens e quantidades na transferencia
        $transfer->quantity = $validated["quantity"];
        $transfer->tool_ids = $validated["tool"];

        $provider->transfers()->save($transfer);
        $warehouse->transfers()->save($transfer);

        //$transferType = TransferType::find($validated["type"]);
        //$transferType->transfers()->save($transfer);

        $transfer->update($validated);

        Cache::forget($this->key . '_id_' . $transfer_id);
        Cache::forget($this->key . '_all');
        Cache::forget('tool_all');
    }

    /**
     * restore a transfer.
     *
     * @param int
     * @param array
     */
    public function restore($transfer_id)
    {
        $transfer = Transfer::withTrashed()->find($transfer_id);
        $transfer->restore();

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $transfer_id);

        return $transfer;
    }

    /**
     * Get's all transfers.
     *
     * @return mixed
     */
    public function allTrashed()
    {
        $key = $this->key . '_all_trashed_';
        $transfers = Cache::remember($key, $this->expiration, static function (){
            return  Transfer::with([
                'warehouse.building',
                'employee',
                'provider'
            ])->orderBy('created_at', 'desc')->onlyTrashed()->get();
        });
        return $transfers;
    }

    public function store(array $validated){

        $transfer = new Transfer($validated);
        $transfer->status = 'NEW';
        $transfer->return_date = Carbon::today()->addDays($validated['return_date']);

        $fromWarehouse = Warehouse::find($validated["warehouse-source"]);
        $toWarehouse = Warehouse::find($validated["warehouse-destination"]);

        $transferType = TransferType::find($validated["type"]);

        // Percorre todos os itens da transferencia
        foreach($validated["tool"] as $i => $item) {
            //dd($item);

            $tool = Tool::find($item['id']);
            $tool->transfers()->save($transfer);

            $validated["products"][$i]['name'] = $tool->name;
            $validated["products"][$i]['cod'] = $tool->cod;
            $validated["products"][$i]['id'] = $tool->id;
            $validated["products"][$i]['quantity'] = $validated["tool"][$i]['quantity'];
            $validated["products"][$i]['value'] = $validated["tool"][$i]['value'];
            $validated["products"][$i]['type_name'] = $tool->itemType->name;
            $validated["products"][$i]['unity_name'] = $tool->unity->name;
        }

        $transfer->products = $validated["products"];

        $fromWarehouse->warehouseFrom()->save($transfer);
        $toWarehouse->warehouseTo()->save($transfer);

        $transferType->transfers()->save($transfer);

        //// Invoice Repository
        /*
        if($validated["invoice"]){

            $invoice = new Invoice();
            $invoice->status = 'NEW';
            $invoice->type = 'TRANSFER';
            $invoice->order = (empty($validated["doc"]) ? rand() : $validated["doc"]);

            $entrance->invoice()->save($invoice);
            $transfer->invoice()->save($invoice);

            $warehouse->invoicesFrom()->save($invoice);

            $toWarehouse = Warehouse::find($validated["warehouse-destination"]);
            $toWarehouse->invoicesTo()->save($invoice);

            $invoiceTotal = 0;
            // Percorre todos os itens da entrada
            foreach($validated["tool"] as $i => $item) {

                $arrQnt = $validated["quantity"];

                $tool = Tool::find($item['id']);
                $tool->invoices()->save($invoice);
                $invoiceTotal += $item['quantity'] * $item['valor'];
                $validated["tool"][$i]['name'] = $tool->name;
            }
            $invoice->products = $validated["tool"];

            $invoice->total_price = $invoiceTotal;
            $invoice->save();
        }
        */

        // Aprovação transferencia
        // Nova entrada
        $newEntrance = [
            'warehouse' => $validated["warehouse-destination"],
            'tool' => $validated["tool"],
            'products' => $validated["products"],
            'type' => '5c6104bb0ccaa90007361e69',
            'transfer_id' => $transfer->id
        ];

        $entrance = $this->entranceRepository->store($newEntrance);

        $entrance->transfer()->save($transfer);
        $transfer->entrance()->save($entrance);
        // Fim Nova entrada

        // Nova retirada
        $newWithdrawal = [
            'warehouse' => $validated["warehouse-source"],
            'tool' => $validated["tool"],
            'products' => $validated["products"],
            'type' => '5d6f51e020dd0f284f624b39',
            'transfer_id' => $transfer->id
        ];

        $withdrawal = $this->withdrawalRepository->store($newWithdrawal);

        $withdrawal->transfer()->save($transfer);
        $transfer->withdrawal()->save($withdrawal);
        // Fim Nova retirada

        $transfer->status = 'PROCESSED';
        $transfer->update();
        //Fim aprovação

        ////// Notificaçao Almoxarifes
        $almoxarifeOrigem = User::where('employee_id', $fromWarehouse->manager_id)->first();
        $almoxarifeDestino = User::where('employee_id', $toWarehouse->manager_id)->first();

        $transferLink = route('admin.transferencia.show', $transfer->id);

        Notification::send($almoxarifeOrigem, new NewTransferNotification(Auth::user()->employee['name'], $transferLink));
        Notification::send($almoxarifeDestino, new NewTransferNotification(Auth::user()->employee['name'], $transferLink));

        Cache::forget($this->key . '_all');
        Cache::forget('tool_all');
        Cache::forget('warehouse_id_' . $validated["warehouse-source"]);
        Cache::forget('warehouse_id_' . $validated["warehouse-destination"]);

        //dd($transfer);
        return $transfer;
    }

    /**
     * Cancel a transfer by it's ID
     *
     * @param int
     * @return collection
     */
    public function cancel($transfer_id)
    {

        $transfer = Transfer::with([
            'entrance',
            'withdrawal'
        ])->withTrashed()->find($transfer_id);
        $transfer->status = 'CANCELED';
        $transfer->update();

        $cancelEntrance = EntranceRepository::cancel($transfer->entrance_id);
        $cancelWithdrawal = WithdrawalRepository::cancel($transfer->withdrawal_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $transfer_id);

        return $transfer;
    }
}
