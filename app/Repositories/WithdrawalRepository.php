<?php

namespace App\Repositories;

use App\Http\Requests\StoreWithdrawal;
use App\Models\Employee;
use App\Models\Inventory;
use App\Models\Invoice;
use App\Models\Provider;
use App\Models\Tool;
use App\Models\Transfer;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\Withdrawal;
use App\Models\WithdrawalType;
use App\Notifications\NewTransferNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;

class WithdrawalRepository
{
    protected $expiration;
    protected $key;

    protected $entranceRepository;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'withdrawal';
        $this->entranceRepository = new EntranceRepository();
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $withdrawals = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  Withdrawal::with([
                'warehouse',
                'warehouse.building',
                'provider',
                'employee.user'
            ])->orderBy($column, $direction)->get();
        });

        return $withdrawals;

    }

    /**
     * Get's a withdrawal by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($withdrawal_id)
    {
        $key = $this->key . '_id_' . $withdrawal_id;

        $withdrawal = Cache::remember($key, $this->expiration, function () use ($withdrawal_id){
            return  Withdrawal::with([
                'warehouse',
                'warehouse.building',
                'provider',
                'employee.user'
            ])->withTrashed()->find($withdrawal_id);
        });

        return $withdrawal;
    }

    /**
     * Get's a withdrawal by it's ID
     *
     * @param int
     * @return collection
     */
    public function show($withdrawal_id)
    {
        $key = $this->key . $withdrawal_id;

        $withdrawal = Cache::remember($key, $this->expiration, function () use ($withdrawal_id){
            return  Withdrawal::with([
                'warehouse',
                'provider',
                'withdrawalType'
            ])->withTrashed()->find($withdrawal_id);
        });

        return $withdrawal;
    }

    /**
     * Get's all withdrawals.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';
        $withdrawals = Cache::remember($key, $this->expiration, static function (){
            return  Withdrawal::with([
                'warehouse.building',
                'employee',
                'provider',
                'withdrawalType'
            ])->orderBy('created_at', 'desc')->get();
        });
        return $withdrawals;
    }

    /**
     * Deletes a withdrawal.
     *
     * @param int
     */
    public function delete($withdrawal_id)
    {

        Withdrawal::destroy($withdrawal_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $withdrawal_id);
    }

    /**
     * Updates a withdrawal.
     *
     * @param int
     * @param array
     */
    public function update($withdrawal_id, array $validated)
    {
        $withdrawal = Withdrawal::find($withdrawal_id);

        $warehouse = Warehouse::find($validated["warehouse"]);

        $provider = Provider::find($validated["provider"]);

        // Percorre todos os itens da retirada para atualizar inventario
        foreach($withdrawal->tool_ids as $i => $actItem) {
            foreach($validated["quantity"] as $j => $quantity) {

                // Procura pelo invetario do almoxarifado e do item selecionado
                $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                    ->where('tool_id', '=', $actItem)
                    ->first();

                // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
                $inventory->quantity = $inventory->quantity - $quantity;
                $inventory->save();
            }
            // Salva a entrada nos itens
            $tool = Tool::find($actItem);
            $tool->withdrawals()->save($withdrawal);

        }

        // Zera os Arrays de itens e quantidades //
        $withdrawal->tool_ids = [];
        $withdrawal->quantity = [];

        // Percorre todos os itens da retirada para atualizar inventario
        foreach($validated["tool"] as $i => $item) {
            foreach($validated["quantity"] as $j => $quantity) {

                $arrQnt = $validated["quantity"];
                $arrItem = $validated["tool"];

                // Procura pelo invetario do almoxarifado e do item selecionado
                $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                    ->where('tool_id', '=', $item)
                    ->first();

                // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
                if ($inventory != null) {
                    $inventory->quantity += $quantity;
                    $inventory->save();
                } // Senao encontrar cria uma retirada nova para o item no inventario do almoxarifado
                else {
                    //$amount = 0;
                    $newInventory = new Inventory();
                    $newInventory->quantity = $quantity;
                    $newInventory->tool_id = $item;

                    $warehouse->withdrawals()->save($newInventory);
                }

                // Salva a retirada nos itens
                $tool = Tool::find($item);
                $tool->withdrawals()->save($withdrawal);
            }
        }

        // Atualiza os itens e quantidades na retirada
        $withdrawal->quantity = $validated["quantity"];
        $withdrawal->tool_ids = $validated["tool"];

        $provider->withdrawals()->save($withdrawal);
        $warehouse->withdrawals()->save($withdrawal);

        //$withdrawalType = WithdrawalType::find($validated["type"]);
        //$withdrawalType->withdrawals()->save($withdrawal);

        $withdrawal->update($validated);

        Cache::forget($this->key . '_id_' . $withdrawal_id);
        Cache::forget($this->key . '_all');
        Cache::forget('tool_all');
    }

    /**
     * restore a withdrawal.
     *
     * @param int
     * @param array
     */
    public function restore($withdrawal_id)
    {
        $withdrawal = Withdrawal::withTrashed()->find($withdrawal_id);
        $withdrawal->restore();

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $withdrawal_id);

        return $withdrawal;
    }

    /**
     * Get's all withdrawals.
     *
     * @return mixed
     */
    public function allTrashed()
    {
        $key = $this->key . '_all_trashed_';
        $withdrawals = Cache::remember($key, $this->expiration, static function (){
            return  Withdrawal::with([
                'warehouse.building',
                'employee',
                'provider'
            ])->orderBy('created_at', 'desc')->onlyTrashed()->get();
        });
        return $withdrawals;
    }

    public function storeBak(array $validated){

        $withdrawal = new Transfer($validated);
        $withdrawal->status = 'NEW';
        $withdrawal->tools = $validated["tool"];

        $warehouse = Warehouse::find($validated["warehouse-source"]);

        $withdrawalType = WithdrawalType::find($validated["type"]);

        // Percorre todos os itens da entrada
        foreach($validated["tool"] as $i => $item) {
            //dd($item);

            $arrQnt = $validated["quantity"];

            // Procura pelo invetario do almoxarifado e do item selecionado
            $aux = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                ->where('tool_id', '=', $item)
                ->first();

            // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
            if ($aux != null) {
                $aux->quantity = $aux->quantity - $arrQnt[$i];
                $aux->save();
            }
            // Senao encontrar cria uma entrada nova para o item no inventario do almoxarifado
            else {
                //$amount = 0;
                $inventory = new Inventory();
                $inventory->quantity = $arrQnt[$i];
                $inventory->tool_id = $item;

                /*$tool = Tool::find($item);
                $tool->entrances()->save($inventory);*/

                $warehouse->withdrawals()->save($inventory);
            }

            $tool = Tool::find($item['id']);
            $tool->withdrawals()->save($withdrawal);
        }

        if(!empty($validated["employee"])){
            $employee = Employee::find($validated["employee"]);
            $employee->withdrawals()->save($withdrawal);
        }

        $warehouse->withdrawals()->save($withdrawal);
        $withdrawalType->withdrawals()->save($withdrawal);

        //// Transfer Repository
        if($validated["type"] === '5d490461c70471340e0fd4ce'){
            $newEntrance = [
                'warehouse' => $validated["warehouse-destination"],
                'tool' => $validated["tool"],
                'quantity' => $validated["quantity"],
                'type' => '5c6104bb0ccaa90007361e69',
                'withdrawal_id' => $withdrawal->id
            ];

            $entrance = $this->entranceRepository->store($newEntrance);

            $entrance->withdrawal()->save($withdrawal);

            /* Notificaçao Almoxarife Destino */
            $almoxarifado = Warehouse::find($validated["warehouse-destination"]);
            $almoxarife = User::where('employee_id', $almoxarifado->manager_id)->first();

            $link = route('admin.entrada.show', $entrance->id);

            $notifyAlmoxarife = Notification::send($almoxarife, new NewTransferNotification(Auth::user()->employee['name'], $link));

        }

        //// Invoice Repository
        if($validated["invoice"]){

            $invoice = new Invoice();
            $invoice->status = 'NEW';
            $invoice->type = 'TRANSFER';
            $invoice->order = (empty($validated["doc"]) ? rand() : $validated["doc"]);

            $entrance->invoice()->save($invoice);
            $withdrawal->invoice()->save($invoice);

            $warehouse->invoicesFrom()->save($invoice);

            $toWarehouse = Warehouse::find($validated["warehouse-destination"]);
            $toWarehouse->invoicesTo()->save($invoice);

            $invoiceTotal = 0;
            // Percorre todos os itens da entrada
            foreach($validated["tool"] as $i => $item) {

                $arrQnt = $validated["quantity"];

                $tool = Tool::find($item['id']);
                $tool->invoices()->save($invoice);
                $invoiceTotal += $item['quantity'] * $item['valor'];
                $validated["tool"][$i]['name'] = $tool->name;
            }
            $invoice->products = $validated["tool"];

            $invoice->total_price = $invoiceTotal;
            $invoice->save();
        }

        Cache::forget($this->key . '_all');
        Cache::forget('tool_all');

        dd($withdrawal);
        return $withdrawal;
    }

    public function store(array $validated){

        $withdrawal = new Withdrawal($validated);
        //$withdrawal->status = 'NEW';
        $withdrawal->status = 'PROCESSED';

        $warehouse = Warehouse::find($validated["warehouse"]);

        // Percorre todos os itens da transferencia
        foreach($validated["tool"] as $i => $item) {
            //dd($validated["tool"]);

            $tool = Tool::find($item["id"]);
            $tool->withdrawals()->save($withdrawal);

            $validated["products"][$i]['name'] = $tool->name;
            $validated["products"][$i]['id'] = $tool->id;
            $validated["products"][$i]['cod'] = $tool->cod;
            $validated["products"][$i]['quantity'] = $validated["tool"][$i]['quantity'];
            $validated["products"][$i]['type_name'] = $tool->itemType->name;
            $validated["products"][$i]['unity_name'] = $tool->unity->name;
        }

        /*
        if($validated["type"] == '5c6104bb0ccaa90007361e69') {
            $withdrawal = Withdrawal::find($validated["withdrawal_id"]);
            $withdrawal->entrance()->save($entrance);
        }
        */
        if(!empty($validated["employee"])) {
            $employee = Employee::find($validated["employee"]);
            $withdrawal->employee()->save($employee);
        }

        // Percorre todos os itens da retirada para atualizar inventario
        foreach($validated["products"] as $i => $product) {

            // Procura pelo invetario do almoxarifado e do item selecionado
            $inventory = Inventory::where('warehouse_id', '=', $validated["warehouse"])
                ->where('tool_id', '=', $product['id'])
                ->first();

            // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
            if ($inventory != null) {
                $inventory->quantity -= $product['quantity'];
                $inventory->save();
            }

            $tool = Tool::find($product['id']);
            $tool->withdrawals()->save($withdrawal);
        }

        // Atualiza os itens e quantidades na retirada
        //$entrance->quantity = $validated["quantity"];
        //$entrance->tool_ids = $validated["tool"];

        $warehouse->withdrawals()->save($withdrawal);

        $withdrawalType = WithdrawalType::find($validated["type"]);
        $withdrawalType->withdrawals()->save($withdrawal);

        Cache::forget($this->key . '_all');
        Cache::forget('tool_all');
        Cache::forget('warehouse_id_' . $validated["warehouse"]);

        return $withdrawal;
    }

    /**
     * Cancel a entrance by it's ID
     *
     * @param int
     * @return collection
     */
    public function cancel($withdrawal_id)
    {

        $withdrawal = Withdrawal::withTrashed()->find($withdrawal_id);
        $withdrawal->status = 'CANCELED';
        $withdrawal->update();

        // Percorre todos os itens da retirada para atualizar inventario
        foreach($withdrawal["products"] as $i => $product) {

            // Procura pelo invetario do almoxarifado e do item selecionado
            $inventory = Inventory::where('warehouse_id', '=', $withdrawal["warehouse_id"])
                ->where('tool_id', '=', $product['id'])
                ->first();

            // Se encontrar pelo item no inventario do almoxarifado, atualiza  a quantidade
            if ($inventory != null) {
                $inventory->quantity += $product['quantity'];
                $inventory->save();
            }
        }

        Cache::forget('withdrawal_all');
        Cache::forget('withdrawal_id_' . $withdrawal_id);
        Cache::forget('tool_all');
        Cache::forget('warehouse_id_' . $withdrawal["warehouse_id"]);

        return $withdrawal;
    }
}
