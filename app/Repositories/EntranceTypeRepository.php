<?php

namespace App\Repositories;

use App\Models\EntranceType;
use Illuminate\Support\Facades\Cache;

class EntranceTypeRepository
{
    protected $expiration;
    protected $key;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'entranceType';
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $entranceTypes = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  EntranceType::orderBy($column, $direction)->get();
        });

        return $entranceTypes;

    }

    /**
     * Get's a entranceType by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($entranceType_id)
    {
        $key = $this->key . '_id_' . $entranceType_id;
        $entranceType = Cache::remember($key, $this->expiration, function () use ($entranceType_id) {
            return EntranceType::withTrashed()->find($entranceType_id);
        });

        return $entranceType;
    }

    /**
     * Get's a entranceType lazy loading
     *
     * @param int
     * @return collection
     */
    public function with($with)
    {
        $key = $this->key . '_with_' . $with;
        $entranceTypes = Cache::remember($key, $this->expiration, function () use ($with){
            return  EntranceType::with($with)->get();
        });

        return $entranceTypes;
    }

    /**
     * Get's all entranceTypes.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';
        $entranceTypes = Cache::remember($key, $this->expiration, static function (){
            return  EntranceType::with([
                'entrances'
            ])->orderBy('created_at', 'desc')->get();
        });

        return $entranceTypes;
    }

    /**
     * Deletes a entranceType.
     *
     * @param int
     */
    public function delete($entranceType_id)
    {

        EntranceType::destroy($entranceType_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $entranceType_id);
    }

    /**
     * Updates a entranceType.
     *
     * @param int
     * @param array
     */
    public function update($entranceType_id, array $entranceType_data)
    {
        EntranceType::withTrashed()->find($entranceType_id)->update($entranceType_data);
    }


}
