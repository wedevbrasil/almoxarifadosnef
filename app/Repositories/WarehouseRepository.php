<?php

namespace App\Repositories;

use App\Models\Warehouse;
use Illuminate\Support\Facades\Cache;

class WarehouseRepository
{
    protected $expiration;
    protected $key;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'warehouse';
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $warehouses = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  Warehouse::orderBy($column, $direction)->get();
        });

        return $warehouses;

    }

    /**
     * Get's a warehouse by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($warehouse_id)
    {
        $key = $this->key . '_id_' . $warehouse_id;
        $warehouse = Cache::remember($key, $this->expiration, function () use ($warehouse_id) {
            return Warehouse::with([
                'inventories.tool.entrances.provider',
                'inventories.tool.withdrawals',
                'inventories.tool.inventories',
                'inventories.tool.unity',
                'inventories.tool.itemType',

                'entrances.provider',
                'entrances.warehouse.building.employees',

                'withdrawals.provider',
                'withdrawals.employee',
                'withdrawals.warehouse.building.employees',

                'building',

                'manager'
            ])->withTrashed()->find($warehouse_id);
        });

        return $warehouse;
    }

    /**
     * Get's a warehouse lazy loading
     *
     * @param int
     * @return collection
     */
    public function with($with)
    {
        $key = $this->key . '_with_' . $with;
        $warehouses = Cache::remember($key, $this->expiration, function () use ($with){
            return  Warehouse::with($with)->get();
        });

        return $warehouses;
    }

    /**
     * Get's all warehouses.
     *
     * @return mixed
     */
    public function all()
    {
        $key = 'warehouse_all';
        $expiration = 60 * 60 * 24;

        $warehouses = Cache::remember($key, $expiration, static function (){
            return  Warehouse::with(['building', 'entrances', 'withdrawals', 'inventories', 'manager'])
                ->orderBy('created_at', 'desc')->get();
        });

        return $warehouses;
    }

    /**
     * Deletes a warehouse.
     *
     * @param int
     */
    public function delete($warehouse_id)
    {

        Warehouse::destroy($warehouse_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $warehouse_id);
    }

    /**
     * Updates a warehouse.
     *
     * @param int
     * @param array
     */
    public function update($warehouse_id, array $warehouse_data)
    {
        Warehouse::withTrashed()->find($warehouse_id)->update($warehouse_data);
    }


}
