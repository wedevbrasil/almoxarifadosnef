<?php

namespace App\Repositories;

use App\Models\WithdrawalType;
use Illuminate\Support\Facades\Cache;

class WithdrawalTypeRepository
{
    protected $expiration;
    protected $key;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'withdrawalType';
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $withdrawalTypes = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  WithdrawalType::orderBy($column, $direction)->get();
        });

        return $withdrawalTypes;

    }

    /**
     * Get's a withdrawalType by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($withdrawalType_id)
    {
        $key = $this->key . '_id_' . $withdrawalType_id;
        $withdrawalType = Cache::remember($key, $this->expiration, function () use ($withdrawalType_id) {
            return WithdrawalType::withTrashed()->find($withdrawalType_id);
        });

        return $withdrawalType;
    }

    /**
     * Get's a withdrawalType lazy loading
     *
     * @param int
     * @return collection
     */
    public function with($with)
    {
        $key = $this->key . '_with_' . $with;
        $withdrawalTypes = Cache::remember($key, $this->expiration, function () use ($with){
            return  WithdrawalType::with($with)->get();
        });

        return $withdrawalTypes;
    }

    /**
     * Get's all withdrawalTypes.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';
        $withdrawalTypes = Cache::remember($key, $this->expiration, static function (){
            return  WithdrawalType::with([
                'withdrawals'
            ])->orderBy('created_at', 'desc')->get();
        });

        return $withdrawalTypes;
    }

    /**
     * Deletes a withdrawalType.
     *
     * @param int
     */
    public function delete($withdrawalType_id)
    {

        WithdrawalType::destroy($withdrawalType_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $withdrawalType_id);
    }

    /**
     * Updates a withdrawalType.
     *
     * @param int
     * @param array
     */
    public function update($withdrawalType_id, array $withdrawalType_data)
    {
        WithdrawalType::withTrashed()->find($withdrawalType_id)->update($withdrawalType_data);
    }


}
