<?php

namespace App\Repositories;

use App\Models\Building;
use App\Models\Warehouse;
use Illuminate\Support\Facades\Cache;

class BuildingRepository
{
    protected $expiration;
    protected $key;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'building';
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_all';
        $buildings = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  Building::orderBy($column, $direction)->get();
        });

        return $buildings;

    }

    /**
     * Get's a building by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($building_id)
    {
        $key = $this->key . '_id_' . $building_id;

        $building = Cache::remember($key, $this->expiration, function () use ($building_id){
            return  Building::with([
                'warehouses.inventories.tool.entrances.provider',
                'warehouses.inventories.tool.withdrawals',
                'warehouses.inventories.tool.inventories',
                'warehouses.inventories.tool.unity',
                'warehouses.inventories.tool.itemType',

                'warehouses.entrances.provider',
                'warehouses.entrances.warehouse.building.employees',

                'warehouses.withdrawals.provider',
                'warehouses.withdrawals.employee',
                'warehouses.withdrawals.warehouse.building.employees',

                'employees.sector',
                'employees.office',

                'manager'
            ])->withTrashed()->find($building_id);
        });

        return $building;
    }

    /**
     * Get's all buildings.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';
        $buildings = Cache::remember($key, $this->expiration, static function (){
            return  Building::with([
                'warehouses',
                'employees',
                'manager'
            ])->orderBy('number', 'desc')->get();
        });

        return $buildings;
    }

    /**
     * Get's all buildings.
     *
     * @return mixed
     */
    public function buildingsCards()
    {
        $key = $this->key . '_cards';
        $buildings = Cache::remember($key, $this->expiration, static function (){
            return  Building::with([
                'warehouses',
                'warehouses.inventories.tool',
                'employees',
                'manager'
            ])->orderBy('number', 'desc')->get();
        });

        return $buildings;
    }

    /**
     * Deletes a building.
     *
     * @param int
     */
    public function delete($building_id)
    {

        Building::destroy($building_id);

        Cache::forget($this->key . '_id_' . $building_id);
    }

    /**
     * Updates a building.
     *
     * @param int
     * @param array
     */
    public function update($building_id, array $building_data)
    {
        Building::withTrashed()->find($building_id)->update($building_data);
    }
}
