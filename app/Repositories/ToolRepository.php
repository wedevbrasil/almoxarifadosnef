<?php

namespace App\Repositories;

use App\Models\Inventory;
use App\Models\ItemType;
use App\Models\Tool;
use App\Models\Unity;
use App\Models\Warehouse;
use Illuminate\Support\Facades\Cache;

class ToolRepository
{
    protected $expiration;
    protected $key;

    public function __construct()
    {
        $this->expiration = 60 * 60 * 24;
        $this->key = 'tool';
    }


    public function orderBy($column, $direction)
    {
        $key = $this->key . '_orderBy_' . $column . $directions;
        $tools = Cache::remember($key, $this->expiration, static function () use ($column, $direction){
            return  Tool::orderBy($column, $direction)->get();
        });

        return $tools;

    }

    /**
     * Get's a tool by it's ID
     *
     * @param int
     * @return collection
     */
    public function find($tool_id)
    {
        $key = $this->key . '_id_' . $tool_id;

        $tool = Cache::remember($key, $this->expiration, function () use ($tool_id) {
            return Tool::with([
                'entrances.provider',
                'entrances.warehouse',

                'withdrawals.provider',
                'withdrawals.warehouse',

                'inventories.warehouse',
                'inventories.warehouse.building',

                'itemType',
                'unity'
            ])->withTrashed()->find($tool_id);
        });

        return $tool;
    }

    /**
     * Get's a tool by it's ID
     *
     * @param int
     * @return collection
     */
    public function showItensEntrance($tool_ids)
    {
        $key = $this->key . serialize($tool_ids);
        $tools = Cache::remember($key, $this->expiration, function () use ($tool_ids) {
            return Tool::with([
                'itemType',
                'unity'
            ])->withTrashed()->findMany($tool_ids);
        });

        return $tools;
    }

    public function showItensWithdrawal($tool_ids)
    {
        $key = $this->key . serialize($tool_ids);
        $tools = Cache::remember($key, $this->expiration, function () use ($tool_ids) {
            return Tool::with([
                'itemType',
                'unity'
            ])->withTrashed()->findMany($tool_ids);
        });

        return $tools;
    }

    public function showItensTransfer($tool_ids)
    {
        $key = $this->key . serialize($tool_ids);
        $tools = Cache::remember($key, $this->expiration, function () use ($tool_ids) {
            return Tool::with([
                'itemType',
                'unity'
            ])->withTrashed()->findMany($tool_ids);
        });

        return $tools;
    }


    /**
     * Get's a tool by it's ID
     *
     * @param int
     * @return collection
     */
    public function show($tool_id)
    {
        $key = $this->key . $tool_id;

        $tool = Cache::remember($key, $this->expiration, function () use ($tool_id) {
            return Tool::with([
                'entrances.provider',
                'entrances.warehouse',

                'withdrawals.provider',
                'withdrawals.warehouse',

                'inventories.warehouses',
                'itemType',
                'unity'
            ])->withTrashed()->find($tool_id);
        });

        return $tool;
    }

    /**
     * Get's a tool lazy loading
     *
     * @param int
     * @return collection
     */
    public function with($with)
    {
        $key = $this->key . '_with_' . serialize($with);
        $tools = Cache::remember($key, $this->expiration, function () use ($with){
            return  Tool::with($with)->get();
        });

        return $tools;
    }

    /**
     * Get's all tools.
     *
     * @return mixed
     */
    public function all()
    {
        $key = $this->key . '_all';

        $tools = Cache::remember($key, $this->expiration, static function (){
            return  Tool::with([
                'entrances.provider',
                'entrances.warehouse',
                'entrances.warehouse.building',

                'withdrawals.provider',
                'withdrawals.warehouse',
                'withdrawals.warehouse.building',

                'inventories.warehouse',
                'itemType',
                'unity'
            ])->orderBy('created_at', 'desc')->get();
        });

        return $tools;
    }

    /**
     * Get's all tools.
     *
     * @return mixed
     */
    public function datatables()
    {
        $key = $this->key . '_all_datatables';

        Cache::forget($key);

        $tools = Cache::remember($key, $this->expiration, static function (){
            return  Tool::orderBy('created_at', 'desc')->get();
        });

        return $tools;
    }

    /**
     * Deletes a tool.
     *
     * @param int
     */
    public function delete($tool_id)
    {

        Tool::destroy($tool_id);

        Cache::forget($this->key . '_all');
        Cache::forget($this->key . '_id_' . $tool_id);
    }

    /**
     * Updates a tool.
     *
     * @param int
     * @param array
     */
    public function update($tool_id, array $tool_data)
    {
        Tool::withTrashed()->find($tool_id)->update($tool_data);
    }

    /**
     * Store a tool.
     *
     * @param int
     * @param array
     */
    public function store(array $validated){
        $tool = new Tool($validated);

        $itemType = ItemType::find($validated['type']);
        $itemType->tools()->save($tool);

        $unity = Unity::find($validated['unity']);
        $unity->tools()->save($tool);

        $warehouses = WarehouseRepository::all();

        // Cria Inventario
        $tools = Tool::all();
        foreach($warehouses as $warehouse) {
            $newInventory = new Inventory();
            $newInventory->quantity = 0;
            $newInventory->tool_id = $tool->id;
            $newInventory->warehouse_id = $warehouse->id;
            $newInventory->stock_min = $validated['stock_min'];
            $newInventory->save();
            Cache::forget('warehouse_id_' . $warehouse->id);

        }
        Cache::forget('tool_all');
dd($tool);
        return $tool;
    }


}
