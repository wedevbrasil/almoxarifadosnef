<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Role extends Eloquent
{
    use SoftDeletes;

    public function user()
    {
        return $this->hasOne(User::class)->withTrashed();
    }
}
