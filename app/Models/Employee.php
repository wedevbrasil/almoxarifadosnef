<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Employee extends Eloquent
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc', 'name', 'sector','function','contact','building'
    ];

    public function building()
    {
        return $this->belongsTo(Building::class)->withTrashed();
    }

    public function user()
    {
        return $this->hasOne(User::class)->withTrashed();
    }

    public function sector()
    {
        return $this->belongsTo(Sector::class)->withTrashed();
    }

    public function office()
    {
        return $this->belongsTo(Office::class)->withTrashed();
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class)->withTrashed();
    }

    public function warehouse()
    {
        return $this->hasOne(Warehouse::class, 'manager_id', 'id')->withTrashed();
    }

    public function buildingManaged()
    {
        return $this->hasOne(Building::class, 'manager_id', 'id')->withTrashed();
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
