<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Building extends Eloquent
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number',
        'name',
        'address'
    ];

    public function warehouses()
    {
        return $this->hasMany(Warehouse::class)->withTrashed();
    }

    public function employees()
    {
        return $this->hasMany(Employee::class)->withTrashed();
    }

    public function manager()
    {
        return $this->belongsTo(Employee::class)->withTrashed();
    }


    public function warehousesCount()
    {
        return $this->hasMany(Warehouse::class)->count();
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
