<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Warehouse extends Eloquent
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'building', 'description',
    ];

    public function building()
    {
        return $this->belongsTo(Building::class)->withTrashed();
    }

    public function entrances()
    {
        return $this->hasMany(Entrance::class)->withTrashed();
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class)->withTrashed();
    }

    public function transfers()
    {
        return $this->hasMany(Transfer::class)->withTrashed();
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class)->withTrashed();
    }

    public function invoices()
    {
        return $this->hasMany(Inventory::class)->withTrashed();
    }

    public function manager()
    {
        return $this->belongsTo(Employee::class)->withTrashed();
    }

    public function invoicesTo()
    {
        return $this->hasMany(Invoice::class, 'to_warehouse')->withTrashed();
    }

    public function invoicesFrom()
    {
        return $this->hasMany(Invoice::class,'from_warehouse')->withTrashed();
    }

    public function warehouseTo()
    {
        return $this->hasMany(Warehouse::class, 'to_warehouse')->withTrashed();
    }

    public function warehouseFrom()
    {
        return $this->hasMany(Warehouse::class,'from_warehouse')->withTrashed();
    }


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
