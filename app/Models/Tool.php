<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Tool extends Eloquent
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod',
        'name',
        'type',
        'stock_min'
    ];

    public function entrances()
    {
        return $this->belongsToMany(Entrance::class)->withTrashed();
    }

    public function withdrawals()
    {
        return $this->belongsToMany(Withdrawal::class)->withTrashed();
    }

    public function transfers()
    {
        return $this->belongsToMany(Transfer::class)->withTrashed();
    }

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class)->withTrashed();
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class)->withTrashed();
    }

    public function unity()
    {
        return $this->belongsTo(Unity::class)->withTrashed();
    }

    public function itemType()
    {
        return $this->belongsTo(ItemType::class)->withTrashed();
    }


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
