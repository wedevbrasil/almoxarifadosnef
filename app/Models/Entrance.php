<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Entrance extends Eloquent
{
    use SoftDeletes;
    //use Cachable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc', 'quantity', 'products'
    ];

    public function provider()
    {
        return $this->belongsTo(Provider::class)->withTrashed();
    }

    public function tool()
    {
        return $this->embedsMany(Tool::class)->withTrashed();
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class)->withTrashed();
    }

    public function entranceType()
    {
        return $this->belongsTo(EntranceType::class)->withTrashed();
    }

    public function withdrawal()
    {
        return $this->hasOne(Withdrawal::class)->withTrashed();
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class)->withTrashed();
    }

    public function transfer()
    {
        return $this->hasOne(Transfer::class)->withTrashed();
    }


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
