<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Provider extends Eloquent
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc',
        'name',
        'address',
        'contact'
    ];

    public function entrances()
    {
        return $this->hasMany(Entrance::class)->withTrashed();
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class)->withTrashed();
    }

    public function user()
    {
        return $this->belongsToMany(User::class)->withTrashed();
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
