<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Clube
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property boolean|null $status
 */

class Invoice extends Eloquent
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order', 'order_date', 'status'
    ];

    public function tools()
    {
        return $this->belongsToMany(Tool::class)->withTrashed();
    }

    public function entrance()
    {
        return $this->hasOne(Entrance::class)->withTrashed();
    }

    public function withdrawal()
    {
        return $this->hasOne(Withdrawal::class)->withTrashed();
    }

    public function fromWarehouse()
    {
        return $this->belongsTo(Warehouse::class,'from_warehouse')->withTrashed();
    }

    public function toWarehouse()
    {
        return $this->belongsTo(Warehouse::class, 'to_warehouse')->withTrashed();
    }

    public function transfer()
    {
        return $this->hasOne(Transfer::class)->withTrashed();
    }


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
