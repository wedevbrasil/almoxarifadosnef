<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Clube
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property boolean|null $status
 */

class Withdrawal extends Eloquent
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc', 'quantity', 'tool_giveback', 'giveback_date', 'giveback_reason', 'products'
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'tools',
        'tools_ids',
    ];

    public function provider()
    {
        return $this->belongsTo(Provider::class)->withTrashed();
    }

    public function tool()
    {
        return $this->belongsTo(Tool::class)->withTrashed();
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class)->withTrashed();
    }

    public function employee()
    {
        return $this->hasOne(Employee::class)->withTrashed();
    }

    public function withdrawalType()
    {
        return $this->belongsTo(WithdrawalType::class)->withTrashed();
    }

    public function entrance()
    {
        return $this->hasOne(Entrance::class)->withTrashed();
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class)->withTrashed();
    }

    public function transfer()
    {
        return $this->hasOne(Transfer::class)->withTrashed();
    }


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
