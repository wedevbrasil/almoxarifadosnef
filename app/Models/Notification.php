<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DatabaseNotification extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'notifications';

}
