<?php

namespace App\Imports;

use App\Models\Tool;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithMappedCells;

class ToolsImport implements WithCalculatedFormulas, ToCollection
{

    /**
     * @param array $row
     *
     * @return Tool|null
     */
    public function model(array $row)
    {
        return new Tool([
            'name'     => $row[0],
            'email'    => $row[1],
            'password' => Hash::make($row[2]),
        ]);
    }

    /**
     * @param array $array
     */
    public function array(array $array)
    {
        // TODO: Implement array() method.
    }

    /**
     * @return array
     */
    public function mapping(): array
    {
        // TODO: Implement mapping() method.
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        // TODO: Implement collection() method.
    }
}
